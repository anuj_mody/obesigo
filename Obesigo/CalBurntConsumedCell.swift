//
//  CalBurntConsumedCell.swift
//  Obesigo
//
//  Created by Anuj  on 24/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class CalBurntConsumedCell: UITableViewCell {
    
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblCaloriesUnit: UILabel!
    @IBOutlet var btQuantity: UIButton!
    @IBOutlet var btDelete: UIButton!
    @IBOutlet var lblUnit: UILabel!
    @IBOutlet var viewUnderline: UIView!
    
    @IBOutlet var lblCalUnitTralingConstToSV: NSLayoutConstraint!
    @IBOutlet var lblCalUnitTralingConstToUnit: NSLayoutConstraint!
    
    var quantity : Int?
    var COUNT_CLICK : (()->Void)? = nil
    var DELETE_CLICK : (()->Void)? = nil
    
    var hideOthers : Bool! {
        didSet{
            if self.hideOthers{
                btDelete.isHidden=true
                btQuantity.isHidden=true
                lblUnit.isHidden=true
                viewUnderline.isHidden=true
                lblCalUnitTralingConstToSV.priority=999
            }else{
                btDelete.isHidden=false
                btQuantity.isHidden=false
                lblUnit.isHidden=false
                viewUnderline.isHidden=false
                lblCalUnitTralingConstToUnit.priority=999
            }
        }
    }
    
    var dataExercise : ExerciseModel?{
        didSet{
            lblType.text = dataExercise?.exercise_name
            lblCaloriesUnit.text = "\((dataExercise?.duration_count)!) \((dataExercise?.units)!), (\((dataExercise?.calories_burnt)!) Cal.)"
            if (dataExercise?.isSelected)!{
                self.accessoryType = .checkmark
            }else{
                self.accessoryType = .none
            }
        }
    }
    
    var dataFoodItems : FoodItemModel!{
        didSet{
            lblType.text = dataFoodItems?.item_name
            lblCaloriesUnit.text = "\((dataFoodItems.quantity)!) \((dataFoodItems?.units)!), (\((dataFoodItems.calories_consume)!) Cal.)"
            if (dataFoodItems?.isSelected)!{
                self.accessoryType = .checkmark
            }else{
                self.accessoryType = .none
            }
        }
    }
    
    var data : CalBurntModel?{
        didSet{
            lblType.text = data?.exercise_name
            lblCaloriesUnit.text = "\((data?.og_duration_count)!) \((data?.units) ?? ""), (\((data?.og_calories_burnt)!))"
            lblUnit.text = data?.units
            btQuantity.setTitle(data?.og_duration_count, for: .normal)
            
        }
    }
    
    var dataCalConsumed : CalConsumedModel!{
        didSet{
            lblType.text = dataCalConsumed?.item_name
            lblCaloriesUnit.text = "\((dataCalConsumed.og_quantity) ?? "") \((dataCalConsumed?.units) ?? ""), (\((dataCalConsumed.og_calories_consume) ?? ""))"
            lblUnit.text = dataCalConsumed?.units
            quantity = Int(Double((dataCalConsumed?.og_quantity)!)!)
            btQuantity.setTitle(String(describing: quantity ?? 1), for: .normal)
            
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btCount(_ sender: Any) {
        if COUNT_CLICK != nil{
            self.COUNT_CLICK!()
        }
    }
    @IBAction func btDelete(_ sender: Any) {
        if DELETE_CLICK != nil{
            self.DELETE_CLICK!()
        }
    }
}
