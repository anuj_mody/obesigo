//
//  ForgotPasswordVC.swift
//  Obesigo
//
//  Created by Anuj  on 02/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit
class ForgotPasswordVC: BaseVC {
    //MARK :- outlets
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var lblEmailError: UILabel!
    
    //MARK :- variables
    var lblEmailPH : UILabel?
    var presenter = ForgotPasswordPresenter(service: ForgotPasswordService())
    
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.addStatusBarBG()
    }
    
    //MARK :- Functions
    func setUp() {
        presenter.attachView(delegate: self)
        //self.addInputAccessoryForTextFields(textFields: [tfEmail])
        self.hideKeyboardWhenTappedAround()
        lblEmailPH = tfEmail.addPlaceHolder(mainView: self.view, placeHolderText: "Email")
        tfEmail.delegate=self
        //self.addInputAccessoryForTextFields(textFields: [tfEmail], dismissable: true, previousNextable: false, showDone: true)
    }
    
    //MARK :- Button Actions
    @IBAction func btBack(_ sender: Any) {
        goBack()
    }
    @IBAction func btSubmit(_ sender: Any) {
        presenter.callForgotPasswordWS(email: tfEmail.text!)
    }
}

extension ForgotPasswordVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lblEmailError.isHidden=true
        lblEmailPH?.animatePlaceHolder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.isEmpty)!{
            lblEmailPH?.animatePlaceHolder(animateUp: false)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}

extension ForgotPasswordVC : ForgotPasswordDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        self.showAlert(title: "Success!", message: successValue as? String ?? "")
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        
    }
    func showEmailError(message: String) {
        lblEmailError.isHidden=false
        lblEmailError.text = message
    }
}
