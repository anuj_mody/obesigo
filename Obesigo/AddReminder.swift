//
//  AddReminder.swift
//  Obesigo
//
//  Created by Anuj  on 05/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit
import UserNotifications

class AddReminder: BaseVC {
    
    //MARK:- outlets
    @IBOutlet var tfReminderName: UITextField!
    @IBOutlet var btTime: UIButton!
    @IBOutlet var btMon: UIButton!
    @IBOutlet var btTue: UIButton!
    @IBOutlet var btWed: UIButton!
    @IBOutlet var btThu: UIButton!
    @IBOutlet var btFri: UIButton!
    @IBOutlet var btSat: UIButton!
    @IBOutlet var btSun: UIButton!
    @IBOutlet var btDelete: UIButton!
    @IBOutlet var lblReminderNameError: UILabel!
    @IBOutlet var lblDaysError: UILabel!
    @IBOutlet var lblHeader: UILabel!
    
    //MARK:- variables
    var isMonSelected = false
    var isTueSelected = false
    var isWedSelected = false
    var isThuSelected = false
    var isFriSelected = false
    var isSatSelected = false
    var isSunSelected = false
    var datePicker: UIDatePicker?
    var toolbar: UIToolbar?
    var formatter = DateFormatter()
    var selectedTime:String?
    var reminderId:String?
    var name:String?
    var lblReminderNamePH:UILabel?
    var isPickerOpened=false
    var presenter = AddReminderPresenter(service: AddReminderService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Button actions
    @IBAction func btMon(_ sender: Any) {
        changeButton(day: 1)
    }
    @IBAction func btTue(_ sender: Any) {
        changeButton(day: 2)
    }
    @IBAction func btWed(_ sender: Any) {
        changeButton(day: 3)
    }
    @IBAction func btThu(_ sender: Any) {
        changeButton(day: 4)
    }
    @IBAction func btFri(_ sender: Any) {
        changeButton(day: 5)
    }
    @IBAction func btSat(_ sender: Any) {
        changeButton(day: 6)
    }
    @IBAction func btSun(_ sender: Any) {
        changeButton(day: 7)
    }
    @IBAction func btTime(_ sender: Any) {
        if !isPickerOpened{
            openDatePicker()
        }
    }
    @IBAction func btSave(_ sender: Any) {
        presenter.callAddEditReminderWS(reminderId: reminderId ?? "", name: tfReminderName.text!, time: btTime.title(for: .normal)!, days: [isMonSelected,isTueSelected,isWedSelected,isThuSelected,isFriSelected,isSatSelected,isSunSelected])
    }
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
    @IBAction func btDelete(_ sender: Any) {
        CommonFunctions().showAlert(viewController: self, message: "Are you sure you want to delete this reminder?") {
            [weak self](posClick) in
            
            if posClick{
                if self?.reminderId != nil{
                    self?.presenter.callDeleteReminderWS(id: (self?.reminderId!)!)
                }
            }
        }
    }
    
    //MARK:- Functions
    func setUp() {
        presenter.attachView(delegate: self)
        self.addStatusBarBG()
        self.hideKeyboardWhenTappedAround()
        self.tfReminderName.delegate=self
        lblReminderNamePH = tfReminderName.addPlaceHolder(mainView: self.view, placeHolderText: "Reminder Name")
        formatter.dateFormat = "hh:mm a"
        btTime.setTitle(formatter.string(from: Date()), for: .normal)
        if reminderId != nil {
            lblHeader.text = "Edit Reminder"
            updateDays()
            tfReminderName.text = name ?? ""
            lblReminderNamePH?.animatePlaceHolder()
            btTime.setTitle("".getFormatedDate(fromFormat: "HH:mm:ss", toFormat: "hh:mm a", dateString: selectedTime!), for: .normal)
        }else{
            btDelete.isHidden=true
        }
    }
    func changeButton(day:Int)  {
        lblDaysError.isHidden=true
        switch day {
        case 1:
            if isMonSelected{
                isMonSelected=false
                btMon.setImage(#imageLiteral(resourceName: "ic_mon_unselected"), for: .normal)
            }else{
                isMonSelected=true
                btMon.setImage(#imageLiteral(resourceName: "ic_mon_selected"), for: .normal)
            }
            break
        case 2:
            if isTueSelected{
                isTueSelected=false
                btTue.setImage(#imageLiteral(resourceName: "ic_tue_unselected"), for: .normal)
            }else{
                isTueSelected=true
                btTue.setImage(#imageLiteral(resourceName: "ic_tue_selected"), for: .normal)
            }
            break
        case 3:
            if isWedSelected{
                isWedSelected=false
                btWed.setImage(#imageLiteral(resourceName: "ic_wed_unslelected"), for: .normal)
            }else{
                isWedSelected=true
                btWed.setImage(#imageLiteral(resourceName: "ic_wed_selected"), for: .normal)
            }
            break
        case 4:
            if isThuSelected{
                isThuSelected=false
                btThu.setImage(#imageLiteral(resourceName: "ic_thu_unselected"), for: .normal)
            }else{
                isThuSelected=true
                btThu.setImage(#imageLiteral(resourceName: "ic_thu_selected"), for: .normal)
            }
            break
        case 5:
            if isFriSelected{
                isFriSelected=false
                btFri.setImage(#imageLiteral(resourceName: "ic_fri_unselected"), for: .normal)
            }else{
                isFriSelected=true
                btFri.setImage(#imageLiteral(resourceName: "ic_fri_selected"), for: .normal)
            }
            break
        case 6:
            if isSatSelected{
                isSatSelected=false
                btSat.setImage(#imageLiteral(resourceName: "ic_sat_unselected"), for: .normal)
            }else{
                isSatSelected=true
                btSat.setImage(#imageLiteral(resourceName: "ic_sat_selected"), for: .normal)
            }
            break
        case 7:
            if isSunSelected{
                isSunSelected=false
                btSun.setImage(#imageLiteral(resourceName: "ic_sun_unselected"), for: .normal)
            }else{
                isSunSelected=true
                btSun.setImage(#imageLiteral(resourceName: "ic_sun_selected"), for: .normal)
            }
            break
        default:
            break
        }
    }
    func openDatePicker() {
        isPickerOpened=true
        self.view.endEditing(true)
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200))
        datePicker?.backgroundColor = UIColor.white
        datePicker?.datePickerMode = .time
        formatter = DateFormatter()
        formatter.timeStyle = .short
        let stringDate = formatter.string(from: Date())
        datePicker?.date = formatter.date(from: stringDate)!
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.maxY+240, width: self.view.frame.width, height: 200))
        toolbar?.sizeToFit()
        toolbar?.isTranslucent=false
        toolbar?.barTintColor = Colors.themeColor
        toolbar?.tintColor = UIColor.white
        let doneButton = UIBarButtonItem(title: "done", style: .done, target: self, action:#selector(AddReminder.getDate))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action:#selector(AddReminder.hidePickers))
        toolbar?.setItems([cancelButton, spacer, doneButton], animated: true)
        self.view.addSubview(self.datePicker!)
        self.view.addSubview(self.toolbar!)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY-200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY-235, width: self.view.frame.width, height: 35)
        }, completion: nil)
        
    }
    func getDate()  {
        hidePickers()
        selectedTime = formatter.string(from: (datePicker?.date)!)
        btTime.setTitle(selectedTime!, for: .normal)
        //        setSelectedDate(showCurrentDate: false)
    }
    func hidePickers()  {
        isPickerOpened=false
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY+235, width: self.view.frame.width, height: 35)
        }){
            
            finished in
            self.toolbar?.removeFromSuperview()
            self.datePicker?.removeFromSuperview()
        }
    }
    func updateDays() {
        if isMonSelected{
            btMon.setImage(#imageLiteral(resourceName: "ic_mon_selected"), for: .normal)
        }
        
        if isTueSelected{
            btTue.setImage(#imageLiteral(resourceName: "ic_tue_selected"), for: .normal)
        }
        
        if isWedSelected{
            btWed.setImage(#imageLiteral(resourceName: "ic_wed_selected"), for: .normal)
        }
        
        if isThuSelected{
            btThu.setImage(#imageLiteral(resourceName: "ic_thu_selected"), for: .normal)
        }
        
        if isFriSelected{
            btFri.setImage(#imageLiteral(resourceName: "ic_fri_selected"), for: .normal)
        }
        
        if isSatSelected{
            btSat.setImage(#imageLiteral(resourceName: "ic_sat_selected"), for: .normal)
        }
        
        if isSunSelected{
            btSun.setImage(#imageLiteral(resourceName: "ic_sun_selected"), for: .normal)
        }
    }
    /*func addNotification()  {
     var components = DateComponents()
     components.hour = 6
     components.minute = 11
     components.weekday = 4 // sunday = 1 ... saturday = 7
     //components.weekdayOrdinal = 10
     components.timeZone = .current
     
     let calendar = Calendar(identifier: .gregorian)
     //calendar.date(from: components)!
     
     let triggerWeekly = Calendar.current.dateComponents([.weekday,.hour,.minute,.second,], from: Date())
     
     if #available(iOS 10.0, *) {
     let trigger =  UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
     let content = UNMutableNotificationContent()
     content.title = "Hello"
     content.body = "Hi"
     content.sound = UNNotificationSound.default()
     content.categoryIdentifier = "todoList"
     let request = UNNotificationRequest(identifier: "textNotification", content: content, trigger: trigger)
     UNUserNotificationCenter.current().delegate = self
     //UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
     UNUserNotificationCenter.current().add(request) {(error) in
     if let error = error {
     print("Uh oh! We had an error: \(error)")
     }
     }
     } else {
     // Fallback on earlier versions
     }
     }*/
    
    func addReminder(successValue:Any) {
        let hours = "".getFormatedDate(fromFormat: "hh:mm a", toFormat: "HH", dateString: btTime.title(for: .normal)!)
        let min = "".getFormatedDate(fromFormat: "hh:mm a", toFormat: "mm", dateString: btTime.title(for: .normal)!)
        if isMonSelected{
            UserReminders.sharedInstance.addReminder(title: tfReminderName.text!, id: ((successValue as? RemindersModel)?.id)!, weekDay: 2, hours: Int(hours)!, mins: Int(min)!)
        }
        
        if isTueSelected{
            UserReminders.sharedInstance.addReminder(title: tfReminderName.text!, id: ((successValue as? RemindersModel)?.id)!, weekDay: 3, hours: Int(hours)!, mins: Int(min)!)
        }
        
        
        if isWedSelected{
            UserReminders.sharedInstance.addReminder(title: tfReminderName.text!, id: ((successValue as? RemindersModel)?.id)!, weekDay: 4, hours: Int(hours)!, mins: Int(min)!)
        }
        
        if isThuSelected{
            UserReminders.sharedInstance.addReminder(title: tfReminderName.text!, id: ((successValue as? RemindersModel)?.id)!, weekDay: 5, hours: Int(hours)!, mins: Int(min)!)
        }
        
        if isFriSelected{
            UserReminders.sharedInstance.addReminder(title: tfReminderName.text!, id: ((successValue as? RemindersModel)?.id)!, weekDay: 6, hours: Int(hours)!, mins: Int(min)!)
        }
        
        if isSatSelected{
            UserReminders.sharedInstance.addReminder(title: tfReminderName.text!, id: ((successValue as? RemindersModel)?.id)!, weekDay: 7, hours: Int(hours)!, mins: Int(min)!)
        }
        
        if isSunSelected{
            UserReminders.sharedInstance.addReminder(title: tfReminderName.text!, id: ((successValue as? RemindersModel)?.id)!, weekDay: 1, hours: Int(hours)!, mins: Int(min)!)
        }
    }
}

extension AddReminder : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lblReminderNamePH?.animatePlaceHolder()
        lblReminderNameError.isHidden=true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.isEmpty)!{
            lblReminderNamePH?.animatePlaceHolder(animateUp: false)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddReminder : AddReminderDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        if reminderId  == nil {
            //add
            addReminder(successValue: successValue)
        }else{
            //edit remove the previous reminder and then add it again
            UserReminders.sharedInstance.removeNotifications(reminderId: reminderId!)
            addReminder(successValue: successValue)
        }
        self.goBack()
        let data = ["from":reminderId == nil ? "add":"edit",
                    "model":successValue]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.REMINDER_ADDED), object: self, userInfo: ["data":data])
        
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        self.showAlert(title: "Error!", message: messgae as? String ?? "")
    }
    func onDeleteReminderSuccess(msg: String) {
        self.goBack()
        UserReminders.sharedInstance.removeNotifications(reminderId: reminderId!)
        let data = ["from":"delete",
                    "message":msg]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.REMINDER_ADDED), object: self, userInfo: ["data":data])
    }
    func onDeleteReminderError(msg: String?, error: ErrorType?) {
        self.showAlert(title: "", message: (error?.rawValue)!)
    }
    func onValidationError(message: String) {
        if message == ErrorType.REMINDER_NAME_ERROR.rawValue{
            lblReminderNameError.isHidden=false
        }else{
            lblDaysError.isHidden=false
        }
        //self.showAlert(title: "Error!", message: message)
    }
}

@available(iOS 10.0, *)
extension AddReminder : UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
    }
}
