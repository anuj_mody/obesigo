//
//  TestimonialsModel.swift
//  Obesigo
//
//  Created by Anuj  on 25/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
struct TestimonialsModel {
    var first_name:String?
    var last_name:String?
    var created_date:String?
    var profile_photo:String?
    var rating:String?
    var testimonial_msg:String?
}
