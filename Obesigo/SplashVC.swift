//
//  SplashVC.swift
//  Obesigo
//
//  Created by Anuj  on 11/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit
import OneSignal

class SplashVC: BaseVC {
    
    //outlets
    @IBOutlet var viewLoginSignup: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var lblEmailPH: UILabel!
    @IBOutlet var lblPasswordPH: UILabel!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var lblEmailError: UILabel!
    @IBOutlet var lblPwdError: UILabel!
    @IBOutlet var ivQuote: UIImageView!
    
    //Contraints
    @IBOutlet var ivYellowImageLeadingConst: NSLayoutConstraint!
    @IBOutlet var ivLogoCenterXConst: NSLayoutConstraint!
    @IBOutlet var ivTagLineCenterXConst: NSLayoutConstraint!
    @IBOutlet var viewLoginSignupTopConst: NSLayoutConstraint!
    @IBOutlet var ivLogoTopConst: NSLayoutConstraint!
    
    // MARK:- variables
    var lblActivePH : UILabel?
    var emailPH: UILabel?
    var passwordPH: UILabel?
    var actualInset:UIEdgeInsets?
    var showQuote = true
    lazy var presenter = SplashPresenter(service: SplashService())
    
    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        addStatusBarBG()
    }
    override func viewDidAppear(_ animated: Bool) {
        //animateView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    // MARK: - Functions
    func animateView()  {
        
        UIView.animate(withDuration: 0.8, delay: 0.5, options: .curveLinear, animations: {
            self.ivYellowImageLeadingConst.constant = -150
            self.ivLogoCenterXConst.constant=0
            self.ivLogoTopConst.constant=20
            self.ivTagLineCenterXConst.constant=590
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.8, delay: 0.5, options: .curveEaseInOut, animations: {
            self.viewLoginSignup.transform = CGAffineTransform(translationX: 0, y: -1250)
        }){
            _ in
            
        }
    }
    func setUp() {
        
        if showQuote{
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.ivQuote.isHidden=true
            }
        }else{
            ivQuote.isHidden=true
        }
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled=false
        presenter.attachView(delegate: self)
        self.addInputAccessoryForTextFields(textFields: [tfEmail,tfPassword])
        tfEmail.delegate = self
        tfPassword.delegate=self
        actualInset = scrollView.contentInset
    }
    
    // MARK: - Button Actions
    @IBAction func btLogin(_ sender: Any) {
        presenter.callLoginWS(email: tfEmail.text!, pwd: tfPassword.text!)
    }
    @IBAction func btSignUp(_ sender: Any) {
        self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "SignUpSB",identifier: "SignUpVC"))
    }
    @IBAction func btForgetPassword(_ sender: Any) {
        self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "ForgetPasswordSB",identifier: "ForgetPasswordVC"))
    }
    
}

extension SplashVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        KeybordAccessories.addKeyboardNotification(scrollView: scrollView, view: self.view, activeField: textField, activeTextView: nil, actualContentInset: actualInset!)
        if textField == tfEmail{
            lblActivePH = lblEmailPH
            lblEmailError.isHidden=true
        }else if textField==tfPassword{
            lblActivePH = lblPasswordPH
            lblPwdError.isHidden=true
        }
        lblActivePH?.animatePlaceHolder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfEmail{
            lblActivePH = lblEmailPH
            presenter.checkEmail(email: textField.text!)
        }else if textField==tfPassword{
            lblActivePH = lblPasswordPH
            presenter.checkPassword(password: textField.text!)
        }
        if (textField.text?.isEmpty)!{
            lblActivePH?.animatePlaceHolder(animateUp: false)
        }
    }
}

extension SplashVC : SplashDelegate{
    func showIndicator(showIt: Bool,showTint:Bool) {
        if showIt{
            IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        //here i am adding reminders
        DispatchQueue.global().async {
            UserReminders.sharedInstance.addRemindersForFirstTime()
        }

        if ((successValue as? [User])?[0].is_mobile_verified! == "0"){
            let vc = self.getViewControllerFromStoryBoard(storyBoardName: "OtpSB", identifier: "OtpVC") as? OtpVC
            vc?.showBack = true
            self.switchViewController(viewController: vc)
        }else if ((successValue as? [User])?[0].diet_preference_id == 0){
            self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "BMICalculatorSB", identifier: "BMICalculatorVC"))
        }else{
            Preffrences().setOTPEntered(value: true)
            Preffrences().setBMIDone(value: true)
            OneSignal.sendTags(["user_id":Preffrences().getUserId()])
            self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "HomeMain", identifier: "SWRevealViewController") as! SWRevealViewController)
        }
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        print(messgae as! String)
        self.showAlert(title: "Error!", message: (messgae as? String ?? "") == "" ? ErrorType.Something_Went_Wrong.rawValue : (messgae as? String ?? ""))
    }
    func showEmailError(message: String) {
        lblEmailError.isHidden=false
        lblEmailError.text = message
    }
    func showPwdError(message: String) {
        lblPwdError.isHidden=false
        lblPwdError.text = message
    }
}
