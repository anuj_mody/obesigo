//
//  CaloriesTracker.swift
//  Obesigo
//
//  Created by Anuj  on 23/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class CaloriesTracker : BaseVC{
    //MARK:- outlets
    @IBOutlet var tvCalories: UITableView!
    @IBOutlet var lblCalConsumed: UILabel!
    @IBOutlet var lblCalBurnt: UILabel!
    @IBOutlet var viewCover: UIView!
    
    //MARK:- variables
    var dateFormatter = DateFormatter()
    var arrCalories  = [TrackerModel]()  {
        didSet{
            
            tvCalories.delegate=self
            tvCalories.dataSource=self
            tvCalories.reloadData()
            if arrCalories.count>0{
                if arrCalories[0].current != nil {
                    if String(describing: arrCalories[0].current!).contains("."){
                        lblCalBurnt.text = String(describing: arrCalories[0].current!).components(separatedBy: ".")[0]
                    }else{
                        lblCalBurnt.text = String(describing: arrCalories[0].current!)
                    }
                }
                
                if arrCalories[0].target != nil{
                    lblCalConsumed.text = String(describing: arrCalories[0].target!).components(separatedBy: ".")[0]
                }
            }
            
            viewCover.isHidden=true
            tvCalories.tableFooterView = nil
            //for updating the stats in preffence
            if (arrCalories.count)>2{
                if Calendar.current.isDateInToday(dateFormatter.date(from: (arrCalories[2].logModel?.log_date)!)!){
                    var stats = Preffrences().getUserStats()
                    stats?["calories"] = "\(((Int((arrCalories[2].logModel?.cal_consumed)!))! - (Int((arrCalories[2].logModel?.cal_burnt)!))!)) Cal."
                    Preffrences().setUserStats(value: stats!)
                }
            }
        }
    }
    var presenter = WeightTrackerPresenter(service: StatService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Functions
    func setUp()  {
        presenter.attachView(delegate: self)
        self.addStatusBarBG()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        
        tvCalories.register(UINib(nibName: String(describing: CalHeaderCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CalHeaderCell.self))
        tvCalories.register(UINib(nibName: String(describing: CalGraphCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CalGraphCell.self))
        tvCalories.register(UINib(nibName: String(describing: CalDataCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CalDataCell.self))
        tvCalories.tableFooterView = UIView()
        tvCalories.estimatedRowHeight = 500
        tvCalories.rowHeight = UITableViewAutomaticDimension
        
        presenter.callWeightTrackerWS(statId: "4")
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constants.CAL_BURNT), object: nil, queue: nil){
            [weak self](noti) in
            var array  = ((noti.userInfo as NSDictionary?)?.object(forKey: "data") as? [TrackerModel])!
            if ((array[0].logs?.count)!>0){
                self?.arrCalories = array
            }else{
                self?.arrCalories = [TrackerModel]()
                var model = TrackerModel()
                var logModel : Logs?
                var arrayLogs=[Logs]()
                //arrCalories = [TrackerModel]()
                model.current = 0.0
                model.target = 0.0
                model.wws_level = 0
                model.isGraph=true
                for _ in 0 ... 11{
                    logModel = Logs()
                    logModel?.log_date = "0"
                    logModel?.cal_burnt = "0"
                    logModel?.cal_consumed = "0"
                    arrayLogs.append(logModel!)
                }
                if model.logs == nil{
                    model.logs = [Logs]()
                    model.logs? = arrayLogs
                }
                self?.arrCalories.append(model)
                //for header
                model = TrackerModel()
                model.isHeader = true
                self?.arrCalories.append(model)
                self?.viewCover.isHidden=true
                // loadData()
                self?.tvCalories.tableFooterView = self?.addFooterView(msg: "You don't have any entries made yet! Click on \"Add Calories Consumed  / Add Calories Burnt now!\"")
            }
        }

    }
    func addFooterView(msg:String) -> UIView {
        let toastLabel = UILabel(frame:CGRect(x: 0,y:0,width: self.view.frame.width,height: 50))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.font = UIFont.systemFont(ofSize: 14)
        toastLabel.numberOfLines = 0
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.textAlignment = NSTextAlignment.center
        self.view.addSubview(toastLabel)
        toastLabel.text = msg
        toastLabel.clipsToBounds  =  true
        return toastLabel
    }
    
    //MARK:- Button Action
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
    @IBAction func btCalConsumed(_ sender: Any) {
        self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: CaloriesConsumed.self), identifier: String(describing: CaloriesConsumed.self)))
        
    }
    @IBAction func btCalBurnt(_ sender: Any) {
        self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: CaloriesBurnt.self), identifier: String(describing: CaloriesBurnt.self)))
    }
    
}

extension CaloriesTracker : WeightTrackerDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        arrCalories = successValue as! [TrackerModel]
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        if errorValue == ErrorType.NO_RESULT{
            var model = TrackerModel()
            var logModel : Logs?
            var arrayLogs=[Logs]()
            //arrCalories = [TrackerModel]()
            model.current = 0.0
            model.target = 0.0
            model.wws_level = 0
            model.isGraph=true
            for _ in 0 ... 11{
                logModel = Logs()
                logModel?.log_date = "0"
                logModel?.cal_burnt = "0"
                logModel?.cal_consumed = "0"
                arrayLogs.append(logModel!)
            }
            if model.logs == nil{
                model.logs = [Logs]()
                model.logs? = arrayLogs
            }
            arrCalories.append(model)
            //for header
            model = TrackerModel()
            model.isHeader = true
            arrCalories.append(model)
            viewCover.isHidden=true
            // loadData()
            tvCalories.tableFooterView = addFooterView(msg: messgae as? String ?? "")
            
        }else{
            ShowDefaultImages.sharedInstance.showDefaultImage(view: self.view, type: errorValue,header: "",msg: messgae as! String){
                [weak self] finished in
                
                self?.presenter.callWeightTrackerWS(statId: "4")
            }
        }
    }
    func onStatAddedSuccess(success: Any?) {
        
    }
    func onStatAddedError(messgae: String?, error: ErrorType?) {
        
    }
}

//MARK:- Tableview Functions
extension CaloriesTracker : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCalories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (arrCalories[indexPath.row].isGraph){
            let  cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CalGraphCell.self), for: indexPath) as? CalGraphCell
            cell?.data = arrCalories[indexPath.row].logs
            return cell ?? UITableViewCell()
        }else if (arrCalories[indexPath.row].isHeader){
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CalHeaderCell.self), for: indexPath) as? CalHeaderCell
            return cell ?? UITableViewCell()
        }else if (arrCalories[indexPath.row].isUsers){
            let  cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CalDataCell.self), for: indexPath) as? CalDataCell
            cell?.data = [(arrCalories[indexPath.row].logModel?.cal_consumed)!,(arrCalories[indexPath.row].logModel?.cal_burnt)!,(arrCalories[indexPath.row].logModel?.log_date)!]
            return cell ?? UITableViewCell()
        }
        return  UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrCalories[indexPath.row].isGraph{
            return 400
        }else if arrCalories[indexPath.row].isHeader{
            return 44
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
