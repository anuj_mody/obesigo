//
//  ChangePasswordPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 09/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
protocol ChangePasswordDelegate : CommonDelegate {
    func showCurrentPwdError(message:String)
    func showNewPwdError(message:String)
    func showConfirmPwdError(message:String)
}

class ChangePasswordPresenter {
    weak var delegate : ChangePasswordDelegate?
    var service : ChangePasswordService
    init(service:ChangePasswordService) {
        self.service=service
    }
    func attachView(delegate:ChangePasswordDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func checkCurrentPwd(value:String)  {
        if service.checkCurrentPwd(value: value) != nil{
            delegate?.showCurrentPwdError(message: (service.checkCurrentPwd(value: value))!)
        }
    }
    func checkNewPwd(value:String)  {
        if service.checkNewPwd(value: value) != nil{
            delegate?.showNewPwdError(message: (service.checkNewPwd(value: value))!)
        }
    }
    func checkConfirmPwd(newPwd:String,confirmPwd:String)  {
        if service.checkConfirmPwd(newPwd: newPwd, confirmPwd: confirmPwd) != nil{
            delegate?.showConfirmPwdError(message: (service.checkConfirmPwd(newPwd: newPwd, confirmPwd: confirmPwd)!))
        }
    }
    func callChangePasswordWS(currentPwd:String,newPwd:String,confirmPwd:String){
        if service.checkCurrentPwd(value: currentPwd) != nil{
            delegate?.showCurrentPwdError(message: (service.checkCurrentPwd(value: currentPwd))!)
            return
        }
        
        if service.checkNewPwd(value: newPwd) != nil{
            delegate?.showNewPwdError(message: (service.checkNewPwd(value: newPwd))!)
            return
        }
        
        if service.checkConfirmPwd(newPwd: newPwd, confirmPwd: confirmPwd) != nil{
            delegate?.showConfirmPwdError(message: (service.checkConfirmPwd(newPwd: newPwd, confirmPwd: confirmPwd)!))
            return
        }
        
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callChangePasswordWS(newPwd: newPwd, currentPwd: currentPwd) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as? String ?? "", errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as? String ?? "")
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
}

class ChangePasswordService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func checkCurrentPwd(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_CURRENT_PWD.rawValue
        }
        
        if value.characters.count < 6{
            return ErrorType.PASSWORD_LENGHT.rawValue
        }
        
        if value != Preffrences().getPassword(){
            return ErrorType.INVALID_CURRENT_PWD.rawValue
        }
        return nil
    }
    func checkNewPwd(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_NEW_PWD.rawValue
        }
        
        if value.characters.count < 6{
            return ErrorType.PASSWORD_LENGHT.rawValue
        }
        return nil
    }
    func checkConfirmPwd(newPwd:String,confirmPwd:String) -> String?  {
        if confirmPwd.isEmpty{
            return ErrorType.EMPTY_CONFIRM_PWD.rawValue
        }
        
        if confirmPwd.characters.count < 6{
            return ErrorType.PASSWORD_LENGHT.rawValue
        }
        
        if !newPwd.isEmpty{
            if newPwd != confirmPwd{
                return ErrorType.INVALID_CONFIRM_PWD.rawValue
            }
        }
        return nil
    }
    func callChangePasswordWS(newPwd:String,currentPwd:String,result: @escaping onResponseReceived) {
        let params = ["screen_name":"Add Edit Reminder",
                      "webservice":"Add/Edit User Reminder",
                      "user_id":Preffrences().getUserId(),
                      "password":newPwd,
                      "current_password":currentPwd]
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.CHANGE_PASSWORD, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, nil)
            }
        }
    }
}
