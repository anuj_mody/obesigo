//
//  Stats.swift
//  Obesigo
//
//  Created by Anuj  on 31/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class Stats: BaseVC {
    //MARK :- outlets
    @IBOutlet var ivImage: UIImageView!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var lblCalories: UILabel!
    @IBOutlet var lblWaterIntake: UILabel!
    @IBOutlet var lblSleepLog: UILabel!
    
    //MARK :- constants
    @IBOutlet var ivImageHeightConst: NSLayoutConstraint!
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Functions
    func setUp()  {
        ivImageHeightConst.constant = ((UIScreen.main.bounds.height/3)-40)
    }
    
    func loadData() {
        lblWeight.text = (Preffrences().getUserStats()?["weight"])! == "0.0" ? "-" : "\(Preffrences().getUserStats()?["weight"] ?? "-")" + " kgs"
        lblCalories.text = Preffrences().getUserStats()?["calories"] == "0.0" ? "-" : Preffrences().getUserStats()?["calories"]
        lblWaterIntake.text = (Preffrences().getUserStats()?["waterIntake"])! == "0.0" ? "-" : "\(Preffrences().getUserStats()?["waterIntake"] ?? "-")" + " ltrs"
        lblSleepLog.text = (Preffrences().getUserStats()?["sleepLog"])! == "0.0" ? "-" : "\(Preffrences().getUserStats()?["sleepLog"] ?? "-")" + " hrs"
    }
    
    //MARK:- Button Actions
    @IBAction func btWeight(_ sender: Any) {
        
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: String(describing: WeightTracker.self), identifier: String(describing: WeightTracker.self)) as? WeightTracker
        vc?.stat_id = 1
        self.switchViewController(viewController: vc)
        
    }
    @IBAction func btCalories(_ sender: Any) {
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: String(describing: CaloriesTracker.self), identifier: String(describing: CaloriesTracker.self)) as? CaloriesTracker
        self.switchViewController(viewController: vc)
    }
    @IBAction func btWaterIntake(_ sender: Any) {
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: String(describing: WeightTracker.self), identifier: String(describing: WeightTracker.self)) as? WeightTracker
        vc?.stat_id = 2
        self.switchViewController(viewController: vc)
    }
    @IBAction func btSleepLog(_ sender: Any) {
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: String(describing: WeightTracker.self), identifier: String(describing: WeightTracker.self)) as? WeightTracker
        vc?.stat_id = 3
        self.switchViewController(viewController: vc)
    }
    
    
    
}
