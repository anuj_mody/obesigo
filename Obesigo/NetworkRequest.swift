//
//  NetworkRequest.swift
//  Obesigo
//
//  Created by Anuj  on 13/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import Alamofire
import SystemConfiguration

enum Type: String{
    case GET = "get"
    case POST = "post"
    case DELETE = "delete"
}

enum ErrorType : String{
    case No_Internet = "Your network doesn’t seem to be working properly"
    case Something_Went_Wrong = "Something went wrong.Please try again later."
    case WRONG_DATA = "Wrong data"
    case NO_RESULT = "No Result"
    case EMPTY_EMAIL = "Email is required"
    case INVALID_EMAIL = "Please enter a valid email id"
    case EMPTY_PASSWORD = "Please enter a password with 6 or more characters"
    case EMPTY_CURRENT_PWD = "Please enter your current password"
    case EMPTY_NEW_PWD = "Please enter your new password"
    case EMPTY_CONFIRM_PWD = "Please enter confirm password"
    case PASSWORD_LENGHT = "Password length should be of at least 6 characters"
    case EMPTY_FNAME = "First Name is required"
    case EMPTY_LNAME = "Last Name is required"
    case EMPTY_MOBILE = "Mobile no. is required"
    case EMPTY_DOB = "Date of birth is required"
    case EMPTY_GENDER = "Gender is required"
    case INVALID_CURRENT_PWD = "Current Password is incorrect"
    case INVALID_CONFIRM_PWD = "New password and Confirm password do not match"
    case REMINDER_NAME_ERROR = "Please enter reminder name"
    case REMINDER_DAYS_ERROR = "Please select days"
    case EMPTY_OTP = "OTP required"
}

class NetworkRequest {
    static let sharedInstance = NetworkRequest()
    typealias onResponseReceived = (_ success:AnyObject?,_ error : ErrorType?) -> Void
    private init(){
        
    }
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    func callWebService(url: String, type :Type, params: [String:Any]?, responseReceived : @escaping onResponseReceived) {
        
        if type == .GET{
            if isInternetAvailable(){
                let headers = ["Authorization":"Bearer \(Preffrences().getAuthToken())"]
                
                Alamofire.request(url,headers:headers).validate().responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        print(response.result.value as Any)
                        responseReceived(response.result.value as AnyObject, nil)
                    case .failure(let error):
                        print(error.localizedDescription)
                        responseReceived(nil, ErrorType.Something_Went_Wrong)
                    }
                })
            }else{
                responseReceived(nil, ErrorType.No_Internet)
            }
            
        }else if type == .POST{
            if isInternetAvailable(){
                
                let header = ["Authorization":"Bearer \(Preffrences().getAuthToken())"]
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        print(response.result.value as Any)
                        responseReceived(response.result.value as AnyObject, nil)
                    case .failure(let error):
                        print(error.localizedDescription)
                        responseReceived(nil, ErrorType.Something_Went_Wrong)
                    }
                })
            }else{
                responseReceived(nil, ErrorType.No_Internet)
            }
        }else{
            let header = ["Authorization":"Bearer \(Preffrences().getAuthToken())"]
            Alamofire.request(url, method: .delete, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success:
                    print(response.result.value as Any)
                    responseReceived(response.result.value as AnyObject, nil)
                case .failure(let error):
                    print(error.localizedDescription)
                    responseReceived(nil, ErrorType.Something_Went_Wrong)
                }
            })
            
            
        }
    }
    func uploadDataWithImage(url:String, images:[String:UIImage],parameters:[String : String],responseReceived : @escaping onResponseReceived) {
        let headers = ["Authorization":"Bearer \(Preffrences().getAuthToken())"]
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
            for (key, value) in images {
                multipartFormData.append(UIImageJPEGRepresentation(value,0.5)!, withName: key, fileName: "image.jpg", mimeType: "image/jpeg")
            }
        },
                         to:url,method:.post,
                         headers:headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON { data in
                    print(data.result.value as Any)
                    
                    if data.value == nil {
                        responseReceived(nil, ErrorType.Something_Went_Wrong)
                    }else{
                        responseReceived(data.result.value as AnyObject, nil)
                    }
                    
                }
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                responseReceived(nil, ErrorType.Something_Went_Wrong)
            }
        }
    }
}
