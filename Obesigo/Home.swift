//
//  Home.swift
//  Obesigo
//
//  Created by Anuj  on 16/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class Home: BaseVC {
    //MARK:- outlets
    @IBOutlet var tvHome: UITableView!
    @IBOutlet var lblCurrentDay: UILabel!
    @IBOutlet var btRight: UIButton!
    @IBOutlet var btLeft: UIButton!
    @IBOutlet var viewMeals: UIView!
    @IBOutlet var tvMealLable: UILabel!
    @IBOutlet var tvMeals: UITableView!
    @IBOutlet var lblMealHeader: UILabel!
    @IBOutlet var lblOr: UILabel!
    @IBOutlet var lblObesigo: UILabel!
    @IBOutlet var lbl1Sachet: UILabel!
    
    
    //MARK:- constraints
    @IBOutlet var tvMealsHeightConst: NSLayoutConstraint!
    @IBOutlet var tvMealBotomConstToDivider: NSLayoutConstraint!
    @IBOutlet var tvHomeTopConstToSV: NSLayoutConstraint!
    
    //MARK:- variables
    var isPurchased = false
    var enableSwipe = false
    var homeVC : HomeVC?
    private(set) lazy var transition : CATransition = {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut);
        transition.type = kCATransitionPush;
        
        return transition
        
    }()
    let transformInitial = CATransform3DMakeScale(1, 1, 1)
    let transformBetween = CATransform3DMakeScale(1, 1, 1)
    let transformFinal = CATransform3DMakeScale(1, 1, 1)
    var index = -1
    var indexTill = 0
    var array = [HomeModel]() {
        didSet{
            if array.count > 0 {
                if array[0].currentDay == nil {
                    tvHomeTopConstToSV.constant=0
                }else{
                    tvHomeTopConstToSV.constant=40
                    
                    if array[0].currentDay! == 1{
                        btLeft.isHidden=true
                    }
                    if array[0].currentDay! == 15{
                        btRight.isHidden=true
                    }
                    index = array[0].currentDay! == 0 ? 0 : array[0].currentDay!
                    if index == 14{
                        indexTill = index+1
                    }else{
                        indexTill = index+2
                    }
                    
                    if index == 15{
                        indexTill = index
                    }
                    
                    if Constants.SHOW_BUYNOW_ALERT{
                        Constants.SHOW_BUYNOW_ALERT = false
                        if 12 ... 15 ~= index{
                            self.showBuyNowAlert()
                        }
                    }
                    lblCurrentDay.text = "Day \(index)"
                }
                tvHome.delegate=self
                tvHome.dataSource=self
                
                if (array[0].data?[0].isPurchaseImage)!{
                    isPurchased=false
                    enableSwipe = false
                    homeVC?.animateContainerViewBottom(show: false)
                }else{
                    enableSwipe=true
                    isPurchased=true
                    homeVC?.animateContainerViewBottom(show: true)
                }
                tvHome.reloadData()
            }
        }
    }
    var arrayMeals = [MealsModel]()
    var tableIndex = 0
    var presenter = HomePresenter(service: HomeService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if isPurchased{
            homeVC?.animateContainerViewBottom(show: true)
        }else{
            homeVC?.animateContainerViewBottom(show: false)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Functions
    func setUp() {
        presenter.attachView(delegate: self)
        homeVC = (self.revealViewController().frontViewController as! UINavigationController).viewControllers[0] as? HomeVC
        self.revealViewController().panGestureRecognizer().isEnabled=false
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(Home.getSwipe(swipe:)))
        swipeLeft.direction = .left
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(Home.getSwipe(swipe:)))
        swipeRight.direction = .right
        
        tvHome.addGestureRecognizer(swipeLeft)
        tvHome.addGestureRecognizer(swipeRight)
        
        
        tvHome.register(UINib(nibName: String(describing: HomeHeader.self), bundle: nil), forCellReuseIdentifier: String(describing: HomeHeader.self))
        tvHome.register(UINib(nibName: String(describing: PlanListCell.self), bundle: nil), forCellReuseIdentifier: String(describing: PlanListCell.self))
        tvHome.register(UINib(nibName: String(describing: ImageCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ImageCell.self))
        tvHome.register(UINib(nibName: String(describing: BuyNowCell.self), bundle: nil), forCellReuseIdentifier: String(describing: BuyNowCell.self))
        tvHome.register(UINib(nibName: String(describing: ProductCodeCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ProductCodeCell.self))
        tvMeals.register(UINib(nibName: String(describing: MedHistoryCell.self), bundle: nil), forCellReuseIdentifier: String(describing: MedHistoryCell.self))
        
        tvHome.estimatedRowHeight = 100
        tvHome.rowHeight = UITableViewAutomaticDimension
        
        tvMeals.estimatedRowHeight = 100
        tvMeals.rowHeight = UITableViewAutomaticDimension
        
        tvMeals.delegate=self
        tvMeals.dataSource=self
        
        tvMeals.tableFooterView = UIView()
        if Constants.HOME_ARRAY != nil{
            array = Constants.HOME_ARRAY!
        }else{
            presenter.callGetHomeScreenWS()
        }
        
    }
    func getSwipe(swipe:UISwipeGestureRecognizer)  {
        if swipe.direction == .right{
            
            if enableSwipe{
                index -= 1
                if index <= 0{
                    index=1
                    return
                }
                changeImage(direction: .right, index: index)
            }
            
        }
        if swipe.direction == .left {
            
            if enableSwipe{
                if index == indexTill{
                    return
                }
                index += 1
                if index > (indexTill){
                    return
                }
                changeImage(direction: .left, index: index)
            }
            
        }
        
    }
    func changeImage(direction : UISwipeGestureRecognizerDirection,index : Int)  {
        
        lblCurrentDay.text = "Day \(index)"
        
        if index==1{
            btLeft.isHidden=true
        }else if index == indexTill{
            btRight.isHidden=true
        }else{
            btLeft.isHidden=false
            btRight.isHidden=false
        }
        
        if direction == .left{
            
            transition.subtype = kCATransitionFromRight
            tvHome.layer.add(transition, forKey: nil)
            tvHome.layer.transform = transformInitial
            UIView.animate(withDuration: 0.5, animations: {
                self.tvHome.layer.transform = self.transformBetween
                
            }){
                finished in
                UIView.animate(withDuration: 0.2, animations: {
                    self.tvHome.layer.transform = self.transformFinal
                    
                }){
                    finished in
                }
                
            }
            
            //load data here
        }
        
        if direction == .right{
            transition.subtype = kCATransitionFromLeft
            tvHome.layer.add(transition, forKey: nil)
            tvHome.layer.transform = transformInitial
            UIView.animate(withDuration: 0.5, animations: {
                self.tvHome.layer.transform = self.transformBetween
            }){
                finished in
                UIView.animate(withDuration: 0.2, animations: {
                    self.tvHome.layer.transform = self.transformFinal
                }){
                    finished in
                }
            }
            //load data
        }
        
        tvHome.reloadData()
        
    }
    func showMealDialog(header:String) {
        tvMeals.reloadData()
        tvMealsHeightConst.constant = CGFloat(60 * arrayMeals.count)
        viewMeals.frame = self.view.bounds
        lblMealHeader.text = header
        viewMeals.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        if header.lowercased() == "lunch" || header.lowercased() == "dinner" || header.lowercased() == "evening snacks"{
            tvMealBotomConstToDivider.constant = 60
            lblOr.isHidden=false
            lblObesigo.isHidden=false
            lbl1Sachet.isHidden=false
            if header.lowercased() == "evening snacks"{
                lblObesigo.text = "Obesigo Soup"
                lbl1Sachet.text = "1.0 sachet (175 Cal)"
            }else{
                lblObesigo.text = "Obesigo Shake"
                lbl1Sachet.text = "1.0 sachet (175 Cal)"
            }
        }else{
            lblOr.isHidden=true
            lblObesigo.isHidden=true
            lbl1Sachet.isHidden=true
            tvMealBotomConstToDivider.constant = 10
        }
        view.addSubview(viewMeals)
    }
    func openSendinquiryDialog()  {
        let subject = "Inquiry"
        let data = "mailto:obesigo@hexagonnutrition.com?subject=\(subject)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let url = NSURL(string: data!) {
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    func showBuyNowAlert()  {
        let alert = UIAlertController(title: "Reminder!", message: "Your plan is about to get over. Please click on the \"Buy Now\" button.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default){
            [weak self]finished in
            
        })
        alert.addAction(UIAlertAction(title: "Buy Now", style: .default){
            [weak self]finished in
            self?.goToBuyNowPage()
        })
        self.present(alert, animated: true, completion: nil)
    }
    func goToBuyNowPage() {
        let url = URL(string: "http://www.mynutrishop.co.in")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btRight(_ sender: Any) {
        if index == indexTill{
            return
        }
        index += 1
        if index > (indexTill){
            return
        }
        changeImage(direction: .left, index: index)
    }
    @IBAction func btLeft(_ sender: Any) {
        index -= 1
        if index <= 0{
            index=1
            return
        }
        changeImage(direction: .right, index: index)
    }
    @IBAction func btOk(_ sender: Any) {
        self.viewMeals.removeFromSuperview()
    }
    
}

extension Home : HomeDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        let vc = (self.revealViewController().frontViewController as! UINavigationController).viewControllers[0] as? HomeVC
        vc?.animateContainerViewBottom(show: true)
        array = [HomeModel]()
        array = (successValue as? [HomeModel])!
        Constants.HOME_ARRAY = array
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        
        //homeVC?.animateContainerViewBottom(show: true)
        ShowDefaultImages.sharedInstance.showDefaultImage(view: self.view, type: errorValue,header: "",msg: messgae as? String ?? "",fromTop: 0,reduceHeight: true){
            [weak self] finished in
            self?.presenter.callGetHomeScreenWS()
            
        }
    }
    func onActivationSuccess(success: Any?) {
        //array.removeAll()
        presenter.callGetHomeScreenWS()
    }
    func onActivationError(success: Any?, error: ErrorType?) {
        if error == ErrorType.No_Internet{
            showToast(msg: ErrorType.No_Internet.rawValue)
        }else{
            showToast(msg: success as? String ?? "")
        }
        
    }
}

//MARK:- Tableview Functions
extension Home : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tvMeals{
            return arrayMeals.count
        }else{
            
            if index == -1{
                return array[0].data?.count ?? 0
            }else{
                tableIndex = index-1 < 0 ? 0 : index-1
                //                if tableIndex == 0{
                //                    tableIndex = 1
                //                }
                return array[tableIndex].data?.count ?? 0
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tvHome{
            if (array[tableIndex].data?[indexPath.row].isMealHeader)!{
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HomeHeader.self), for: indexPath) as? HomeHeader
                cell?.header =  "Meal Plan"
                return cell ?? UITableViewCell()
            }else if (array[tableIndex].data?[indexPath.row].isMealData)!{
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PlanListCell.self), for: indexPath) as? PlanListCell
                cell?.mealData =  array[tableIndex].data?[indexPath.row].mealName
                return cell ?? UITableViewCell()
            }else if (array[tableIndex].data?[indexPath.row].isExerciseHeader)!{
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HomeHeader.self), for: indexPath) as? HomeHeader
                cell?.header =  "Exercise Plan(Any One)"
                return cell ?? UITableViewCell()
            }else if (array[tableIndex].data?[indexPath.row].isExerciseData)!{
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PlanListCell.self), for: indexPath) as? PlanListCell
                cell?.exerciseData =  [(array[tableIndex].data?[indexPath.row].exerciseName)!,"\(((array[tableIndex].data?[indexPath.row].exerciseTime)!)) \(((array[tableIndex].data?[indexPath.row].exerciseUnit)!))"]
                return cell ?? UITableViewCell()
            }else if (array[tableIndex].data?[indexPath.row].isImage)!{
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageCell.self), for: indexPath) as? ImageCell
                cell?.url = array[tableIndex].data?[indexPath.row].imageUrl
                return cell ?? UITableViewCell()
            }else if (array[tableIndex].data?[indexPath.row].isBuyNowCell)!{
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BuyNowCell.self), for: indexPath) as? BuyNowCell
                cell?.BUY_NOW_CLICK = {
                    [weak self] in
                    self?.goToBuyNowPage()
                }
                
                cell?.CONTACT_US_CLICK = {
                    [weak self] in
                    self?.openSendinquiryDialog()
                }
                
                cell?.READ_MORE_CLICK = {
                    [weak self] in
                    self?.switchViewController(viewController: self?.getViewControllerFromStoryBoard(storyBoardName: String(describing: ReadMore.self), identifier: String(describing: ReadMore.self)))
                }
                
                return cell ?? UITableViewCell()
            }else if (array[tableIndex].data?[indexPath.row].isPurchaseCell)!{
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProductCodeCell.self), for: indexPath) as? ProductCodeCell
                cell?.mainTable = tvHome
                cell?.DONE_CLICK = {
                    [weak self] in
                    self?.presenter.callSubmitActivationCode(code: (cell?.tfActivationCode.text)!)
                }
                return cell ?? UITableViewCell()
            }else if (array[tableIndex].data?[indexPath.row].isPurchaseImage)!{
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageCell.self), for: indexPath) as? ImageCell
                cell?.showImage = true
                
                return cell ?? UITableViewCell()
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MedHistoryCell.self), for: indexPath) as? MedHistoryCell
            cell?.meals = arrayMeals[indexPath.row]
            cell?.selectionStyle = .none
            return cell ?? UITableViewCell()
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tvHome{
            if (array[tableIndex].data?[indexPath.row].isMealData)! || (array[tableIndex].data?[indexPath.row].isExerciseData)!{
                return 100
            }else if (array[tableIndex].data?[indexPath.row].isImage)!{
                return self.view.frame.height/3
            }else if (array[tableIndex].data?[indexPath.row].isBuyNowCell)!{
                return 270
            }else if (array[tableIndex].data?[indexPath.row].isPurchaseCell)!{
                return 112
            }else if (array[tableIndex].data?[indexPath.row].isPurchaseImage)!{
                return 200
            }
            return UITableViewAutomaticDimension
        }
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tvHome{
            if (array[tableIndex].data?[indexPath.row].isMealData)!{
                arrayMeals = (array[tableIndex].data?[indexPath.row].mealData)!
                showMealDialog(header: (array[tableIndex].data?[indexPath.row].mealName)!)
            }
        }
    }
}
