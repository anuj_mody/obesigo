//
//  ReminderCell.swift
//  Obesigo
//
//  Created by Anuj  on 03/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class ReminderCell: UITableViewCell {
    //Mark :- outlets
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDays: UILabel!
    @IBOutlet var lblTime: UILabel!
    
    var data : RemindersModel!{
        didSet{
            lblName.text=data.name
            lblTime.text = "".getFormatedDate(fromFormat: "HH:mm:ss", toFormat: "hh:mm a", dateString: data.time!)
            lblDays.text = formatDay(day: data.days!)
        }
    }
    
    //MARK:-Functions
    func formatDay(day:String) -> String {
        var returnedDays = ""
        if day == "1,2,3,4,5,6,7"{
            return "Everyday"
        }else{
            if day.contains("1"){
                returnedDays += "Mon,"
            }
            
            if day.contains("2"){
                returnedDays += "Tue,"
            }
            
            if day.contains("3"){
                returnedDays += "Wed,"
            }
            
            if day.contains("4"){
                returnedDays += "Thur,"
            }
            
            if day.contains("5"){
                returnedDays += "Fri,"
            }
            
            if day.contains("6"){
                returnedDays += "Sat,"
            }
            
            if day.contains("7"){
                returnedDays += "Sun,"
            }
          
            return String(returnedDays.characters.dropLast())
        }
    }
    
    
}
