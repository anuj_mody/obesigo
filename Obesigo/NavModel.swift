//
//  NavModel.swift
//  Obesigo
//
//  Created by Anuj  on 31/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
struct NavModel{
    var header: String?
    var icon : String?
    var isSelected = false
}
