//
//  WeightTracker.swift
//  Obesigo
//
//  Created by Anuj  on 25/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class WeightTracker: BaseVC {
    
    //Mvar:- outlets
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var viewCover: UIView!
    @IBOutlet var tvWeightTracker: UITableView!
    @IBOutlet var btEnterWeight: UIButton!
    @IBOutlet var viewAddWeight: UIView!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var lblDecimalWeight: UILabel!
    @IBOutlet var lblKgs: UILabel!
    @IBOutlet var lblBMI: UILabel!
    @IBOutlet var lblDecimalBMI: UILabel!
    @IBOutlet var ivBMI: UIImageView!
    @IBOutlet var ivTarget: UIImageView!
    @IBOutlet var ivBMIWaterLeft: UIImageView!
    @IBOutlet var lblWaterLeft: UILabel!
    @IBOutlet var tfAddIntakeValue: UITextField!
    @IBOutlet var btDate: UIButton!
    //edit view
    @IBOutlet var editViewMain: UIView!
    @IBOutlet var editViewCenter: UIView!
    @IBOutlet var lblEVHeader: UILabel!
    @IBOutlet var lblEVDate: UILabel!
    @IBOutlet var tfEVValue: UITextField!
    @IBOutlet var btEVSave: UIButton!
    @IBOutlet var btEVCancel: UIButton!
    
    
    
    //MARK:- constraints
    @IBOutlet var viewEnterWeightTopConst: NSLayoutConstraint!
    @IBOutlet var topViewTopConst: NSLayoutConstraint!
    @IBOutlet var lblKgsHSToWeight: NSLayoutConstraint!
    @IBOutlet var lblKgsHSToDecimalWeight: NSLayoutConstraint!
    @IBOutlet var lblWaterLeftHSToBMI: NSLayoutConstraint!
    @IBOutlet var lblWaterLeftHSToDecimalBMI: NSLayoutConstraint!
    
    //MARK:- variables
    var presenter = WeightTrackerPresenter(service: StatService())
    var arrayTracker : [TrackerModel]?
    let GRAPH_CELL  = "WeightGraphCell"
    let HEADER_CELL  = "HeaderCell"
    let WEIGHT_LOG_CELL  = "WeightLogCell"
    var stat_id : Int?
    var from : Int?
    var selectedDate : String?
    var datePicker: UIDatePicker?
    var toolbar: UIToolbar?
    var dateFormatter = DateFormatter()
    var isPickerOpen=false
    var screenName : String?
    
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
        
        //ShowDefaultImages(view: self.view).showDefaultImage(type: .No_Internet)
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.addStatusBarBG()
        viewAddWeight.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
    }
    override func viewWillDisappear(_ animated: Bool) {
        presenter.detachView()
    }
    
    //MARK:- Functions
    func setUp() {
        presenter.attachView(delegate: self)
        tvWeightTracker.delegate=self
        self.hideKeyboardWhenTappedAround()
        self.addInputAccessoryForTextFields(textFields: [tfAddIntakeValue],dismissable: true,previousNextable: false,showDone: true)
        self.addInputAccessoryForTextFields(textFields: [tfEVValue],dismissable: true,previousNextable: false,showDone: true)
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        if stat_id==1{
            lblHeader.text = "Weight Tracker"
        }else if stat_id==2{
            lblHeader.text = "Water Intake Tracker"
        }else{
            lblHeader.text = "Sleep Log"
        }
        tvWeightTracker.register(UINib(nibName: GRAPH_CELL, bundle: nil), forCellReuseIdentifier: GRAPH_CELL)
        tvWeightTracker.register(UINib(nibName: HEADER_CELL, bundle: nil), forCellReuseIdentifier: HEADER_CELL)
        tvWeightTracker.register(UINib(nibName: WEIGHT_LOG_CELL, bundle: nil), forCellReuseIdentifier: WEIGHT_LOG_CELL)
        tvWeightTracker.estimatedRowHeight = 400
        tvWeightTracker.delegate=self
        tvWeightTracker.dataSource=self
        presenter.callWeightTrackerWS(statId: "\(stat_id!)")
    }
    func animateBtEnterWeight(animateOut:Bool) {
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            if animateOut{
                self.viewAddWeight.transform = .identity
                self.btEnterWeight.transform = CGAffineTransform(translationX: -(self.view.frame.width), y: 0)
            }else{
                self.viewAddWeight.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
                self.btEnterWeight.transform = .identity
            }
            
        }, completion: nil)
        
    }
    func loadData() {
        setSelectedDate(showCurrentDate: true)
        viewCover.isHidden=true
        tvWeightTracker.tableFooterView = nil
        tvWeightTracker.reloadData()
        
        switch stat_id! {
        case 1:
            showWeightData()
            break
        case 2:
            showWaterIntakeData()
            break
        case 3:
            showSleepIntake()
            break
        default:
            break
            
        }
        
        
    }
    func showWeightData() {
        btEnterWeight.setTitle("Enter Your Weight", for: .normal)
        tfAddIntakeValue.placeholder = "Add Weight in Kgs"
        //for updating the stats in preffence
        if (arrayTracker?.count)!>2{
            if Calendar.current.isDateInToday(dateFormatter.date(from: (arrayTracker?[2].logModel?.log_date)!)!){
                var stats = Preffrences().getUserStats()
                stats?["weight"] = arrayTracker?[2].logModel?.log_val
                Preffrences().setUserStats(value: stats!)
            }
        }
        
        let target = arrayTracker![0].target!.round(to: 1)
        if String(target).components(separatedBy: ".")[1] == "0"{
            lblWeight.text = String(describing: abs(target))
            lblDecimalWeight.isHidden=true
            lblKgsHSToDecimalWeight.priority=255
            lblKgsHSToWeight.priority = 999
        }else{
            
            lblWeight.text = String(describing: abs(target)).components(separatedBy: ".")[0]
            lblDecimalWeight.text = "."+String(describing: target).components(separatedBy: ".")[1]
            lblDecimalWeight.isHidden=false
            lblKgsHSToWeight.priority = 255
            lblKgsHSToDecimalWeight.priority=999
        }
        if arrayTracker![0].target! < 0{
            lblKgs.text = "kgs\nto loose"
        }else{
            lblKgs.text = "kgs\nto maintain"
        }
        
        let dataLeft = arrayTracker![0].current!.round(to: 1)
        if String(dataLeft).components(separatedBy: ".")[1] == "0"{
            lblBMI.text = String(describing: abs(dataLeft))
            lblDecimalBMI.isHidden=true
        }else{
            lblBMI.text = String(describing: abs(dataLeft)).components(separatedBy: ".")[0]
            lblDecimalBMI.text = "."+String(describing: dataLeft).components(separatedBy: ".")[1]
            lblDecimalBMI.isHidden=false
        }
        
        ivTarget.image = #imageLiteral(resourceName: "ic_target")
        switch arrayTracker![0].wws_level ?? 1 {
        case 1:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_underweight")
            break
        case 2:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_normal")
            break
        case 3:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_overweight")
            break
        case 4:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_obese")
            break
        default:
            break
        }
    }
    func showWaterIntakeData()  {
        btEnterWeight.setTitle("Add Water Intake", for: .normal)
        tfAddIntakeValue.placeholder = "Add Water Intake"
        //for updating the stats in preffence
        if (arrayTracker?.count)!>2{
            if Calendar.current.isDateInToday(dateFormatter.date(from: (arrayTracker?[2].logModel?.log_date)!)!){
                var stats = Preffrences().getUserStats()
                stats?["waterIntake"] = arrayTracker?[2].logModel?.log_val
                Preffrences().setUserStats(value: stats!)
            }
        }
        let target = arrayTracker![0].target!.round(to: 1)
        if String(target).components(separatedBy: ".")[1] == "0"{
            
            if arrayTracker?.count==2{
                lblWeight.text = target == 0 ? "\((Double(Preffrences().getUserStats()?["weight"] ?? "0.0")! * 0.05))" : String(describing: abs(Int(target)))
            }else{
                
                lblWeight.text = "\(Int(target))"
            }
            lblDecimalWeight.isHidden=true
            lblKgsHSToDecimalWeight.priority=255
            lblKgsHSToWeight.priority = 999
        }else{
            lblWeight.text = String(describing: abs(target)).components(separatedBy: ".")[0]
            lblDecimalWeight.text = "."+String(describing: target).components(separatedBy: ".")[1]
            lblDecimalWeight.isHidden=false
            lblKgsHSToWeight.priority = 255
            lblKgsHSToDecimalWeight.priority=999
        }
        lblKgs.text = "Ltrs.\nto Drink"
        lblWaterLeft.isHidden=false
        lblWaterLeft.text = "Ltrs.\nDrank"
        let dataLeft = arrayTracker![0].current!.round(to: 1)
        if String(dataLeft).components(separatedBy: ".")[1] == "0"{
            lblBMI.text = String(describing: abs(Int(dataLeft)))
            lblDecimalBMI.isHidden=true
            lblWaterLeftHSToDecimalBMI.priority=255
            lblWaterLeftHSToBMI.priority = 999
        }else{
            lblBMI.text = String(describing: abs(dataLeft)).components(separatedBy: ".")[0]
            lblDecimalBMI.text = "."+String(describing: dataLeft).components(separatedBy: ".")[1]
            lblDecimalBMI.isHidden=false
            lblWaterLeftHSToBMI.priority=255
            lblWaterLeftHSToDecimalBMI.priority = 999
        }
        ivTarget.image = #imageLiteral(resourceName: "ic_water_jar")
        switch arrayTracker![0].wws_level ?? 1 {
        case 1:
            ivBMI.image = #imageLiteral(resourceName: "ic_water_glass_a")
            break
        case 2:
            ivBMI.image = #imageLiteral(resourceName: "ic_water_glass_b")
            break
        case 3:
            ivBMI.image = #imageLiteral(resourceName: "ic_water_glass_c")
            break
        case 4:
            ivBMI.image = #imageLiteral(resourceName: "ic_water_glass_d")
            break
        default:
            ivBMI.image = #imageLiteral(resourceName: "ic_water_glass_d")
            break
        }
    }
    func showSleepIntake() {
        btEnterWeight.setTitle("Add Your Sleeping Hours", for: .normal)
        tfAddIntakeValue.placeholder = "Add Sleeping Hrs"
        //        print(arrayTracker?[2].logModel?.log_date)
        
        //for updating the stats in preffence
        if (arrayTracker?.count)!>2{
            for i in (arrayTracker?[0].logs)!{
                if Calendar.current.isDateInYesterday(dateFormatter.date(from: (i.log_date)!)!){
                    var stats = Preffrences().getUserStats()
                    stats?["sleepLog"] = i.log_val
                    Preffrences().setUserStats(value: stats!)
                    break
                }
            }
            
        }
        
        let target = arrayTracker![0].target!.round(to: 1)
        if String(target).components(separatedBy: ".")[1] == "0"{
            if arrayTracker?.count == 2{
                lblWeight.text = target == 0 ? "8" :String(describing: abs(Int(target)))
            }else{
                lblWeight.text = String(describing: abs(Int(target)))
            }
            
            lblDecimalWeight.isHidden=true
            lblKgsHSToDecimalWeight.priority=255
            lblKgsHSToWeight.priority = 999
        }else{
            lblWeight.text = String(describing: abs(target)).components(separatedBy: ".")[0]
            lblDecimalWeight.text = "."+String(describing: target).components(separatedBy: ".")[1]
            lblDecimalWeight.isHidden=false
            lblKgsHSToWeight.priority = 255
            lblKgsHSToDecimalWeight.priority=999
        }
        lblKgs.text = "Hrs.\nto Sleep"
        lblWaterLeft.isHidden=false
        lblWaterLeft.text = "Hrs.\nSlept"
        let dataLeft = arrayTracker![0].current!.round(to: 1)
        if String(dataLeft).components(separatedBy: ".")[1] == "0"{
            lblBMI.text = String(describing: abs(Int(dataLeft)))
            lblDecimalBMI.isHidden=true
            lblWaterLeftHSToDecimalBMI.priority=255
            lblWaterLeftHSToBMI.priority = 999
        }else{
            lblBMI.text = String(describing: abs(dataLeft)).components(separatedBy: ".")[0]
            lblDecimalBMI.text = "."+String(describing: dataLeft).components(separatedBy: ".")[1]
            lblDecimalBMI.isHidden=false
            lblWaterLeftHSToBMI.priority=255
            lblWaterLeftHSToDecimalBMI.priority = 999
        }
        ivTarget.image = #imageLiteral(resourceName: "ic_sleep")
        ivBMI.image = #imageLiteral(resourceName: "ic_total_sleep")
    }
    func setSelectedDate(showCurrentDate:Bool)  {
        if showCurrentDate{
            if stat_id==3{
                btDate.setTitle(dateFormatter.string(from: Calendar.current.date(byAdding: .day, value: -1, to: Date())!), for: .normal)
            }else{
                btDate.setTitle(dateFormatter.string(from: Date()), for: .normal)
            }
            
            
        }else{
            btDate.setTitle(selectedDate ?? dateFormatter.string(from: Date()), for: .normal)
        }
        for i in arrayTracker!{
            if i.logs != nil {
                for j in i.logs!{
                    print(btDate.title(for: .normal)!)
                    if btDate.title(for: .normal)! == j.log_date!{
                        tfAddIntakeValue.text = j.log_val
                        return
                    }else{
                        tfAddIntakeValue.text = ""
                    }
                    
                }
            }
        }
    }
    func getDate()  {
        hidePickers()
        selectedDate = dateFormatter.string(from: (datePicker?.date)!)
        setSelectedDate(showCurrentDate: false)
    }
    func hidePickers()  {
        isPickerOpen=false
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY+235, width: self.view.frame.width, height: 35)
        }){
            
            finished in
            self.toolbar?.removeFromSuperview()
            self.datePicker?.removeFromSuperview()
        }
        
        
    }
    func openDatePicker(date:String) {
        //hidePickers()
        isPickerOpen=true
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200))
        datePicker?.backgroundColor = UIColor.white
        datePicker?.datePickerMode = .date
        datePicker?.date = dateFormatter.date(from: date)!
        datePicker?.minimumDate = Calendar.current.date(byAdding: .month, value: -2, to: Date())
        if (stat_id == 3){
            datePicker?.maximumDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        }else{
            datePicker?.maximumDate = Date()
        }
        
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.maxY+240, width: self.view.frame.width, height: 200))
        toolbar?.sizeToFit()
        toolbar?.isTranslucent=false
        toolbar?.barTintColor = Colors.themeColor
        toolbar?.tintColor = UIColor.white
        let doneButton = UIBarButtonItem(title: "done", style: .done, target: self, action:#selector(WeightTracker.getDate))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action:#selector(WeightTracker.hidePickers))
        toolbar?.setItems([cancelButton, spacer, doneButton], animated: true)
        self.view.addSubview(self.datePicker!)
        self.view.addSubview(self.toolbar!)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY-200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY-235, width: self.view.frame.width, height: 35)
        }, completion: nil)
        
    }
    func addFooterView(msg:String) -> UIView {
        let toastLabel = UILabel(frame:CGRect(x: 0,y:0,width: self.view.frame.width,height: 50))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.font = UIFont.systemFont(ofSize: 14)
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.numberOfLines = 0
        toastLabel.textAlignment = NSTextAlignment.center
        self.view.addSubview(toastLabel)
        toastLabel.text = msg
        toastLabel.clipsToBounds  =  true
        return toastLabel
    }
    func showEditDialog(date:String,value:String) {
        editViewMain.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        editViewMain.frame = view.bounds
        if stat_id == 1{
            lblEVHeader.text = "Edit Weight Log"
            tfEVValue.placeholder = "Enter your weight"
        }else if stat_id == 2{
            lblEVHeader.text = "Edit Water Intake"
            tfEVValue.placeholder = "Enter your water intake"
        }else if stat_id == 3{
            lblEVHeader.text = "Edit Sleep Log"
            tfEVValue.placeholder = "Enter sleep hours"
        }
        lblEVDate.text = date
        tfEVValue.text = value
        self.view.addSubview(editViewMain)
    }
    
    
    //MARK:- Button Actions
    @IBAction func btDate(_ sender: Any) {
        if !isPickerOpen{
            openDatePicker(date: btDate.title(for: .normal)!)
        }
    }
    @IBAction func btDone(_ sender: Any) {
        if !(tfAddIntakeValue.text?.isEmpty)!{
            self.view.endEditing(true)
            let params = ["user_id":Preffrences().getUserId(),
                          "stats_id":stat_id,
                          "log_of_on":"".getFormatedDate(fromFormat: "dd MMMM, yyyy", toFormat: "yyyy-MM-dd", dateString: btDate.title(for: .normal)!),
                          "log_val":tfAddIntakeValue.text!,
                          "screen_name":lblHeader.text,
                          "webservice":"Add/UpdateUserStats"] as [String : Any]
            presenter.callAddStatsWS(params: params)
        }
    }
    @IBAction func btEnterWeight(_ sender: Any) {
        animateBtEnterWeight(animateOut: true)
    }
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
    @IBAction func btEVSave(_ sender: Any) {
        
        if !(tfEVValue.text?.isEmpty)!{
            self.view.endEditing(true)
            self.editViewMain.removeFromSuperview()
            let params = ["user_id":Preffrences().getUserId(),
                          "stats_id":stat_id,
                          "log_of_on":"".getFormatedDate(fromFormat: "dd MMMM, yyyy", toFormat: "yyyy-MM-dd", dateString: lblEVDate.text!),
                          "log_val":tfEVValue.text ?? "",
                          "screen_name":lblHeader.text,
                          "webservice":"Add/UpdateUserStats"] as [String : Any]
            presenter.callAddStatsWS(params: params)
        }
        
    }
    @IBAction func btEVCancel(_ sender: Any) {
        self.editViewMain.removeFromSuperview()
    }
    
}

extension WeightTracker : WeightTrackerDelegate{
    func showIndicator(showIt: Bool, showTint:Bool) {
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        arrayTracker = successValue as? [TrackerModel]
        loadData()
    }
    func onError(messgae :Any?, errorValue: ErrorType) {
        //ShowDefaultImages(view: self.view).showDefaultImage(type: errorValue)
        
        if errorValue == ErrorType.NO_RESULT{
            var model = TrackerModel()
            var logModel : Logs?
            var arrayLogs=[Logs]()
            arrayTracker = [TrackerModel]()
            model.current = 0
            model.target = 0
            model.wws_level = 0
            model.isGraph=true
            for _ in 0 ... 11{
                logModel = Logs()
                logModel?.log_date = "0"
                logModel?.log_val = "0"
                arrayLogs.append(logModel!)
            }
            if model.logs == nil{
                model.logs = [Logs]()
                model.logs? = arrayLogs
            }
            arrayTracker?.append(model)
            //for header
            model = TrackerModel()
            model.isHeader = true
            arrayTracker?.append(model)
            loadData()
            tvWeightTracker.tableFooterView = addFooterView(msg: messgae as? String ?? "")
            
        }else{
            ShowDefaultImages.sharedInstance.showDefaultImage(view: self.view, type: errorValue,header: "",msg: messgae as! String){
                [weak self] finished in
                self?.presenter.callWeightTrackerWS(statId: "\(self?.stat_id!)")
                
            }
        }
    }
    func onStatAddedSuccess(success: Any?) {
        arrayTracker?.removeAll()
        arrayTracker = success as? [TrackerModel]
        animateBtEnterWeight(animateOut: false)
        loadData()
    }
    func onStatAddedError(messgae: String?, error: ErrorType?) {
        showToast(msg: messgae ?? "")
    }
}

//MARK:- Tableview Functions
extension WeightTracker : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTracker?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (arrayTracker?[indexPath.row].isGraph)!{
            let  cell = tableView.dequeueReusableCell(withIdentifier: GRAPH_CELL, for: indexPath) as? WeightGraphCell
            if stat_id == 1{
                cell?.headers = ["kgs", "Weight Summary"]
            }else if stat_id == 2{
                cell?.headers = ["Ltrs", "Water Intake Summary"]
            }else if stat_id == 3{
                cell?.headers = ["Hrs", "Sleep Hrs. Summary"]
            }
            cell?.data = arrayTracker?[indexPath.row].logs
            return cell ?? UITableViewCell()
        }else if (arrayTracker?[indexPath.row].isHeader)!{
            let cell = tableView.dequeueReusableCell(withIdentifier: HEADER_CELL, for: indexPath) as? HeaderCell
            
            if stat_id == 1{
                cell?.header = "Weight Log"
            }else if stat_id == 2{
                cell?.header = "Water Intake Summary"
            }else if stat_id == 3{
                cell?.header = "Sleep Hrs. Summary"
            }
            return cell ?? UITableViewCell()
        }else if (arrayTracker?[indexPath.row].isUsers)!{
            let  cell = tableView.dequeueReusableCell(withIdentifier: WEIGHT_LOG_CELL, for: indexPath) as? WeightLogCell
            cell?.weight = arrayTracker?[indexPath.row].logModel?.log_val
            cell?.date = arrayTracker?[indexPath.row].logModel?.log_date
            cell?.id = stat_id
            cell?.IS_CLICKED =  {
                [weak self] in
                let value = Double((self?.arrayTracker?[indexPath.row].logModel?.log_val)!)?.round(to: 1)
                self?.showEditDialog(date: (self?.arrayTracker?[indexPath.row].logModel?.log_date)!, value: "\((value ?? 0.0))")
            }
            return cell ?? UITableViewCell()
        }
        return  UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (arrayTracker?[indexPath.row].isGraph)!{
            return 300
        }else{
            
            return UITableViewAutomaticDimension
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
