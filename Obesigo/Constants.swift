//
//  Constants.swift
//  Obesigo
//
//  Created by Anuj  on 03/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
struct Constants {
    static let TESTIMONIAL_ADDED = "TestimonialAdded"
    static let REMINDER_ADDED = "ReminderAdded"
    static let PROFILE_EDITED = "ProfileEdited"
    static let MOBILE_EDITED = "MobileEdited"
    static var OTP = "otp"
    static var EXERCISE_ARRAY = "exerciseArray"
    static var FOOD_ARRAY = "foodArray"
    static var CAL_BURNT = "calorieBurnt"
    static var CAL_CONSUMNE = "calorieConsumed"
    static var HOME_ARRAY : [HomeModel]?
    static var SHOW_BUYNOW_ALERT = true
}
