//
//  AnimateKeyboard.swift
//  Obesigo
//
//  Created by Anuj  on 11/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit

class AnimateKeyboard {
    
    var scrollView: UIScrollView?
    var view: UIViewController?
    
    init(scrollView: UIScrollView, view: UIViewController) {
        self.scrollView = scrollView
        self.view = view
        registerKeyboardEvents()
    }
    
    func registerKeyboardEvents()  {
        NotificationCenter.default.addObserver(self, selector: #selector(AnimateKeyboard.keyboardIsShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AnimateKeyboard.keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    @objc func keyboardIsShown(notification: NSNotification)  {
        
        var userInfo = notification.userInfo!
        // swiftlint:disable force_cast
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        // swiftlint:enable force_cast
        keyboardFrame = (self.view?.view.convert(keyboardFrame, from: nil))!
        var contentInset: UIEdgeInsets = self.scrollView!.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView?.contentInset = contentInset
        
    }
    
    @objc func keyBoardWillHide() {
        let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView?.contentInset = contentInset
    }
    
    
}
