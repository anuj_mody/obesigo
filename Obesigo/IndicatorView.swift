//
//  IndicatorView.swift
//  Obesigo
//
//  Created by Anuj  on 13/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit

class IndicatorView {
    
    static let sharedInstance = IndicatorView()
    var activityIndicator : UIActivityIndicatorView?
    var view: UIView?
    
    private init(){}
    
    
    func showActivityIdicator(mainView: UIView, showTint: Bool = false, isObtrusive: Bool = true)  {
        if isObtrusive{
           UIApplication.shared.beginIgnoringInteractionEvents()
        }else{
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.hidesWhenStopped=true
        activityIndicator?.startAnimating()
        if showTint{
            view = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            view?.center = mainView.center
            view?.backgroundColor = Colors.themeColor.withAlphaComponent(0.6)
            view?.layer.cornerRadius = 5
            mainView.addSubview(view!)
            activityIndicator?.center = CGPoint(x: (view?.bounds.size.width)!/2, y: (view?.bounds.size.height)!/2)
            view?.addSubview(activityIndicator!)
        }else{
            activityIndicator?.color=Colors.themeColor
            activityIndicator?.center = mainView.center
            mainView.addSubview(activityIndicator!)
        }
    }
    func hideActivityIndicator()  {
        UIApplication.shared.endIgnoringInteractionEvents()
        activityIndicator?.stopAnimating()
        activityIndicator?.removeFromSuperview()
        view?.removeFromSuperview()
        view=nil
        activityIndicator=nil
    }
    
    deinit {
        print("Activity indicator deinitialize")
    }
    
}
