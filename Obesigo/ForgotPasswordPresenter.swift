//
//  ForgotPasswordPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 02/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
protocol ForgotPasswordDelegate : CommonDelegate {
    func showEmailError(message:String)
}
class ForgotPasswordPresenter {
    weak var delegate : ForgotPasswordDelegate?
    var service : ForgotPasswordService?
    init(service:ForgotPasswordService) {
        self.service=service
    }
    func attachView(delegate:ForgotPasswordDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func checkEmail(email:String)  {
        if service?.checkEmail(email: email) != nil{
            delegate?.showEmailError(message: (service?.checkEmail(email: email))!)
        }
    }
    func callForgotPasswordWS(email:String) {
        if service?.checkEmail(email: email) != nil{
            delegate?.showEmailError(message: (service?.checkEmail(email: email))!)
            return
        }
        delegate?.showIndicator(showIt: true, showTint: true)
        let params : [String:Any] = [
            "email":email,
            "screen_name":"Forgot Password",
            "webservice":"Reset Password"
        ]
        service?.callForgotPasswordWS(params: params, result: {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as? String ?? "", errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        })
    }
}
class ForgotPasswordService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func checkEmail(email:String) -> String?  {
        if email.isEmpty{
            return ErrorType.EMPTY_EMAIL.rawValue
        }
        
        if !email.isValidEmail(){
            return ErrorType.INVALID_EMAIL.rawValue
        }
        return nil
    }
    func callForgotPasswordWS(params: [String:Any], result: @escaping onResponseReceived){
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.RESET_PASSWORD, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "status") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, nil)
            }
        }
    }
    
}

