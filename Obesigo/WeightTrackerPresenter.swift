//
//  WeightTrackerPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 26/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol WeightTrackerDelegate:CommonDelegate {
    func onStatAddedSuccess(success:Any?)
    func onStatAddedError(messgae:String?,error:ErrorType?)
}

class WeightTrackerPresenter {
    weak var delegate:WeightTrackerDelegate?
    var service:StatService?
    init(service:StatService) {
        self.service = service
    }
    func attachView(delegate:WeightTrackerDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callWeightTrackerWS(statId:String)  {
        
        delegate?.showIndicator(showIt: true, showTint: false)
        service?.callGetStatsWS(stat_id: statId, result: {
            [weak self](success, error) in
            
            if error != nil{
                self?.delegate?.onError(messgae: success ?? "", errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: false)
        })
    }
    func callAddStatsWS(params:[String:Any])  {
        delegate?.showIndicator(showIt: true, showTint: true)
        service?.callAddStatsWS(params: params, result: { [weak self](success, error) in
            
            if error != nil{
                self?.delegate?.onStatAddedError(messgae: success as? String ?? ErrorType.Something_Went_Wrong.rawValue, error: error)
            }else{
                self?.delegate?.onStatAddedSuccess(success: success)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: false)
        })
        
    }
    
}

class StatService{
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callGetStatsWS(stat_id:String,result: @escaping onResponseReceived) {
        var screenName :String?
        if stat_id == "1"{
            screenName = "Weight Tracker"
        }else if stat_id == "2"{
            screenName = "Water Intake"
        }else if stat_id == "3"{
            screenName = "Sleep Log"
        }else{
            screenName = "Calories Tracker"
        }
        let url = NetworkUrls.USER_STAT + "?screen_name=\(screenName!)&webservice=Get User Stats Log&user_id=\(Preffrences().getUserId())&stats_id=\(stat_id)"
        NetworkRequest.sharedInstance.callWebService(url: url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, type: .GET, params: nil) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            var array = [TrackerModel]()
            var model : TrackerModel?
            var logModel : Logs?
            var arrayLogs=[Logs]()
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                if (success as? NSDictionary)?.object(forKey: "status_code") as? Int == 404{
                    result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.NO_RESULT)
                }else{
                    result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.Something_Went_Wrong)
                }
                
            }else{
                if stat_id == "4"{
                    model = TrackerModel()
                    model?.current = Double(((((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "current") as? String)!)
                    model?.target = Double(((((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "target") as? String)!)
                    model?.isGraph=true
                    for i in (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "logs") as! NSArray{
                        logModel = Logs()
                        logModel?.log_date = (i as! NSDictionary).object(forKey: "log_date") as? String
                        logModel?.cal_burnt = (i as! NSDictionary).object(forKey: "cal_burnt") as? String
                        logModel?.cal_consumed = (i as! NSDictionary).object(forKey: "cal_consumed") as? String
                        arrayLogs.append(logModel!)
                    }
                    
                    if model?.logs == nil{
                        model?.logs = [Logs]()
                        model?.logs? = arrayLogs
                    }
                    array.append(model!)
                    
                    //for header
                    model = TrackerModel()
                    model?.isHeader = true
                    array.append(model!)
                    
                    //for users
                    for j in arrayLogs{
                        model = TrackerModel()
                        model?.isUsers = true
                        model?.logModel = j
                        array.append(model!)
                    }
                    result(array as AnyObject, nil)
                }else{
                    model = TrackerModel()
                    model?.current = (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "current") as? Double
                    model?.target = (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "target") as? Double
                    model?.wws_level = (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "wws_level") as? Int
                    model?.isGraph=true
                    for i in (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "logs") as! NSArray{
                        logModel = Logs()
                        logModel?.id = (i as! NSDictionary).object(forKey: "id") as? Int
                        logModel?.log_date = (i as! NSDictionary).object(forKey: "log_date") as? String
                        logModel?.log_of_on = (i as! NSDictionary).object(forKey: "log_of_on") as? String
                        logModel?.log_val = (i as! NSDictionary).object(forKey: "log_val") as? String
                        logModel?.stats_id = (i as! NSDictionary).object(forKey: "stats_id") as? String
                        logModel?.user_id = (i as! NSDictionary).object(forKey: "user_id") as? String
                        logModel?.height = (i as! NSDictionary).object(forKey: "height") as? String
                        arrayLogs.append(logModel!)
                    }
                    if model?.logs == nil{
                        model?.logs = [Logs]()
                        model?.logs? = arrayLogs
                    }
                    array.append(model!)
                    
                    //for header
                    model = TrackerModel()
                    model?.isHeader = true
                    array.append(model!)
                    
                    //for users
                    for j in arrayLogs{
                        model = TrackerModel()
                        model?.isUsers = true
                        model?.logModel = j
                        array.append(model!)
                    }
                    result(array as AnyObject, nil)
                }
            }
        }
    }
    func callAddStatsWS(params:[String:Any],result: @escaping onResponseReceived)  {
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.USER_STAT, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            var array = [TrackerModel]()
            var model : TrackerModel?
            var logModel : Logs?
            var arrayLogs=[Logs]()
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                
                model = TrackerModel()
                model?.current = (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "current") as? Double
                model?.target = (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "target") as? Double
                model?.wws_level = (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "wws_level") as? Int
                model?.isGraph=true
                for i in (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "logs") as! NSArray{
                    logModel = Logs()
                    logModel?.id = (i as! NSDictionary).object(forKey: "id") as? Int
                    logModel?.log_date = (i as! NSDictionary).object(forKey: "log_date") as? String
                    logModel?.log_of_on = (i as! NSDictionary).object(forKey: "log_of_on") as? String
                    logModel?.log_val = (i as! NSDictionary).object(forKey: "log_val") as? String
                    logModel?.stats_id = (i as! NSDictionary).object(forKey: "stats_id") as? String
                    logModel?.user_id = (i as! NSDictionary).object(forKey: "user_id") as? String
                    logModel?.height = (i as! NSDictionary).object(forKey: "height") as? String
                    arrayLogs.append(logModel!)
                }
                if model?.logs == nil{
                    model?.logs = [Logs]()
                    model?.logs? = arrayLogs
                }
                array.append(model!)
                
                //for header
                model = TrackerModel()
                model?.isHeader = true
                array.append(model!)
                
                //for users
                for j in arrayLogs{
                    model = TrackerModel()
                    model?.isUsers = true
                    model?.logModel = j
                    array.append(model!)
                }
                result(array as AnyObject, nil)
            }
        }
        
    }
}
