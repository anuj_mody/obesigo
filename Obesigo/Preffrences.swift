//
//  UserDefaults.swift
//  Obesigo
//
//  Created by Anuj  on 13/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

struct Preffrences {
    let USER_ID = "userId"
    let AUTH_TOKEN = "authToken"
    let USER_ARRAY = "userArray"
    let REMINDER_ARRAY = "reminderArray"
    let NAME = "name"
    let STATS = "stats"
    let PASSWORD = "password"
    let PROFILE_PIC = "profile_pic"
    let OTP_ENTERED = "otp_entered"
    let BMI_DONE = "bmi_done"
    let DIETARY_PREF_DONE = "dietary_pref_done"
    
    func setAuthToken(token:String) {
        UserDefaults.standard.set(token, forKey: AUTH_TOKEN)
    }
    func getAuthToken() -> String {
        guard let value = UserDefaults.standard.object(forKey: AUTH_TOKEN) else {
            return ""
        }
        return value as! String
    }
    
    func setUserId(id:String) {
        UserDefaults.standard.set(id, forKey: USER_ID)
    }
    func getUserId() -> String {
        guard let value = UserDefaults.standard.object(forKey: USER_ID) else {
            return ""
        }
        return value as! String
    }
    
    func setName(value:String) {
        UserDefaults.standard.set(value, forKey: NAME)
    }
    func getName() -> String {
        guard let value = UserDefaults.standard.object(forKey: NAME) else {
            return ""
        }
        return value as! String
    }
    
    func setPassword(value:String) {
        UserDefaults.standard.set(value, forKey: PASSWORD)
    }
    func getPassword() -> String {
        guard let value = UserDefaults.standard.object(forKey: PASSWORD) else {
            return ""
        }
        return value as! String
    }
    
    func setProfilePic(value:String) {
        UserDefaults.standard.set(value, forKey: PROFILE_PIC)
    }
    func getProfilePic() -> String {
        guard let value = UserDefaults.standard.object(forKey: PROFILE_PIC) else {
            return ""
        }
        return value as! String
    }
    
    func setOTPEntered(value:Bool) {
        UserDefaults.standard.set(value, forKey: OTP_ENTERED)
    }
    func getOTPEntered() -> Bool {
        guard let value = UserDefaults.standard.object(forKey: OTP_ENTERED) else {
            return false
        }
        return value as! Bool
    }
    
    func setBMIDone(value:Bool) {
        UserDefaults.standard.set(value, forKey: BMI_DONE)
    }
    func getBMIDone() -> Bool {
        guard let value = UserDefaults.standard.object(forKey: BMI_DONE) else {
            return false
        }
        return value as! Bool
    }
    
    func setDietaryPrefDone(value:Bool) {
        UserDefaults.standard.set(value, forKey: DIETARY_PREF_DONE)
    }
    func getDietaryPrefDone() -> Bool {
        guard let value = UserDefaults.standard.object(forKey: DIETARY_PREF_DONE) else {
            return false
        }
        return value as! Bool
    }
    
    func setUserArray(array:[User])  {
        let users = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(users, forKey: USER_ARRAY)
    }
    func getUserArray() -> [User]? {
        let users = UserDefaults.standard.object(forKey: USER_ARRAY) as? NSData
        if let users = users {
            return NSKeyedUnarchiver.unarchiveObject(with: users as Data) as? [User]
        }
        return nil
    }
    
    func setUserStats(value:[String:String]) {
        UserDefaults.standard.set(value, forKey: STATS)
    }
    func getUserStats() -> [String:String]? {
        guard let value = UserDefaults.standard.object(forKey: STATS) else {
            return nil
        }
        return value as? [String:String]
    }
    
    func setReminderArray(array:[RemindersModel])  {
        let reminders = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(reminders, forKey: REMINDER_ARRAY)
    }
    func getReminderArray() -> [RemindersModel]? {
        let reminders = UserDefaults.standard.object(forKey: REMINDER_ARRAY) as? NSData
        if let reminders = reminders {
            return NSKeyedUnarchiver.unarchiveObject(with: reminders as Data) as? [RemindersModel]
        }
        return nil
    }
    
    func clearDefaults()  {
        
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        
    }
    
    
    
}
