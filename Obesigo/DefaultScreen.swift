//
//  DefaultScreen.swift
//  Obesigo
//
//  Created by Anuj  on 28/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit
class DefaultScreen : UIView{
    
    @IBOutlet var ivImage: UIImageView!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var lblMsg: UILabel!
    
    var clickRegistered : (()->Void)? = nil
    
    override func awakeFromNib() {
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DefaultScreen.registerClick)))
    }

    
    func registerClick() {
        if self.clickRegistered != nil {
            self.clickRegistered!()
        }
    }
}
