//
//  Extensions.swift
//  Obesigo
//
//  Created by Anuj  on 11/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func addPlaceHolder(mainView: UIView, placeHolderText: String) -> UILabel {
        let label = UILabel()
        label.text = placeHolderText
        label.font = label.font.withSize(14)
        label.textColor = Colors.hintColor
        label.translatesAutoresizingMaskIntoConstraints=false
        mainView.addSubview(label)
        var leadingAnchorConst: NSLayoutConstraint?
        var centerConst: NSLayoutConstraint?
        
        if #available(iOS 9.0, *) {
            leadingAnchorConst = label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant:0)
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 9.0, *) {
            centerConst =  label.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        } else {
            // Fallback on earlier versions
        }
        NSLayoutConstraint.activate([leadingAnchorConst!, centerConst!])
        return label
    }
}
extension UIViewController{
    func addStatusBarBG(){
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size:CGSize(width: self.view.frame.width, height:20))
        let view : UIView = UIView.init(frame: rect)
        view.backgroundColor = UIColor.init(red: 194/255, green: 31/255, blue: 37/255, alpha: 1) //Replace value with your required background color
        self.view?.addSubview(view)
        let tintView = UIView.init(frame: rect)
        tintView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        self.view?.addSubview(tintView)
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    func showActionSheet(data: [String], onClicked : @escaping (String) -> (Void)) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.view.tintColor = Colors.themeColor
        for i in 0..<data.count {
            
            let action = UIAlertAction(title: data[i], style: .default) { (_) in
                onClicked(data[i])
            }
            actionSheet.addAction(action)
        }
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    func addInputAccessoryForTextFields(textFields: [UITextField], dismissable: Bool = true, previousNextable: Bool = true,showDone : Bool = true) {
        
        for (index, textField) in textFields.enumerated() {
            
            let toolbar: UIToolbar = UIToolbar()
            toolbar.sizeToFit()
            
            var items = [UIBarButtonItem]()
            if previousNextable {
                
                let previousButton = UIBarButtonItem(title: "previous", style: .plain, target: nil, action: nil)
                previousButton.tintColor = Colors.themeColor
                if textField == textFields.first {
                    previousButton.isEnabled = false
                } else {
                    previousButton.target = textFields[index - 1]
                    previousButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                _ = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                let nextButton = UIBarButtonItem(title: "next", style: .plain, target: nil, action: nil)
                nextButton.tintColor = Colors.themeColor
                if textField == textFields.last {
                    nextButton.isEnabled = false
                } else {
                    nextButton.target = textFields[index + 1]
                    nextButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                items.append(contentsOf: [previousButton,nextButton])
            }
            
            let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            if(showDone){
                let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
                doneButton.target = textFields[index]
                doneButton.action = #selector(UITextField.resignFirstResponder)
                doneButton.tintColor = Colors.themeColor
                items.append(contentsOf: [spacer, doneButton])
            }else{
                
                items.append(contentsOf: [spacer])
            }
            
            toolbar.barTintColor = UIColor.white
            toolbar.setItems(items, animated: false)
            textField.inputAccessoryView = toolbar
        }
    }
    func addInputAccessoryForTextViews(textFields: [UITextView], dismissable: Bool = true, previousNextable: Bool = true,showDone : Bool = true) {
        
        for (index, textField) in textFields.enumerated() {
            
            let toolbar: UIToolbar = UIToolbar()
            toolbar.sizeToFit()
            
            var items = [UIBarButtonItem]()
            if previousNextable {
                
                let previousButton = UIBarButtonItem(title: "previous", style: .plain, target: nil, action: nil)
                previousButton.tintColor = Colors.themeColor
                if textField == textFields.first {
                    previousButton.isEnabled = false
                } else {
                    previousButton.target = textFields[index - 1]
                    previousButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                _ = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                let nextButton = UIBarButtonItem(title: "next", style: .plain, target: nil, action: nil)
                nextButton.tintColor = Colors.themeColor
                if textField == textFields.last {
                    nextButton.isEnabled = false
                } else {
                    nextButton.target = textFields[index + 1]
                    nextButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                items.append(contentsOf: [previousButton,nextButton])
            }
            
            let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            if(showDone){
                let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
                doneButton.target = textFields[index]
                doneButton.action = #selector(UITextField.resignFirstResponder)
                doneButton.tintColor = Colors.themeColor
                items.append(contentsOf: [spacer, doneButton])
            }else{
                
                items.append(contentsOf: [spacer])
            }
            
            toolbar.barTintColor = UIColor.white
            toolbar.setItems(items, animated: false)
            textField.inputAccessoryView = toolbar
        }
    }
    func getViewControllerFromStoryBoard(storyBoardName:String = "",identifier:String = "") -> UIViewController{
        let storyBoard = UIStoryboard(name: storyBoardName, bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier:identifier)
    }
    func switchViewController(viewController:UIViewController?){
        guard let vc = viewController else { return  }
        if self.navigationController == nil {
            self.present(vc, animated: true, completion: nil)
        }else{
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func showAlert(title:String,message:String){
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alertVC, animated: true, completion: nil)
    }
    func showToast(msg:String,height:CGFloat = 0)  {
        let toastLabel = UILabel(frame:CGRect(x: 0,y:0 + height,width: self.view.frame.width,height: 30))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(1)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont.systemFont(ofSize: 12)
        toastLabel.textAlignment = NSTextAlignment.center
        self.view.addSubview(toastLabel)
        toastLabel.text = msg
        toastLabel.alpha = 1.0
        //toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 6.0, animations: {
            toastLabel.alpha = 0.0
            //toastLabel.removeFromSuperview()
        })
    }
}
extension UILabel{
    func animatePlaceHolder(animateUp: Bool=true)  {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            if animateUp{
                self.font = self.font.withSize(12)
                self.textColor = Colors.themeColor
                self.transform = CGAffineTransform(translationX: -20, y: -25)
            }else{
                self.font = self.font.withSize(14)
                self.textColor = Colors.hintColor
                self.transform = .identity
            }
        }, completion: nil)
    }
}

extension Date{
    func getFormattedDate(formatterString: String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = formatterString
        //guard let dateString = formatter.string(from: self) else { return nil }
        return formatter.string(from: self)
    }
}

extension String {
    func isValidEmail() -> Bool {
        //        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        //        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        //        return emailTest.evaluate(with: self)
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }
    func getFormatedDate(fromFormat:String,toFormat:String,dateString:String) -> String {
        
        var formater = DateFormatter()
        formater.dateFormat = fromFormat
        let date = formater.date(from: dateString)
        formater=DateFormatter()
        formater.dateFormat=toFormat
        
        
        //        guard let _ = date else {
        //            return "Today"
        //        }
        //        if Calendar.current.isDateInYesterday(date!){
        //            return "Yesterday"
        //        }else if Calendar.current.isDateInToday(date!){
        //            return "Today"
        //        }else{
        //            return formater.string(from: date!)
        //        }
        
        if date == nil{
            return ""
        }else{
            return formater.string(from: date!)
        }
    }
    
    func getFormatedDateUTC(fromFormat:String,toFormat:String,dateString:String) -> String {
        
        var formater = DateFormatter()
        formater.timeZone = TimeZone(abbreviation: "UTC")
        formater.dateFormat = fromFormat
        let date = formater.date(from: dateString)
        formater=DateFormatter()
        formater.dateFormat=toFormat
        
        if date == nil{
            return ""
        }else{
            return formater.string(from: date!)
        }
    }
    
    
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}

extension Dictionary {
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    func printJson() {
        print(json)
    }
}

//extension UIViewController {
//    func setTabBarVisible(visible:Bool, duration: TimeInterval, animated:Bool) {
//        if (tabBarIsVisible() == visible) { return }
//        let frame = self.tabBar.frame
//        let height = frame.size.height
//        let offsetY = (visible ? -height : height)
//        // animation
//        UIView.animate(withDuration: animated ? duration : 0.0) {
//            self.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY)
//            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height + offsetY)
//            self.view.setNeedsDisplay()
//            self.view.layoutIfNeeded()
//
//        }
//    }
//
//    func tabBarIsVisible() ->Bool {
//        return self.tabBar.frame.origin.y < self.view.frame.maxY
//    }
//}
