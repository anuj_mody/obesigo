//
//  EditProfile.swift
//  Obesigo
//
//  Created by Anuj  on 31/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit
import Kingfisher

class Profile: BaseVC {
    //MARK:- outlets
    @IBOutlet var ivProfilePic: UIImageView!
    @IBOutlet var viewWeight: UIView!
    @IBOutlet var viewHeight: UIView!
    @IBOutlet var viewBMI: UIView!
    @IBOutlet var lblFName: UILabel!
    @IBOutlet var lblLName: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblDOB: UILabel!
    @IBOutlet var lblGender: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var lblHeight: UILabel!
    @IBOutlet var lblBMI: UILabel!
    @IBOutlet var ivBMI: UIImageView!
    @IBOutlet var ivMedHistory: UIImageView!
    @IBOutlet var lblMedHistory: UILabel!
    @IBOutlet var ivDietaryPref: UIImageView!
    @IBOutlet var lblDietaryPref: UILabel!
    @IBOutlet var ivCover: UIImageView!
    @IBOutlet var viewMedHistory: UIView!
    @IBOutlet var tvMedHistory: UITableView!
    @IBOutlet var lblNumber: UILabel!
    @IBOutlet var lbName: UILabel!
    
    
    //MARK:- variables
    var presenter = ProfilePresenter(service: ProfileService())
    var arrayProfile : [User]?
    var medHistoryCount = 0
    var medConditions : [String]?
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- functions
    func setUp()  {
        presenter.attachView(delegate: self)
        viewHeight.layer.borderWidth=1
        viewWeight.layer.borderColor = UIColor.white.cgColor
        viewBMI.layer.borderColor = UIColor.white.cgColor
        viewHeight.layer.borderColor = UIColor.white.cgColor
        tvMedHistory.register(UINib(nibName: String(describing: MedHistoryCell.self), bundle: nil), forCellReuseIdentifier:  String(describing: MedHistoryCell.self))
        tvMedHistory.delegate=self
        tvMedHistory.dataSource=self
        tvMedHistory.estimatedRowHeight=50
        tvMedHistory.rowHeight = UITableViewAutomaticDimension
        presenter.callGetProfileWS()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constants.PROFILE_EDITED), object: nil, queue: nil){
            [weak self] (noti) in
            self?.arrayProfile?.removeAll()
            self?.arrayProfile = (noti.userInfo as NSDictionary?)?.object(forKey: "data") as? [User]
            self?.loadData()
        }
        
    }
    func loadData() {
        guard arrayProfile?[0] != nil else {
            return
        }
        lblFName.text = arrayProfile?[0].fName
        lblLName.text = arrayProfile?[0].lName
        lblEmail.text = arrayProfile?[0].email
        lblDOB.text = "".getFormatedDate(fromFormat: "yyyy-MM-dd", toFormat: "dd MMMM, yyyy", dateString: (arrayProfile?[0].dob)!)
        lblGender.text = arrayProfile?[0].gender_lu_id == 1 ? "Male" : "Female"
        lblWeight.text = (arrayProfile?[0].arrStats?[0].weight)! + " kgs"
        //lblHeight.text = (arrayProfile?[0].height)!+"\""
        if arrayProfile?[0].heightUnitId == "1"{
            lblHeight.text = "\((arrayProfile?[0].height)!) mtr"
        }else if arrayProfile?[0].heightUnitId == "2"{
            let cm = Double((arrayProfile?[0].height)!)!.round(to: 2)
            lblHeight.text = "\(cm * 100) cm"
        }else{
            let feet = (Double((arrayProfile?[0].height!)!)! / 0.3048).round(to: 1)
            lblHeight.text = "\(String(feet).components(separatedBy: ".")[0]) ft \(String(feet).components(separatedBy: ".")[1]) inch"
        }
        lbName.text = lblFName.text! + " " + lblLName.text!
        lblNumber.text = arrayProfile?[0].mobile ?? ""
        calculateBMI()
        
        medHistoryCount = 0
        for i in (arrayProfile?[0].arrMedHistory)!{
            if i.has_user == 1{
                if medConditions == nil{
                    medConditions = [String]()
                }
                medHistoryCount += 1
                medConditions?.append(i.name!)
            }
        }
        if medHistoryCount > 0 {
            ivMedHistory.image = #imageLiteral(resourceName: "ic_medical_history_yes")
            lblMedHistory.text = "\(medHistoryCount)"
        }else{
            ivMedHistory.image = #imageLiteral(resourceName: "ic_medical_history_no")
            lblMedHistory.text = "0"
        }
        
        if arrayProfile?[0].diet_preference_id == 1{
            ivDietaryPref.image = #imageLiteral(resourceName: "ic_veg_profile")
            lblDietaryPref.text = "Veg"
        }else{
            ivDietaryPref.image = #imageLiteral(resourceName: "ic_nonveg_profile")
            lblDietaryPref.text = "Non-Veg"
        }
        
        
        if let imageUrl = URL(string: (arrayProfile?[0].profile_photo ?? "")!){
            ivProfilePic.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "im_default_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        }else{
            //ivProfilePic.kf.setImage(with: <#T##Resource?#>, placeholder: <#T##Image?#>, options: <#T##KingfisherOptionsInfo?#>, progressBlock: <#T##DownloadProgressBlock?##DownloadProgressBlock?##(Int64, Int64) -> ()#>, completionHandler: <#T##CompletionHandler?##CompletionHandler?##(Image?, NSError?, CacheType, URL?) -> ()#>)
            ivProfilePic.image = #imageLiteral(resourceName: "im_default_profile")
        }
        
        
        ivCover.isHidden=true
    }
    func calculateBMI()  {
        let BMIvalue =  Double((arrayProfile?[0].arrStats?[0].weight)!)! / (Double((arrayProfile?[0].height!)!)! * Double((arrayProfile?[0].height)!)!)
        lblBMI.text = String(round((BMIvalue*1000)/1000))
        switch BMIvalue {
        //Underweight
        case 0...18.5:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_underweight")
            break
        //Normal
        case 18.5...24.9:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_normal")
            break
        //Overweight
        case 24.9...29.9:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_overweight")
            break
        //Obese
        case 30...50:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_obese")
            break
        case 50...BMIvalue:
            ivBMI.image = #imageLiteral(resourceName: "ic_bmi_obese")
            break
        default:
            break
        }
    }
    func showMedHistory()  {
        tvMedHistory.reloadData()
        viewMedHistory.frame = self.view.bounds
        viewMedHistory.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(viewMedHistory)
    }
    
    //MARK:- Button Actions
    @IBAction func btMedHistory(_ sender: Any) {
        
        if medHistoryCount > 0{
            showMedHistory()
        }
        
    }
    @IBAction func btOK(_ sender: Any) {
        viewMedHistory.removeFromSuperview()
    }
}

extension Profile : ProfileDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        arrayProfile = successValue as? [User]
        loadData()
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        ShowDefaultImages.sharedInstance.showDefaultImage(view: self.view, type: errorValue,header: "",msg: "",fromTop: 0,reduceHeight: false){
            [weak self] finished in
            
            
            
        }
    }
}

//MARK:- Tableview Functions
extension Profile : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medConditions?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MedHistoryCell.self), for: indexPath) as? MedHistoryCell
        cell?.data = medConditions?[indexPath.row]
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}


