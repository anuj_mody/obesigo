//
//  ReadMore.swift
//  Obesigo
//
//  Created by Anuj  on 08/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class ReadMore: BaseVC {
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        self.addStatusBarBG()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
    //MARK:- Button Actions
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
}
