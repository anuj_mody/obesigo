//
//  CaloriesBurntPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 24/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol CaloriesBurntDelegate : CommonDelegate {
    func onCalAddedSuccess(msg:Any?)
    func onCalAddedError(msg:String?,error:ErrorType?)
}

class CaloriesBurntPresenter {
    weak var delegate : CaloriesBurntDelegate?
    var service : CaloriesBurntService
    init(service:CaloriesBurntService) {
        self.service=service
    }
    func attachView(delegate:CaloriesBurntDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callGetCaloriesBurntWS(date:String) {
        delegate?.showIndicator(showIt: true, showTint: false)
        service.callGetCaloriesBurntWS(date: date) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as Any, errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
    func callAddCalBurntWS(array : [CalBurntModel],date:String) {
        delegate?.showIndicator(showIt: true, showTint: false)
        service.callAddCalBurntWS(array: array, date: date) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onCalAddedError(msg: error?.rawValue, error: error)
            }else{
                self?.delegate?.onCalAddedSuccess(msg: success as Any)
                
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
}

class CaloriesBurntService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callGetCaloriesBurntWS(date:String,result: @escaping onResponseReceived) {
        let url = "\(NetworkUrls.CALORIES_BURNT)?screen_name=Calories Burnt&webservice=Get User Calories Burnt Details&user_id=\(Preffrences().getUserId())&for_date=\("".getFormatedDate(fromFormat: "dd MMMM, yyyy", toFormat: "yyyy-MM-dd", dateString: date))"

        NetworkRequest.sharedInstance.callWebService(url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, type: .GET, params: nil) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            var model : CaloriesBurntModel?
            var array = [CaloriesBurntModel]()
            var arrCalModel : [CalBurntModel]?
            var calBurntModel : CalBurntModel?
            var exerciseModel:ExerciseModel?
            var arrayExercise : [ExerciseModel]?
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                if (success as? NSDictionary)?.object(forKey: "status_code") as? Int == 404{
                    model = CaloriesBurntModel()
                    model?.calories_sum = ((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "calories_sum") as? String
                    //for all exercise
                    for (pos,i) in (((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "all_exercise") as! NSArray).enumerated(){
                        exerciseModel = ExerciseModel()
                        exerciseModel?.id = (i as? NSDictionary)?.object(forKey: "id") as? String
                        exerciseModel?.exercise_name = (i as? NSDictionary)?.object(forKey: "exercise_name") as? String
                        exerciseModel?.duration_count = (i as? NSDictionary)?.object(forKey: "duration_count") as? String
                        exerciseModel?.calories_burnt = (i as? NSDictionary)?.object(forKey: "calories_burnt") as? String
                        exerciseModel?.unit_lu_id = (i as? NSDictionary)?.object(forKey: "unit_lu_id") as? String
                        exerciseModel?.units = (i as? NSDictionary)?.object(forKey: "units") as? String
                        exerciseModel?.index = pos
                        if arrayExercise == nil{
                            arrayExercise = [ExerciseModel]()
                        }
                        arrayExercise?.append(exerciseModel!)
                    }
                    
                    if model?.allExercise == nil {
                        model?.allExercise = [ExerciseModel]()
                    }
                    model?.allExercise = arrayExercise
                    array.append(model!)
                    
                    result(array as AnyObject, ErrorType.NO_RESULT)
                }else{
                    result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.Something_Went_Wrong)
                }
            }else{
                model = CaloriesBurntModel()
                model?.calories_sum = ((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "calories_sum") as? String
                //for cal burnt details
                for i in ((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "calorie_burnt_details") as! NSArray{
                    calBurntModel = CalBurntModel()
                    calBurntModel?.id = (i as? NSDictionary)?.object(forKey: "id") as? String
                    calBurntModel?.exercise_id = (i as? NSDictionary)?.object(forKey: "exercise_id") as? String
                    calBurntModel?.exercise_name = (i as? NSDictionary)?.object(forKey: "exercise_name") as? String
                    calBurntModel?.og_duration_count = (i as? NSDictionary)?.object(forKey: "og_duration_count") as? String
                    calBurntModel?.og_calories_burnt = (i as? NSDictionary)?.object(forKey: "og_calories_burnt") as? String
                    calBurntModel?.unit_lu_id = (i as? NSDictionary)?.object(forKey: "unit_lu_id") as? String
                    calBurntModel?.units = (i as? NSDictionary)?.object(forKey: "units") as? String
                    calBurntModel?.user_duration_count = (i as? NSDictionary)?.object(forKey: "user_duration_count") as? String
                    calBurntModel?.user_calories_burnt = (i as? NSDictionary)?.object(forKey: "user_calories_burnt") as? String
                    if arrCalModel == nil{
                        arrCalModel = [CalBurntModel]()
                    }
                    arrCalModel?.append(calBurntModel!)
                }
                
                //for all exercise
                for (pos,i) in (((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "all_exercise") as! NSArray).enumerated(){
                    exerciseModel = ExerciseModel()
                    exerciseModel?.id = (i as? NSDictionary)?.object(forKey: "id") as? String
                    exerciseModel?.exercise_name = (i as? NSDictionary)?.object(forKey: "exercise_name") as? String
                    exerciseModel?.duration_count = (i as? NSDictionary)?.object(forKey: "duration_count") as? String
                    exerciseModel?.calories_burnt = (i as? NSDictionary)?.object(forKey: "calories_burnt") as? String
                    exerciseModel?.unit_lu_id = (i as? NSDictionary)?.object(forKey: "unit_lu_id") as? String
                    exerciseModel?.units = (i as? NSDictionary)?.object(forKey: "units") as? String
                    exerciseModel?.index = pos
                    if arrayExercise == nil{
                        arrayExercise = [ExerciseModel]()
                    }
                    arrayExercise?.append(exerciseModel!)
                }
                
                if model?.calBurt == nil {
                    model?.calBurt = [CalBurntModel]()
                }
                model?.calBurt = arrCalModel
                if model?.allExercise == nil {
                    model?.allExercise = [ExerciseModel]()
                }
                model?.allExercise = arrayExercise
                array.append(model!)
                result(array as AnyObject, nil)
            }
        }
    }
    func callAddCalBurntWS(array: [CalBurntModel],date:String,result: @escaping onResponseReceived)  {
        var values = [[String:Any]]()
        
        for i in array{
            values.append([ "exercise_id" : i.exercise_id ?? "",
                            "duration_count":i.og_duration_count ?? "",
                            "unit_lu_id":i.unit_lu_id ?? "",
                            ])
        }
        let jsonO = ["details" : values]
        let params: [String: Any] = ["screen_name":"Calories Burnt",
            "webservice":"Apple",
            "user_id":Preffrences().getUserId(),
            "for_date":date,
            "calories" :jsonO.json
        ]
        print(params)
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.CALORIES_BURNT, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            var array = [TrackerModel]()
            var model : TrackerModel?
            var logModel : Logs?
            var arrayLogs=[Logs]()
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "status") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                model = TrackerModel()
                model?.current = Double(((((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "current") as? String)!)
                model?.target = Double(((((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "target") as? String)!)
                model?.isGraph=true
                
                for i in (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "logs") as! NSArray{
                    logModel = Logs()
                    logModel?.log_date = (i as! NSDictionary).object(forKey: "log_date") as? String
                    logModel?.cal_burnt = (i as! NSDictionary).object(forKey: "cal_burnt") as? String
                    logModel?.cal_consumed = (i as! NSDictionary).object(forKey: "cal_consumed") as? String
                    arrayLogs.append(logModel!)
                }
                
                if model?.logs == nil{
                    model?.logs = [Logs]()
                    model?.logs? = arrayLogs
                }
                array.append(model!)
                
                //for header
                model = TrackerModel()
                model?.isHeader = true
                array.append(model!)
                
                //for users
                for j in arrayLogs{
                    model = TrackerModel()
                    model?.isUsers = true
                    model?.logModel = j
                    array.append(model!)
                }
                result(array as AnyObject, nil)
            }
        }
        
    }
    
}
