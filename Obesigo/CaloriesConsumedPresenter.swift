//
//  CaloriesConsumedPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 28/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
protocol CaloriesConsumedDelegate : CommonDelegate {
    func onCalAddedSuccess(msg:Any?)
    func onCalAddedError(msg:String?,error:ErrorType?)
}

class CaloriesConsumedPresenter {
    weak var delegate : CaloriesConsumedDelegate?
    var service : CaloriesConsumedService
    init(service:CaloriesConsumedService) {
        self.service=service
    }
    func attachView(delegate:CaloriesConsumedDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callGetCaloriesconsumedWS(date:String) {
        delegate?.showIndicator(showIt: true, showTint: false)
        service.callGetCaloriesConsumedWS(date: date) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as Any, errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
    func callAddCalConsumedWS(array : [CalConsumedModel],date:String) {
        delegate?.showIndicator(showIt: true, showTint: false)
        service.callAddCalConsumedWS(array: array, date: date) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onCalAddedError(msg: error?.rawValue, error: error)
            }else{
                self?.delegate?.onCalAddedSuccess(msg: success as? Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
}

class CaloriesConsumedService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callGetCaloriesConsumedWS(date:String,result: @escaping onResponseReceived) {
        let url = "\(NetworkUrls.CALORIES_CONSUMED)?screen_name=Calories Burnt&webservice=Get User Calories Burnt Details&user_id=\(Preffrences().getUserId())&for_date=\("".getFormatedDate(fromFormat: "dd MMMM, yyyy", toFormat: "yyyy-MM-dd", dateString: date))"
        
        //let url = "\(NetworkUrls.CALORIES_CONSUMED)?screen_name=Calories Burnt&webservice=Get User Calories Burnt Details&user_id=\(Preffrences().getUserId())&for_date=2017-07-29"
        NetworkRequest.sharedInstance.callWebService(url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, type: .GET, params: nil) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            var model : CaloriesConsumedModel?
            var array = [CaloriesConsumedModel]()
            var arrCalModel : [CalConsumedModel]?
            var calConsumedModel : CalConsumedModel?
            var foodItemModel:FoodItemModel?
            var arrayFood : [FoodItemModel]?
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                if (success as? NSDictionary)?.object(forKey: "status_code") as? Int == 404{
                    model = CaloriesConsumedModel()
                    model?.calories_sum = ((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "calories_sum") as? String
                    //for all exercise
                    for (pos,i) in (((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "all_food_items") as! NSArray).enumerated(){
                        foodItemModel = FoodItemModel()
                        foodItemModel?.id = (i as? NSDictionary)?.object(forKey: "id") as? String
                        foodItemModel?.item_name = (i as? NSDictionary)?.object(forKey: "item_name") as? String
                        foodItemModel?.quantity = (i as? NSDictionary)?.object(forKey: "quantity") as? String
                        foodItemModel?.calories_consume = (i as? NSDictionary)?.object(forKey: "calories_consume") as? String
                        foodItemModel?.unit_lu_id = (i as? NSDictionary)?.object(forKey: "unit_lu_id") as? String
                        foodItemModel?.units = (i as? NSDictionary)?.object(forKey: "units") as? String
                        foodItemModel?.diet_preference_id = (i as? NSDictionary)?.object(forKey: "diet_preference_id") as? String
                        foodItemModel?.index = pos
                        if arrayFood == nil{
                            arrayFood = [FoodItemModel]()
                        }
                        arrayFood?.append(foodItemModel!)
                    }
                    
                    if model?.allFood == nil {
                        model?.allFood = [FoodItemModel]()
                    }
                    model?.allFood = arrayFood
                    array.append(model!)
                    
                    result(array as AnyObject, ErrorType.NO_RESULT)
                }else{
                    result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.Something_Went_Wrong)
                }
            }else{
                model = CaloriesConsumedModel()
                model?.calories_sum = ((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "calories_sum") as? String
                //for cal consumed details
                for i in ((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "calorie_consume_details") as! NSArray{
                    calConsumedModel = CalConsumedModel()
                    calConsumedModel?.id = (i as? NSDictionary)?.object(forKey: "id") as? String
                    calConsumedModel?.food_item_id = (i as? NSDictionary)?.object(forKey: "food_item_id") as? String
                    calConsumedModel?.item_name = (i as? NSDictionary)?.object(forKey: "item_name") as? String
                    calConsumedModel?.og_quantity = (i as? NSDictionary)?.object(forKey: "og_quantity") as? String
                    calConsumedModel?.og_calories_consume = (i as? NSDictionary)?.object(forKey: "og_calories_consume") as? String
                    calConsumedModel?.unit_lu_id = (i as? NSDictionary)?.object(forKey: "unit_lu_id") as? String
                    calConsumedModel?.units = (i as? NSDictionary)?.object(forKey: "units") as? String
                    calConsumedModel?.user_quantity = (i as? NSDictionary)?.object(forKey: "user_quantity") as? String
                    calConsumedModel?.user_calories_consume = (i as? NSDictionary)?.object(forKey: "user_calories_consume") as? String
                    if arrCalModel == nil{
                        arrCalModel = [CalConsumedModel]()
                    }
                    arrCalModel?.append(calConsumedModel!)
                }
                
                //for all exercise
                for (pos,i) in (((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "all_food_items") as! NSArray).enumerated(){
                    foodItemModel = FoodItemModel()
                    foodItemModel?.id = (i as? NSDictionary)?.object(forKey: "id") as? String
                    foodItemModel?.item_name = (i as? NSDictionary)?.object(forKey: "item_name") as? String
                    foodItemModel?.quantity = (i as? NSDictionary)?.object(forKey: "quantity") as? String
                    foodItemModel?.calories_consume = (i as? NSDictionary)?.object(forKey: "calories_consume") as? String
                    foodItemModel?.unit_lu_id = (i as? NSDictionary)?.object(forKey: "unit_lu_id") as? String
                    foodItemModel?.units = (i as? NSDictionary)?.object(forKey: "units") as? String
                    foodItemModel?.diet_preference_id = (i as? NSDictionary)?.object(forKey: "diet_preference_id") as? String
                    foodItemModel?.index = pos
                    if arrayFood == nil{
                        arrayFood = [FoodItemModel]()
                    }
                    arrayFood?.append(foodItemModel!)
                }
                
                if model?.calConsumed == nil {
                    model?.calConsumed = [CalConsumedModel]()
                }
                model?.calConsumed = arrCalModel
                
                if model?.allFood == nil {
                    model?.allFood = [FoodItemModel]()
                }
                model?.allFood = arrayFood
                array.append(model!)
                result(array as AnyObject, nil)
            }
        }
    }
    func callAddCalConsumedWS(array: [CalConsumedModel],date:String,result: @escaping onResponseReceived)  {
        var values = [[String:Any]]()
        for i in array{
            values.append([ "food_item_id" : i.food_item_id ?? "",
                            "quantity":i.og_quantity ?? "",
                            "unit_lu_id":i.unit_lu_id ?? "",
                            ])
        }
        let jsonO = ["details" : values]
        let params: [String: Any] = ["screen_name":"\(UIDevice.current.systemVersion)",
            "webservice":"Apple",
            "user_id":Preffrences().getUserId(),
            "for_date":date,
            "calories" :jsonO.json
        ]
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.CALORIES_CONSUMED, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            var array = [TrackerModel]()
            var model : TrackerModel?
            var logModel : Logs?
            var arrayLogs=[Logs]()
            
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                
                result((success as? NSDictionary)?.object(forKey: "status") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                
                model = TrackerModel()
                model?.current = Double(((((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "current") as? String)!)
                model?.target = Double(((((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "target") as? String)!)
                model?.isGraph=true
                for i in (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as! NSDictionary).object(forKey: "logs") as! NSArray{
                    logModel = Logs()
                    logModel?.log_date = (i as! NSDictionary).object(forKey: "log_date") as? String
                    logModel?.cal_burnt = (i as! NSDictionary).object(forKey: "cal_burnt") as? String
                    logModel?.cal_consumed = (i as! NSDictionary).object(forKey: "cal_consumed") as? String
                    arrayLogs.append(logModel!)
                }
                
                if model?.logs == nil{
                    model?.logs = [Logs]()
                    model?.logs? = arrayLogs
                }
                array.append(model!)
                
                //for header
                model = TrackerModel()
                model?.isHeader = true
                array.append(model!)
                
                //for users
                for j in arrayLogs{
                    model = TrackerModel()
                    model?.isUsers = true
                    model?.logModel = j
                    array.append(model!)
                }
                result(array as AnyObject, nil)
                
            }
        }
        
    }
    
}
