//
//  NavCell.swift
//  Obesigo
//
//  Created by Anuj  on 31/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class NavCell : UITableViewCell{
    
    @IBOutlet var ivIcon: UIImageView!
    @IBOutlet var lblHeader: UILabel!
    
    
    var data : NavModel?{
        didSet{
            if (data?.isSelected)!{
                lblHeader.textColor = Colors.themeColor
                lblHeader.font = UIFont.boldSystemFont(ofSize: lblHeader.font.pointSize)
                self.backgroundColor = UIColor(red: 241/256, green: 235/256, blue: 211/256, alpha: 1)
            }else{
                lblHeader.font = UIFont.systemFont(ofSize: lblHeader.font.pointSize)
                self.backgroundColor = UIColor.clear
                lblHeader.textColor = UIColor.black
            }
        }
    }
    
}
