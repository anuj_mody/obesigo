//
//  AddTestimonial.swift
//  Obesigo
//
//  Created by Anuj  on 25/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class AddTestimonial : BaseVC{
    
    //MARK:- outlets
    @IBOutlet var tvTestimonial: UITextView!
    @IBOutlet var btStar1: UIButton!
    @IBOutlet var btStar2: UIButton!
    @IBOutlet var btStar3: UIButton!
    @IBOutlet var btStar4: UIButton!
    @IBOutlet var btStar5: UIButton!
    
    //MARK:- Constraints
    @IBOutlet var tvTestimonialBottomConst: NSLayoutConstraint!
    
    //MARK:- variables
    var selectedRating = 5
    var presenter = AddTestimonialPresenter(service: AddTestimonialService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.addStatusBarBG()
    }
    
    //MARK:- Functions
    func setUp() {
        presenter.attachView(delegate: self)
        addKeyboardObserver()
        self.hideKeyboardWhenTappedAround()
        tvTestimonial.textColor = Colors.hintColor
        tvTestimonial.delegate=self
        self.addInputAccessoryForTextViews(textFields: [tvTestimonial], dismissable: true, previousNextable: false, showDone: true)
    }
    func addKeyboardObserver()  {
        NotificationCenter.default.addObserver(self, selector: #selector(AddTestimonial.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddTestimonial.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func keyboardWillShow(notification: Notification)  {
        let userInfo = notification.userInfo!
        let keyboardSize = (userInfo [UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size;
        UIView.animate(withDuration: 0.3) {
            self.tvTestimonialBottomConst.constant = ((keyboardSize?.height)! - 48)
        }
    }
    func keyboardWillHide()  {
        tvTestimonialBottomConst.constant=0
    }
    func changeRating(star:Int)  {
        switch star {
        case 1:
            selectedRating = 1
            btStar1.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar2.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            btStar3.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            btStar4.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            btStar5.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            break
        case 2:
            selectedRating = 2
            btStar1.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar2.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar3.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            btStar4.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            btStar5.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            break
        case 3:
            selectedRating = 3
            btStar1.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar2.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar3.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar4.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            btStar5.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            break
        case 4:
            selectedRating = 4
            btStar1.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar2.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar3.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar4.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar5.setImage(#imageLiteral(resourceName: "ic_star_empty"), for: .normal)
            break
        case 5:
            selectedRating = 5
            btStar1.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar2.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar3.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar4.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            btStar5.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            break
        default:
            break
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btStar1(_ sender: Any) {
        changeRating(star: 1)
    }
    @IBAction func btStar2(_ sender: Any) {
        changeRating(star: 2)
    }
    @IBAction func btStar3(_ sender: Any) {
        changeRating(star: 3)
    }
    @IBAction func btStar4(_ sender: Any) {
        changeRating(star: 4)
    }
    @IBAction func btStar5(_ sender: Any) {
        changeRating(star: 5)
    }
    @IBAction func btSave(_ sender: Any) {
        
        if tvTestimonial.textColor != Colors.hintColor{
            let params = ["screen_name":String(describing:AddTestimonial.self),
                          "webservice":"Submit Testimonial",
                          "user_id":Preffrences().getUserId(),
                          "testimonial_msg":tvTestimonial.text!,
                          "rating":String(describing:selectedRating)]
            presenter.callAddTestimonialWS(params: params)
        }else{
            self.showToast(msg: "Please enter your feedback",height: 58)
        }
        
    }
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
    
}

extension AddTestimonial : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if tvTestimonial.textColor == Colors.hintColor{
            tvTestimonial.text = ""
        }
        tvTestimonial.textColor = UIColor.black
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        //        if textView.textColor == Colors.hintColor{
        //            tvTestimonial.textColor
        //        }
        if textView.text.isEmpty{
            tvTestimonial.textColor = Colors.hintColor
            tvTestimonial.text = "type your feedback here"
        }
    }
}

extension AddTestimonial : AddTestimonialDelegate{
    func showIndicator(showIt: Bool,showTint:Bool) {
        if showIt{
            IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: true)
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        
        //        var model = TestimonialsModel()
        //        model.created_date = "today"
        //        if Preffrences().getName().components(separatedBy: " ").count == 0{
        //            model.first_name = Preffrences().getName()
        //            model.last_name = ""
        //        }else{
        //            model.first_name = Preffrences().getName().components(separatedBy: " ")[0]
        //            model.last_name = Preffrences().getName().components(separatedBy: " ")[1]
        //        }
        //        model.rating = String(selectedRating)
        //        model.testimonial_msg = tvTestimonial.text!
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.TESTIMONIAL_ADDED), object: self, userInfo: ["data":successValue as? String ?? ""])
        goBack()
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        self.showAlert(title: "Error!", message: messgae as? String ?? ErrorType.Something_Went_Wrong.rawValue)
    }
}
