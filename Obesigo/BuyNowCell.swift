//
//  BuyNowCell.swift
//  Obesigo
//
//  Created by Anuj  on 06/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class BuyNowCell: UITableViewCell {
    
    @IBOutlet var btContactUs: UIButton!
    var BUY_NOW_CLICK : (()->Void)? = nil
    var CONTACT_US_CLICK : (()->Void)? = nil
    var READ_MORE_CLICK : (()->Void)? = nil
    
    override func awakeFromNib() {
        btContactUs.layer.borderWidth=1
        btContactUs.layer.borderColor = Colors.themeColor.cgColor
    }
    
    
    @IBAction func btBuyNow(_ sender: Any) {
        if BUY_NOW_CLICK != nil{
            self.BUY_NOW_CLICK!()
        }
    }
    @IBAction func btContactUs(_ sender: Any) {
        if CONTACT_US_CLICK != nil{
            self.CONTACT_US_CLICK!()
        }
    }
    @IBAction func btReadMore(_ sender: Any) {
        if READ_MORE_CLICK != nil{
            self.READ_MORE_CLICK!()
        }
    }
    
    
}
