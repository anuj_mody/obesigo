//
//  TestimonialCell.swift
//  Obesigo
//
//  Created by Anuj  on 24/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class TestimonialCell: UITableViewCell {
    
    //MARK:-outlets
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var ivPP: UIImageView!
    @IBOutlet var btStar1: UIButton!
    @IBOutlet var btStar2: UIButton!
    @IBOutlet var btStar3: UIButton!
    @IBOutlet var btStar4: UIButton!
    @IBOutlet var btStar5: UIButton!
    @IBOutlet var btReadMore: UIButton!
    
    @IBOutlet var btReadMoreBottomConst: NSLayoutConstraint!
    @IBOutlet var lblMsgBottomConst: NSLayoutConstraint!
    
    let starSelected = #imageLiteral(resourceName: "ic_star_small")
    let starEmpty = #imageLiteral(resourceName: "ic_star_empty_small")
    let defaultImage = #imageLiteral(resourceName: "im_default_profile_testimonails")
    
    var READ_MORE_CLICK : (()->Void)? = nil
    
    override func awakeFromNib() {
        ivPP.layer.cornerRadius = ivPP.frame.width/2
    }
    
    var data : TestimonialsModel!{
        didSet{
            lblName.text = data.first_name! + " " + data.last_name!
            lblDate.text = data.created_date
            lblMessage.text = data.testimonial_msg
            showRating(rating: Int(data.rating ?? "1")!)
            
            if (data.testimonial_msg?.characters.count)! > 150{
                btReadMore.isHidden=false
                lblMsgBottomConst.priority=255
                btReadMoreBottomConst.priority=999
                btReadMoreBottomConst.constant=0
                lblMsgBottomConst.constant=0
            }else{
                btReadMore.isHidden=true
                btReadMoreBottomConst.priority=255
                lblMsgBottomConst.priority=999
                lblMsgBottomConst.constant = 10
            }
            
            if let imageUrl = URL(string: (data.profile_photo ?? "")){
                ivPP.kf.setImage(with: imageUrl, placeholder: defaultImage, options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                ivPP.image = defaultImage
            }
        }
    }
    
    //MARK:-Functions
    func showRating(rating:Int)  {
        
        switch rating {
        case 1:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starEmpty, for: .normal)
            btStar3.setImage(starEmpty, for: .normal)
            btStar4.setImage(starEmpty, for: .normal)
            btStar5.setImage(starEmpty, for: .normal)
            break
            
        case 2:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starSelected, for: .normal)
            btStar3.setImage(starEmpty, for: .normal)
            btStar4.setImage(starEmpty, for: .normal)
            btStar5.setImage(starEmpty, for: .normal)
            break
        case 3:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starSelected, for: .normal)
            btStar3.setImage(starSelected, for: .normal)
            btStar4.setImage(starEmpty, for: .normal)
            btStar5.setImage(starEmpty, for: .normal)
            break
        case 4:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starSelected, for: .normal)
            btStar3.setImage(starSelected, for: .normal)
            btStar4.setImage(starSelected, for: .normal)
            btStar5.setImage(starEmpty, for: .normal)
            break
        case 5:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starSelected, for: .normal)
            btStar3.setImage(starSelected, for: .normal)
            btStar4.setImage(starSelected, for: .normal)
            btStar5.setImage(starSelected, for: .normal)
            break
        default:
            break
        }
    }
    
    //MARK:-Button Actions
    @IBAction func btReadMore(_ sender: Any) {
        if READ_MORE_CLICK != nil{
            self.READ_MORE_CLICK!()
        }
    }
    
    //    func size(for label: UILabel) -> CGSize {
    ////        let constrain = CGSize(width: label.bounds.size.width, height: CGFloat(FLT_MAX))
    ////        let size: CGSize? = label.text?.size(with: label.font, constrainedTo: constrain, lineBreakMode: .wordWrap)
    ////        print(size)
    //        return CGFLOAT_MAX
    //    }
    
}
