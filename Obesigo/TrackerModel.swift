//
//  TrackerModel.swift
//  Obesigo
//
//  Created by Anuj  on 26/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
struct TrackerModel {
    var wws_level : Int?
    var current : Double?
    var target : Double?
    var logs : [Logs]?
    var isGraph = false
    var isHeader = false
    var isUsers = false
    var logModel : Logs?
}
struct Logs {
    
    var id : Int?
    var stats_id : String?
    var user_id : String?
    var log_of_on : String?
    var log_val : String?
    var height : String?
    var log_date : String?
    var cal_burnt: String?
    var cal_consumed:String?
}
