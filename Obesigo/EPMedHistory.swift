//
//  EPMedHistory.swift
//  Obesigo
//
//  Created by Anuj  on 10/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

class EPMedHistoryCell: UITableViewCell {
    @IBOutlet var lblHeader: UILabel!
    
    var data : MedicalHistory!{
        didSet{
            if data.has_user == 1{
                self.accessoryType = .checkmark
            }else{
                self.accessoryType = .none
            }
            lblHeader.text = data.name
        }
    }
}
