//
//  Colors.swift
//  Obesigo
//
//  Created by Anuj  on 11/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static let hintColor = UIColor(red: 146/255, green: 145/255, blue: 145/255, alpha: 1)
    static let themeColor = UIColor(red: 194/255, green: 31/255, blue: 37/255, alpha: 1)
    static let bmiRedColor = UIColor(red: 255/255 , green: 41/255, blue: 41/255, alpha: 1.0)
    static let bmiBlueColor = UIColor(red: 0/255 , green: 147/255, blue: 234/255, alpha: 1.0)
    static let bmiYellowColor = UIColor(red: 255/255 , green: 168/255, blue: 0/255, alpha: 1.0)
    static let bmiGreenColor = UIColor(red: 22/255 , green: 190/255, blue: 51/255, alpha: 1.0)
    static let chartBlue = UIColor(red: 0/255 , green: 192/255, blue: 243/255, alpha: 1.0)
}
