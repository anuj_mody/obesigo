//
//  HomeVC.swift
//  Obesigo
//
//  Created by Anuj  on 28/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit
import OneSignal


class HomeVC: BaseVC {
    //MARK :- outlets
    @IBOutlet var tabBar: UITabBar!
    @IBOutlet var containerView: UIView!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var btNotification: UIButton!
    @IBOutlet var ivLogo: UIImageView!
    
    //MARK :- constaints
    @IBOutlet var containerViewBottomConst: NSLayoutConstraint!
    @IBOutlet var tabBarHeightConst: NSLayoutConstraint!
    
    //MARK :- variable
    var isTabVisible = false
    
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Button Actions
    @IBAction func btNotification(_ sender: Any) {
        if self.childViewControllers[0] is Testimonials{
            self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "AddTestimonial", identifier: "AddTestimonial"))
        }else if self.childViewControllers[0] is Reminders{
            self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: AddReminder.self), identifier: String(describing: AddReminder.self)))
        }else if self.childViewControllers[0] is Profile{
            let profile = self.childViewControllers[0] as? Profile
            let vc =  self.getViewControllerFromStoryBoard(storyBoardName: String(describing: EditProfile.self), identifier: String(describing: EditProfile.self)) as? EditProfile
            
            if profile?.arrayProfile != nil{
                vc?.fName = profile?.arrayProfile?[0].fName
                vc?.lName = profile?.arrayProfile?[0].lName
                vc?.email = profile?.arrayProfile?[0].email
                vc?.number = profile?.arrayProfile?[0].mobile
                vc?.dob = "".getFormatedDate(fromFormat: "yyyy-MM-dd", toFormat: "dd MMMM, yyyy", dateString: (profile?.arrayProfile?[0].dob)!)
                vc?.gender = profile?.arrayProfile?[0].gender_lu_id == 1 ? "Male" : "Female"
                vc?.medPref = (profile?.medHistoryCount)! > 0 ? "\(profile?.medHistoryCount ?? 0) count" : "none"
                vc?.arrMedHistory = profile?.arrayProfile?[0].arrMedHistory
                vc?.dietaryPref = profile?.arrayProfile?[0].diet_preference_id == 1 ? "Veg" : "Non Veg"
                vc?.profile_image = profile?.arrayProfile?[0].profile_photo ?? ""
                self.switchViewController(viewController: vc)
                
            }
        }
    }
    @IBAction func btNavDrawerToggle(_ sender: Any) {
        self.revealViewController().revealToggle(animated: true)
    }
    
    //MARK:- Functions
    func addViewControllerToContainer(vc:UIViewController) {
        self.childViewControllers.forEach({
            $0.view.removeFromSuperview()
            $0.removeFromParentViewController()
        })
        self.addChildViewController(vc)
        vc.view.frame = self.containerView.bounds
        self.containerView.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    func animateVC(newVC:UIViewController)  {
        let oldVC = self.childViewControllers[0]
        oldVC.willMove(toParentViewController: nil)
        newVC.view.translatesAutoresizingMaskIntoConstraints=true
        self.addChildViewController(newVC)
        newVC.view.frame = self.containerView.bounds
        //newVC.view.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.containerView.addSubview(newVC.view)
        oldVC.view.removeFromSuperview()
        oldVC.removeFromParentViewController()
        newVC.didMove(toParentViewController: self)
        //        UIView.animate(withDuration: 1, delay: 0, options: .transitionFlipFromLeft, animations: {
        //            oldVC.view.transform = CGAffineTransform(translationX: -(self.view.frame.width), y: 0)
        //            newVC.view.transform = CGAffineTransform(translationX: 0, y: 0)
        //        }) { (true) in
        //            oldVC.view.removeFromSuperview()
        //            oldVC.removeFromParentViewController()
        //            newVC.didMove(toParentViewController: self)
        //        }
    }
    func changeViewController(which:Int) {
        switch which {
        case 0:
            self.tabBar.selectedItem = self.tabBar.items?[0]
            btNotification.isHidden=true
            lblHeader.text = ""
            ivLogo.isHidden=false
            addViewControllerToContainer(vc: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: Home.self), identifier: String(describing: Home.self)))
            break
        case 1:
            self.revealViewController().panGestureRecognizer().isEnabled=true
            self.tabBar.selectedItem = nil
            lblHeader.text = "Reminders"
            ivLogo.isHidden=true
            btNotification.isHidden=false
            btNotification.setImage(#imageLiteral(resourceName: "ic_add"), for: .normal)
            addViewControllerToContainer(vc: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: Reminders.self), identifier: String(describing: Reminders.self)))
            break
        case 2:
            self.revealViewController().panGestureRecognizer().isEnabled=true
            self.tabBar.selectedItem = nil
            lblHeader.text = "Testimonials"
            ivLogo.isHidden=true
            btNotification.isHidden=false
            btNotification.setImage(#imageLiteral(resourceName: "ic_add"), for: .normal)
            addViewControllerToContainer(vc: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: Testimonials.self), identifier: String(describing: Testimonials.self)))
            break
        case 3:
            self.revealViewController().panGestureRecognizer().isEnabled=true
            self.tabBar.selectedItem = nil
            ivLogo.isHidden=true
            openShareDialog()
            break
        case 4:
            self.revealViewController().panGestureRecognizer().isEnabled=true
            self.tabBar.selectedItem = nil
            ivLogo.isHidden=true
            lblHeader.text = "Change Password"
            btNotification.isHidden=true
            addViewControllerToContainer(vc: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: ChangePassword.self), identifier: String(describing: ChangePassword.self)))
            break
        case 5:
            ivLogo.isHidden=true
            self.revealViewController().panGestureRecognizer().isEnabled=true
            openSendinquiryDialog()
            break
        case 6:
            ivLogo.isHidden=true
            showLogoutDialog()
            break
        default:
            break
        }
    }
    func setUp()  {
        self.addStatusBarBG()
        Preffrences().setOTPEntered(value: true)
        Preffrences().setBMIDone(value: true)
        self.tabBar.delegate = self
        for (_ , data) in (self.tabBar.items?.enumerated())!{
            data.setTitleTextAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14)], for: .normal)
            data.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: .normal)
        }
        self.revealViewController().delegate = self
        self.tabBar.selectedItem = self.tabBar.items?[0]
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer());
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer());
        
        lblHeader.text = ""
        ivLogo.isHidden=false
        btNotification.setImage(#imageLiteral(resourceName: "ic_add"), for: .normal)
        addViewControllerToContainer(vc: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: Home.self), identifier: String(describing: Home.self)))
    }
    func openShareDialog()  {
        self.present(UIActivityViewController(activityItems: ["OBESIGO, app that helps you control your obesity - Download from App Store - https://goo.gl/NeC5aP"], applicationActivities: nil), animated: true, completion: nil)
    }
    func openSendinquiryDialog()  {
        let subject = "Enquiry"
        let data = "mailto:obesigo@hexagonnutrition.com?subject=\(subject)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let url = NSURL(string: data!) {
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    func showLogoutDialog() {
        CommonFunctions().showAlert(viewController: self, title: "", message: "Are you sure you want to logout?", showReverseButtons: true, posButton: "No", negButton: "Yes") {
            [weak self](posClick) in
            if !posClick{
                Preffrences().clearDefaults()
                OneSignal.sendTags(["user_id":"0"])
                Constants.HOME_ARRAY = nil
                UserReminders.sharedInstance.removeAllNotifications()
                Constants.SHOW_BUYNOW_ALERT = true
                let vc =  self?.getViewControllerFromStoryBoard(storyBoardName: "SplashSB", identifier: "SplashVC") as? SplashVC
                vc?.showQuote = false
                self?.present(vc!, animated: true, completion: nil)
                //                self?.switchViewController(viewController: vc)
            }
            
        }
    }
    func openEditProfile() {
        self.childViewControllers.forEach({
            $0.view.removeFromSuperview()
            $0.removeFromParentViewController()
        })
        btNotification.setImage(#imageLiteral(resourceName: "ic_edit"), for: .normal)
        btNotification.isHidden=false
        lblHeader.text = "Profile"
        ivLogo.isHidden=true
        self.revealViewController().panGestureRecognizer().isEnabled=true
        addViewControllerToContainer(vc: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: Profile.self), identifier: String(describing: Profile.self)))
    }
    func animateContainerViewBottom(show:Bool) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            if show{
                self.tabBarHeightConst.constant=49
            }else{
                self.tabBarHeightConst.constant=0
            }
        }){
            finished in
            self.view.layoutIfNeeded()
        }
    }
}

extension HomeVC : UITabBarDelegate{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.title == "Stats"{
            self.revealViewController().panGestureRecognizer().isEnabled=true
            btNotification.isHidden=true
            lblHeader.text = "Stats"
            ivLogo.isHidden=true
            addViewControllerToContainer(vc: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: Stats.self), identifier: String(describing: Stats.self)))
        }else{
            btNotification.isHidden=true
            lblHeader.text = ""
            ivLogo.isHidden=false
            addViewControllerToContainer(vc: self.getViewControllerFromStoryBoard(storyBoardName: String(describing: Home.self), identifier: String(describing: Home.self)))
        }
    }
}
extension HomeVC : UIGestureRecognizerDelegate{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension HomeVC : SWRevealViewControllerDelegate{
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        if self.childViewControllers.count > 0 {
            if self.childViewControllers[0] is Profile || self.childViewControllers[0] is ChangePassword || self.childViewControllers[0] is Testimonials || self.childViewControllers[0] is Reminders{
                animateContainerViewBottom(show: false)
            }else{
                if self.childViewControllers[0] is Home{
                    if ((self.childViewControllers[0] as? Home)?.isPurchased)!{
                        animateContainerViewBottom(show: true)
                    }else{
                        animateContainerViewBottom(show: false)
                    }
                }else{
                    animateContainerViewBottom(show: true)
                }
                
            }
        }
    }
}

