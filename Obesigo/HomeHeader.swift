//
//  HomeHeader.swift
//  Obesigo
//
//  Created by Anuj  on 04/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class HomeHeader: UITableViewCell {
    @IBOutlet var lblHeader: UILabel!
    
    
    var header : String!{
        didSet{
            lblHeader.text = header
        }
    }
    
}
