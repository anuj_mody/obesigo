//
//  OtpPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 12/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol OtpDelegate : CommonDelegate {
    func showOtpError(message:String)
}

class OtpPresenter {
    weak var delegate : OtpDelegate?
    var service : OtpService
    init(service:OtpService) {
        self.service=service
    }
    func attachView(delegate:OtpDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func checkOTP(value:String)  {
        if service.checkOTP(value: value) != nil{
            delegate?.showOtpError(message: (service.checkOTP(value: value))!)
        }
    }
    func callOTPWS(value:String,type:Int) {
        if service.checkOTP(value: value) != nil{
            delegate?.showOtpError(message: (service.checkOTP(value: value))!)
            return
        }
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callOTPWS(value: value, type: type) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as? String ?? "", errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as? String ?? "")
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
}

class OtpService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func checkOTP(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_OTP.rawValue
        }
        return nil
    }
    func callOTPWS(value:String,type:Int,result: @escaping onResponseReceived) {
        
        var params : [String:String]?
        
        if type == 0{
            params = ["screen_name":"OTP Sceen",
                      "webservice":"OTP/Mobile Number Change/Resend OTP",
                      "user_id":Preffrences().getUserId(),
                      "otp":value]
        }else if type == 1{
            params = ["screen_name":"Add Edit Reminder",
                      "webservice":"OTP/Mobile Number Change/Resend OTP",
                      "user_id":Preffrences().getUserId(),
                      "mobile":value]
        }else{
            params = ["screen_name":"OTP Sceen",
                      "webservice":"OTP/Mobile Number Change/Resend OTP",
                      "user_id":Preffrences().getUserId(),
                      "resend_otp":value]
        }

        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.OTP, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, nil)
            }
        }
    }
}
