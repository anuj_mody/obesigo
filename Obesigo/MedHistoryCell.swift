//
//  MedHistoryCell.swift
//  Obesigo
//
//  Created by Anuj  on 09/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class MedHistoryCell: UITableViewCell {
    @IBOutlet var lblHeader: UILabel!
    
    var data : String!{
        didSet{
            lblHeader.text = data
        }
    }
    
    var meals:MealsModel!{
        didSet{
            lblHeader.text = "\(meals.item_name ?? "") - \(meals.quantity ?? "") \(meals.units!)\n(\(meals.calories ?? "0") Cal.)"
        }
    }
}
