//
//  ImageCell.swift
//  Obesigo
//
//  Created by Anuj  on 04/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

class ImageCell: UITableViewCell {
    @IBOutlet var ivImage: UIImageView!
    var buyImage = #imageLiteral(resourceName: "im_home_banner_a")
    
    var url : String!{
        didSet{
            if let imageUrl = URL(string: url){
                ivImage.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "im_default_banner_image"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
    }
    
    var showImage : Bool!{
        didSet{
            ivImage.image = buyImage
        }
    }
    
}
