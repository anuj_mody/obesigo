//
//  HeaderCell.swift
//  Obesigo
//
//  Created by Anuj  on 25/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit
class HeaderCell : UITableViewCell{
    @IBOutlet var lblHeader: UILabel!
    var header : String!{
        didSet{
            lblHeader.text=header
        }
    }
}
