//
//  CaloriesBurntModel.swift
//  Obesigo
//
//  Created by Anuj  on 24/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
struct CaloriesBurntModel {
    var calories_sum: String?
    var calBurt : [CalBurntModel]?
    var allExercise: [ExerciseModel]?
}

struct CalBurntModel {
    var id: String?
    var exercise_id: String?
    var exercise_name: String?
    var og_duration_count: String?
    var og_calories_burnt: String?
    var unit_lu_id: String?
    var units: String?
    var user_duration_count: String?
    var user_calories_burnt: String?
}

struct ExerciseModel {
    var id: String?
    var exercise_name: String?
    var duration_count: String?
    var calories_burnt: String?
    var unit_lu_id: String?
    var units: String?
    var isSelected = false
    var index:Int?
}
