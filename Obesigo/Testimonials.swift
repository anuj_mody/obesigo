//
//  Testimonials.swift
//  Obesigo
//
//  Created by Anuj  on 24/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class Testimonials : BaseVC{
    //MARK:- outlets
    @IBOutlet var tvTestimonials: UITableView!
    
    //MARK:- Variable
    var presenter = TestimonialsPresenter(service: TestimonialsService())
    var arrayTestimonials : [TestimonialsModel]?
    let TESTIMONIAL_CELL = String(describing: TestimonialCell.self)
    var position : Int?
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.TESTIMONIAL_ADDED), object: nil)
    }
    
    //MARK:- Functions
    func setUp()  {
        presenter.attachView(delegate: self)
        presenter.callGetTestimonialsWS()
        tvTestimonials.register(UINib(nibName: TESTIMONIAL_CELL, bundle: nil), forCellReuseIdentifier: TESTIMONIAL_CELL)
        tvTestimonials.estimatedRowHeight=100
        tvTestimonials.rowHeight=UITableViewAutomaticDimension
        tvTestimonials.tableFooterView = UIView()
        //to get the added testimonials
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constants.TESTIMONIAL_ADDED), object: nil, queue: nil){
            [weak self] (noti) in
            //            let model = (noti.userInfo as NSDictionary?)?.object(forKey: "data")
            //            self?.arrayTestimonials?.insert((model as? TestimonialsModel)!, at: 0)
            //            self?.tvTestimonials.reloadData()
            self?.showToast(msg: (noti.userInfo as NSDictionary?)?.object(forKey: "data") as! String)
        }
        
    }
}

extension Testimonials : TestimonialsDelegate{
    func showIndicator(showIt: Bool,showTint:Bool) {
        if showIt{
            IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        arrayTestimonials = successValue as? [TestimonialsModel]
        tvTestimonials.delegate=self
        tvTestimonials.dataSource=self
        tvTestimonials.reloadData()
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        ShowDefaultImages.sharedInstance.showDefaultImage(view: self.view, type: errorValue,header: "",msg: messgae as? String ?? "",fromTop: 0,reduceHeight: true){
            [weak self] finished in
            self?.presenter.callGetTestimonialsWS()
        }
    }
}

//MARK:- Tableview Functions
extension Testimonials : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrayTestimonials?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TESTIMONIAL_CELL, for: indexPath) as? TestimonialCell
        cell?.data = arrayTestimonials?[indexPath.row]
        cell?.READ_MORE_CLICK = {
            [weak self] in
            let vc = self?.getViewControllerFromStoryBoard(storyBoardName: String(describing: ReadMoreTestimonial.self), identifier: String(describing: ReadMoreTestimonial.self)) as? ReadMoreTestimonial
            vc?.name = (self?.arrayTestimonials?[indexPath.row].first_name!)! + " " + (self?.arrayTestimonials?[indexPath.row].last_name!)!
            vc?.date = self?.arrayTestimonials?[indexPath.row].created_date
            vc?.msg = self?.arrayTestimonials?[indexPath.row].testimonial_msg
            vc?.starRating = self?.arrayTestimonials?[indexPath.row].rating
            vc?.imageUrl = self?.arrayTestimonials?[indexPath.row].profile_photo
            self?.switchViewController(viewController: vc)
        }
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
