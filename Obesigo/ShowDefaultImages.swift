//
//  ShowDefaultImages.swift
//  Obesigo
//
//  Created by Anuj  on 28/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit


class ShowDefaultImages {
    static let sharedInstance = ShowDefaultImages()
    weak var defaultaImage : DefaultScreen?
    typealias onClick = (()->Void)?
    
    let DEFAULT_SCREEN = "DefaultScreen"
    private init() {
    }
    func showDefaultImage(view:UIView,type:ErrorType,header:String = "", msg:String = "",fromTop: CGFloat = 60,reduceHeight:Bool = false,image:UIImage? = nil,clicked : onClick)  {
        defaultaImage = Bundle.main.loadNibNamed(DEFAULT_SCREEN, owner: view, options: nil)?.last as? DefaultScreen
        defaultaImage?.frame = CGRect(x: 0, y: (view.frame.maxY), width: (view.frame.width), height: (view.frame.height - (reduceHeight ? 40 : 0))-fromTop)
        if type == .No_Internet{
            defaultaImage?.ivImage.image = #imageLiteral(resourceName: "im_no_internet")
            defaultaImage?.lblHeader.text = "Oops!"
            defaultaImage?.lblMsg.text = ErrorType.No_Internet.rawValue
            defaultaImage?.clickRegistered = {
                self.removeDefaultScreen(view: view)
                clicked!()
            }
        }else if type == .NO_RESULT{
            defaultaImage?.ivImage.image = #imageLiteral(resourceName: "im_no_results")
            defaultaImage?.lblHeader.text = "Sorry!"
            defaultaImage?.lblMsg.text = msg
        }else{
            defaultaImage?.ivImage.image = #imageLiteral(resourceName: "im_something_went_wrong")
            defaultaImage?.lblHeader.text = "Sorry!"
            defaultaImage?.lblMsg.text = ErrorType.Something_Went_Wrong.rawValue
//            defaultaImage?.clickRegistered = {
//                self.removeDefaultScreen(view: view)
//                clicked!()
//            }
        }
        
        if image != nil {
            defaultaImage?.ivImage.image = image
            defaultaImage?.lblHeader.text = header
            defaultaImage?.lblMsg.text = msg
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.defaultaImage?.frame = CGRect(x: 0, y: fromTop, width: (view.frame.width), height: (view.frame.height)-fromTop)
            view.addSubview(self.defaultaImage!)
        }, completion: nil)
        
        
    }
    func removeDefaultScreen(view:UIView)  {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.defaultaImage?.frame = CGRect(x: 0, y: (view.frame.maxY), width: (view.frame.width), height: (view.frame.height))
            self.defaultaImage?.removeFromSuperview()
        }, completion: nil)
        
    }
    deinit {
        print("denit called")
    }
    
}
