//
//  ProductCodeCell.swift
//  Obesigo
//
//  Created by Anuj  on 06/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

class ProductCodeCell: UITableViewCell {
    @IBOutlet var tfActivationCode: UITextField!
    @IBOutlet var btDone: UIButton!
    var parentTable : UITableView?
    var DONE_CLICK : (()->Void)? = nil
    
    override func awakeFromNib() {
        tfActivationCode.delegate=self
        KeybordAccessories.addInputAccessoryForTextFields(textFields: [tfActivationCode], dismissable: true, previousNextable: false, showDone: true)
    }
    
    var mainTable : UITableView!{
        didSet{
            parentTable = mainTable
            KeybordAccessories.addKeyboardNotification(scrollView: parentTable!, view:self.contentView,actualContentInset: (parentTable?.contentInset)!)
        }
    }
    
    @IBAction func btDone(_ sender: Any) {
        if DONE_CLICK != nil {
            self.DONE_CLICK!()
        }
    }
    
}

extension ProductCodeCell : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        KeybordAccessories.activeTextField(textField: textField, textView: nil)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.characters.count)! + string.characters.count - range.length > 0{
            btDone.backgroundColor = Colors.themeColor
            btDone.isEnabled=true
        }else{
            btDone.backgroundColor = UIColor.lightGray
            btDone.isEnabled=false
        }
        return true
    }
}
