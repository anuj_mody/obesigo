//
//  WeightLogCell.swift
//  Obesigo
//
//  Created by Anuj  on 26/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class WeightLogCell: UITableViewCell {
    
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var ivIcon: UIImageView!
    let weightIcon = #imageLiteral(resourceName: "ic_weight")
    let waterIntakeIcon = #imageLiteral(resourceName: "ic_water_jar_small")
    let sleepIcon = #imageLiteral(resourceName: "ic_sleep_small")
    var IS_CLICKED : (()->Void)? = nil
    
    
    var weight : String?{
        didSet{
            lblWeight.text = String(describing: Double(weight!)!.round(to: 1))
        }
    }
    
    var id : Int!{
        didSet{
            switch id {
            case 1:
                ivIcon.image = weightIcon
                lblWeight.text = lblWeight.text! + " kgs"
                break
            case 2:
                ivIcon.image = waterIntakeIcon
                lblWeight.text = lblWeight.text! + " lts"
                break
                
            case 3:
                ivIcon.image = sleepIcon
                lblWeight.text = lblWeight.text! + " Hrs"
                break
            default:
                break
            }
        }
    }
    
    var date : String?{
        didSet{
            lblDate.text = "".getFormatedDate(fromFormat: "dd MMMM, yyyy", toFormat: "dd MMM, yyyy", dateString:date!)
        }
    }
    
    
    @IBAction func btVIewClick(_ sender: Any) {
        if IS_CLICKED != nil{
            self.IS_CLICKED!()
        }
    }
    
    
}
