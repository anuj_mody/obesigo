//
//  PlanListCell.swift
//  Obesigo
//
//  Created by Anuj  on 04/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

class PlanListCell: UITableViewCell {
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var ivImage: UIImageView!
    @IBOutlet var viewExercise: UIView!
    @IBOutlet var lblCalQuantity: UILabel!
    
    let earlyMorning = #imageLiteral(resourceName: "ic_early_morning")
    let morning_snacks = #imageLiteral(resourceName: "ic_morning_snacks")
    let breakFast = #imageLiteral(resourceName: "ic_breakfast")
    let lunch = #imageLiteral(resourceName: "ic_lunch")
    let eveningSnacks = #imageLiteral(resourceName: "ic_evening_snacks")
    let dinner = #imageLiteral(resourceName: "ic_dinner")
    
    
    
    var mealData : String!{
        didSet{
            lblHeader.text = mealData
            viewExercise.isHidden=true
            ivImage.isHidden=false
            lblCalQuantity.isHidden=true
            if mealData.lowercased() == "early morning"{
                ivImage.image = earlyMorning
            }else if mealData.lowercased() == "breakfast"{
                ivImage.image = breakFast
            }else if mealData.lowercased() == "morning snacks"{
                ivImage.image = morning_snacks
            }else if mealData.lowercased() == "lunch"{
                ivImage.image = lunch
            }else if mealData.lowercased() == "evening snacks"{
                ivImage.image = eveningSnacks
            }else if mealData.lowercased() == "dinner"{
                ivImage.image = dinner
            }
        }
    }
    
    var exerciseData:[String]!{
        didSet{
            viewExercise.isHidden=false
            ivImage.isHidden=true
            lblCalQuantity.isHidden=false
            lblCalQuantity.text =  exerciseData[1]
            lblHeader.text = exerciseData[0]
        }
    }
}
