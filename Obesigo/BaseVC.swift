//
//  BaseVC.swift
//  Obesigo
//
//  Created by Anuj  on 11/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit

class BaseVC: UIViewController {
    
    //MARK :- Functions
    func goBack() {
        if self.navigationController == nil{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
