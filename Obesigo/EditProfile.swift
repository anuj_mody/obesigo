//
//  EditProfile.swift
//  Obesigo
//
//  Created by Anuj  on 10/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

class EditProfile : BaseVC{
    //MARK:- outlets
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var ivProfilePic: UIImageView!
    @IBOutlet var tfFName: UITextField!
    @IBOutlet var tfLName: UITextField!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfMobile: UITextField!
    @IBOutlet var tfDOB: UITextField!
    @IBOutlet var tfGender: UITextField!
    @IBOutlet var tfMedHistory: UITextField!
    @IBOutlet var tfDietaryPref: UITextField!
    @IBOutlet var viewMedHistory: UIView!
    @IBOutlet var viewMedHistoryData: UIView!
    @IBOutlet var tvMedHistory: UITableView!
    @IBOutlet var viewTransparent: UIView!
    @IBOutlet var lblFNameError: UILabel!
    @IBOutlet var lblLNameError: UILabel!
    
    //MARK:- constraints
    @IBOutlet var viewMedHistoryDataHeightConst: NSLayoutConstraint!
    
    //MARK:- variables
    var lblFNamePH:UILabel?
    var lblLNamePH:UILabel?
    var lblEmailPH:UILabel?
    var lblMobilePH:UILabel?
    var lblDOBPH:UILabel?
    var lblGenderPH:UILabel?
    var lblMedHistoryPH:UILabel?
    var lblDietaryPrefPH:UILabel?
    var fName:String?
    var lName:String?
    var email:String?
    var number:String?
    var dob:String?
    var gender:String?
    var medPref:String?
    var dietaryPref:String?
    var arrMedHistory : [MedicalHistory]?
    var actualInset : UIEdgeInsets?
    var datePicker: UIDatePicker?
    var toolbar: UIToolbar?
    var imagePicker : UIImagePickerController?
    var profile_image : String?
    var isDatePickerVisible = false
    var isImageEdited = false
    var presenter = EditProfilePresenter(service: EditProfileService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Functions
    func setUp()  {
        presenter.attachView(delegate: self)
        self.addStatusBarBG()
        //self.hideKeyboardWhenTappedAround()
        self.addInputAccessoryForTextFields(textFields: [tfFName,tfLName])
        viewMedHistoryDataHeightConst.constant = (self.view.frame.height / 3) + 50
        actualInset = scrollView.contentInset
        lblFNamePH = tfFName.addPlaceHolder(mainView:scrollView, placeHolderText: "First Name")
        lblLNamePH = tfLName.addPlaceHolder(mainView:scrollView, placeHolderText: "Last Name")
        lblEmailPH = tfEmail.addPlaceHolder(mainView:scrollView, placeHolderText: "Email")
        lblMobilePH = tfMobile.addPlaceHolder(mainView:scrollView, placeHolderText: "Mobile")
        lblDOBPH = tfDOB.addPlaceHolder(mainView:scrollView, placeHolderText: "D.O.B")
        lblGenderPH = tfGender.addPlaceHolder(mainView:scrollView, placeHolderText: "Gender")
        lblMedHistoryPH = tfMedHistory.addPlaceHolder(mainView:scrollView, placeHolderText: "Medical History")
        lblDietaryPrefPH = tfDietaryPref.addPlaceHolder(mainView:scrollView, placeHolderText: "Dietary Preference")
        tvMedHistory.register(UINib(nibName: String(describing: EPMedHistoryCell.self), bundle: nil), forCellReuseIdentifier: String(describing: EPMedHistoryCell.self))
        tvMedHistory.estimatedRowHeight = 50
        tvMedHistory.rowHeight = UITableViewAutomaticDimension
        tvMedHistory.delegate=self
        tvMedHistory.dataSource=self
        tvMedHistory.reloadData()
        loadData()
        addDelegates()
        viewTransparent.isUserInteractionEnabled = true
        viewTransparent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(EditProfile.openImagePicker)))
        
    }
    func loadData()  {
        
        tfFName.text = fName
        lblFNamePH?.animatePlaceHolder()
        
        tfLName.text = lName
        lblLNamePH?.animatePlaceHolder()
        
        
        tfEmail.text = email
        lblEmailPH?.animatePlaceHolder()
        
        
        tfDOB.text = dob
        lblDOBPH?.animatePlaceHolder()
        
        
        tfMobile.text = number
        lblMobilePH?.animatePlaceHolder()
        
        tfGender.text = gender
        lblGenderPH?.animatePlaceHolder()
        
        
        tfMedHistory.text = medPref
        lblMedHistoryPH?.animatePlaceHolder()
        
        
        tfDietaryPref.text = dietaryPref
        lblDietaryPrefPH?.animatePlaceHolder()
        
        if let imageUrl = URL(string: profile_image!){
            ivProfilePic.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "im_upload_picture"), options: nil, progressBlock: nil, completionHandler: nil)
        }else{
            ivProfilePic.image = #imageLiteral(resourceName: "im_upload_picture")
        }
    }
    func addDelegates() {
        tfFName.delegate=self
        tfLName.delegate=self
        tfMobile.delegate=self
        tfDOB.delegate=self
        tfGender.delegate=self
        tfMedHistory.delegate=self
        tfDietaryPref.delegate=self
        KeybordAccessories.addKeyboardNotification(scrollView: scrollView, view: self.view,actualContentInset: actualInset!)
    }
    func openGenderDialog() {
        self.view.endEditing(true)
        self.showActionSheet(data: ["Male" , "Female"]) {
            [weak self](value) -> () in
            self?.tfGender.text = value
            self?.lblGenderPH?.animatePlaceHolder()
        }
    }
    func openDatePicker() {
        //hidePickers()
        self.view.endEditing(true)
        isDatePickerVisible=true
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200))
        datePicker?.backgroundColor = UIColor.white
        datePicker?.datePickerMode = .date
        datePicker?.date = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        datePicker?.maximumDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.maxY+240, width: self.view.frame.width, height: 200))
        toolbar?.sizeToFit()
        toolbar?.isTranslucent=false
        toolbar?.barTintColor = Colors.themeColor
        toolbar?.tintColor = UIColor.white
        let doneButton = UIBarButtonItem(title: "done", style: .done, target: self, action:#selector(EditProfile.getDate))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action:#selector(EditProfile.hidePickers))
        toolbar?.setItems([cancelButton, spacer, doneButton], animated: true)
        self.view.addSubview(self.datePicker!)
        self.view.addSubview(self.toolbar!)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY-200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY-235, width: self.view.frame.width, height: 35)
        }, completion: nil)
        
    }
    func getDate()  {
        hidePickers()
        tfDOB.text = datePicker?.date.getFormattedDate(formatterString: "dd-MM-yyyy")
        lblDOBPH?.animatePlaceHolder()
    }
    func hidePickers()  {
        isDatePickerVisible = false
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY+235, width: self.view.frame.width, height: 35)
        }){
            finished in
            self.toolbar?.removeFromSuperview()
            self.datePicker?.removeFromSuperview()
        }
        
        
    }
    func showMedicalHistoryDialog(show:Bool = true) {
        
        if show{
            viewMedHistory.backgroundColor = UIColor.clear
            viewMedHistory.frame = self.view.bounds
            viewMedHistoryData.frame = CGRect(x: 0, y: self.viewMedHistory.frame.maxY + viewMedHistoryDataHeightConst.constant, width: self.viewMedHistory.frame.width, height: viewMedHistoryDataHeightConst.constant)
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.viewMedHistoryData.frame = CGRect(x: 0, y: self.viewMedHistory.frame.maxY - self.view.frame.height/3, width: self.viewMedHistory.frame.width, height: self.viewMedHistoryDataHeightConst.constant)
                self.viewMedHistory.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }, completion: nil)
            self.view.addSubview(viewMedHistory)
        }else{
            var counter = 0
            for i in arrMedHistory!{
                if i.has_user == 1{
                    counter += 1
                }
            }
            self.tfMedHistory.text = counter == 0 ? "none" : "\(counter)"
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.viewMedHistoryData.frame = CGRect(x: 0, y: self.viewMedHistory.frame.maxY + self.viewMedHistoryDataHeightConst.constant, width: self.viewMedHistory.frame.width, height: self.viewMedHistoryDataHeightConst.constant)
                self.viewMedHistory.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            }){
                finished in
                self.viewMedHistory.removeFromSuperview()
                
            }
            
        }
        
    }
    func showDietaryPrefDialog() {
        self.view.endEditing(true)
        self.showActionSheet(data: ["Veg" , "Non- Veg"]) {
            [weak self](value) -> () in
            self?.tfDietaryPref.text = value
            self?.lblDietaryPrefPH?.animatePlaceHolder()
        }
    }
    func openImagePicker() {
        self.view.endEditing(true)
        self.showActionSheet(data: ["Camera" , "Gallery"]) {
            [weak self](value) -> () in
            if value == "Camera"{
                self?.openCamerGalleryPicker(fromGallery: false)
            }else{
                self?.openCamerGalleryPicker(fromGallery: true)
            }
        }
    }
    func openCamerGalleryPicker(fromGallery:Bool)  {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            
            imagePicker = UIImagePickerController()
            imagePicker?.delegate=self
            if fromGallery{
                imagePicker?.sourceType = .photoLibrary
                // imagePicker?.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            }else{
                imagePicker?.sourceType = .camera
                imagePicker?.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            }
            self.present(imagePicker!, animated: true, completion: nil)
            
        }
        
    }
    
    //MARK:- Button actions
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
    @IBAction func btSave(_ sender: Any) {
        if isImageEdited{
            presenter.callEditProfileWithImageWS(images: ["profile_photo":ivProfilePic.image!], fName: tfFName.text!, lName: tfLName.text!, dob: tfDOB.text!, deitaryPref: tfDietaryPref.text!, medHistory: arrMedHistory!, gender: tfGender.text!)
        }else{
            presenter.callEditProfileWS(fName: tfFName.text!, lName: tfLName.text!, dob: tfDOB.text!, deitaryPref: tfDietaryPref.text!, medHistory: arrMedHistory!, gender: tfGender.text!)
        }
        
    }
    @IBAction func btDone(_ sender: Any) {
        showMedicalHistoryDialog(show: false)
    }
}

extension EditProfile : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfFName || textField == tfLName{
            KeybordAccessories.activeTextField(textField: textField, textView: nil)
        }
        
        if textField == tfFName{
            lblLNamePH?.animatePlaceHolder()
            lblFNameError.isHidden=true
        }else if textField == tfLName{
            lblLNamePH?.animatePlaceHolder()
            lblLNameError.isHidden=true
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField ==  tfFName{
            presenter.checkFName(value: textField.text!)
        }else if textField == tfLName{
            presenter.checkLName(value: textField.text!)
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfGender{
            openGenderDialog()
            return false
        }
        
        if textField == tfDOB{
            if !isDatePickerVisible{
                openDatePicker()
            }
            return false
        }
        
        if textField == tfMedHistory{
            showMedicalHistoryDialog()
            return false
        }
        
        if textField == tfDietaryPref{
            showDietaryPrefDialog()
            return false
        }
        
        
        return true
    }
}

//MARK:- Tableview Functions
extension EditProfile : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMedHistory?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EPMedHistoryCell.self), for: indexPath) as? EPMedHistoryCell
        cell?.data = arrMedHistory?[indexPath.row]
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrMedHistory?[indexPath.row].has_user == 1{
            arrMedHistory?[indexPath.row].has_user = 0
        }else{
            arrMedHistory?[indexPath.row].has_user = 1
        }
        tvMedHistory.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension EditProfile : EditProfileDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        goBack()
        Constants.HOME_ARRAY = nil
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.PROFILE_EDITED), object: self, userInfo: ["data":successValue])
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        self.showAlert(title: "Error!", message: messgae as? String ?? "")
    }
    func showFNameError(message: String) {
        lblFNameError.isHidden=false
        lblFNameError.text = message
    }
    func showLNameError(message: String) {
        lblLNameError?.isHidden=false
        lblLNameError?.text = message
    }
}

extension EditProfile : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        ivProfilePic.image = selectedImage
        isImageEdited = true
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
