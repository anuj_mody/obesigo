//
//  ProfilePresenter.swift
//  Obesigo
//
//  Created by Anuj  on 08/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
protocol ProfileDelegate: CommonDelegate {
    
}

class ProfilePresenter {
    weak var delegate : ProfileDelegate?
    var service : ProfileService
    init(service:ProfileService) {
        self.service=service
    }
    func attachView(delegate:ProfileDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callGetProfileWS()  {
        delegate?.showIndicator(showIt: true, showTint: false)
        service.callGetProfileWS(){
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success, errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: false)
        }
        
    }
    
}

class ProfileService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callGetProfileWS(result: @escaping onResponseReceived) {
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.PROFILE+"/\(Preffrences().getUserId())", type: .GET, params: nil) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                var array = [User]()
                var model : User?
                var medHistoryModel : MedicalHistory?
                var statsModel = StatsModel()
                var arrMedHistory = [MedicalHistory]()
                var arrStats = [StatsModel]()
                for i in  ((success as? NSDictionary)?.object(forKey: "response") as? NSArray)!{
                    let response = (i as? NSDictionary)
                    model = User()
                    model?.userId = response?.object(forKey: "user_id") as? Int
                    model?.access_till = response?.object(forKey: "access_till") as? String
                    model?.activated_on = response?.object(forKey: "activated_on") as? String
                    model?.api_token = response?.object(forKey: "api_token") as? String
                    model?.created_at = response?.object(forKey: "created_at") as? String
                    model?.diet_preference_id = Int((response?.object(forKey: "diet_preference_id") as? String)!)
                    model?.dob = response?.object(forKey: "dob") as? String
                    model?.email = response?.object(forKey: "email") as? String
                    model?.expire_on = response?.object(forKey: "expire_on") as? String
                    model?.fName = response?.object(forKey: "first_name") as? String
                    model?.lName = response?.object(forKey: "last_name") as? String
                    model?.mobile = response?.object(forKey: "mobile") as? String
                    model?.profile_photo = response?.object(forKey: "profile_photo") as? String
                    model?.height = response?.object(forKey: "height") as? String
                    model?.gender_lu_id = Int((response?.object(forKey: "gender_lu_id") as? String)!)
                    model?.heightUnitId = response?.object(forKey: "height_unit") as? String
                    
                    //for med history
                    for j in (response?.object(forKey: "medical_history_list") as! NSArray){
                        medHistoryModel = MedicalHistory()
                        medHistoryModel?.has_user = Int(((j as? NSDictionary)?.object(forKey: "has_user") as? String)!)
                        medHistoryModel?.id = Int(((j as? NSDictionary)?.object(forKey: "id") as? String)!)
                        medHistoryModel?.name = (j as? NSDictionary)?.object(forKey: "name") as? String
                        arrMedHistory.append(medHistoryModel!)
                    }
                    
                    //for stats
                    statsModel.calories = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "calories") as? String
                    statsModel.sleep_log = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "sleep_log") as? String
                    statsModel.water_intake = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "water_intake") as? String
                    statsModel.weight = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "weight") as? String
                    arrStats.append(statsModel)
                    
                    
                    if model?.arrMedHistory == nil{
                        model?.arrMedHistory = [MedicalHistory]()
                        model?.arrMedHistory = arrMedHistory
                    }
                    
                    if model?.arrStats == nil{
                        model?.arrStats = [StatsModel]()
                        model?.arrStats = arrStats
                    }
                    array.append(model!)
                }
                result(array as AnyObject, nil)
            }
        }
    }
}
