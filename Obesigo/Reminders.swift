//
//  Reminders.swift
//  Obesigo
//
//  Created by Anuj  on 03/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit

class Reminders: BaseVC {
    //MARK:- outlets
    @IBOutlet var tvReminders: UITableView!
    
    //MARK:- variables
    var arrayReminder : [RemindersModel]?
    var position : Int?
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.REMINDER_ADDED), object: nil)
        //arrayReminder = nil
    }
    
    //MARK:- Functions
    func setUp() {
        tvReminders.register(UINib(nibName: String(describing: ReminderCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ReminderCell.self))
        tvReminders.estimatedRowHeight = 100
        tvReminders.rowHeight = UITableViewAutomaticDimension
        tvReminders.tableFooterView = UIView()
        tvReminders.contentSize = .zero
        tvReminders.separatorInset = .zero
        arrayReminder = [RemindersModel]()
        arrayReminder = Preffrences().getReminderArray()
        tvReminders.delegate=self
        tvReminders.dataSource = self
        tvReminders.reloadData()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constants.REMINDER_ADDED), object: nil, queue: nil){
            [weak self](noti) in
            let data : NSDictionary = (noti.userInfo as NSDictionary?)?.object(forKey: "data")  as! NSDictionary
            if (data.object(forKey: "from") as? String) == "add"{
                if self?.arrayReminder == nil {
                    self?.arrayReminder = [RemindersModel]()
                    self?.arrayReminder?.append((data.object(forKey: "model") as? RemindersModel)!)
                    self?.tvReminders.reloadData()
                }else{
                    self?.arrayReminder?.insert((data.object(forKey: "model") as? RemindersModel)!, at: 0)
                    self?.tvReminders.insertRows(at: [IndexPath(row: 0, section: 0)], with: .top)
                }
                
                
            }else if (data.object(forKey: "from") as? String) == "edit"{
                self?.arrayReminder?[(self?.position!)!] = (data.object(forKey: "model") as? RemindersModel)!
                self?.tvReminders.reloadRows(at: [IndexPath(row: (self?.position!)!, section: 0)], with: .automatic)
            }else{
                self?.showToast(msg: (data.object(forKey: "message") as? String ?? ""))
                self?.arrayReminder?.remove(at: (self?.position!)!)
                self?.tvReminders.reloadData()
                if self?.arrayReminder?.count == 0{
                    ShowDefaultImages.sharedInstance.showDefaultImage(view: (self?.view)!, type: ErrorType.Something_Went_Wrong, header: "Oops!", msg: "No reminders found", fromTop: 0, reduceHeight: true, image: #imageLiteral(resourceName: "im_no_remiders")){
                        [weak self] finished in
                        
                    }
                }
            }
            Preffrences().setReminderArray(array: (self?.arrayReminder!)!)
        }
    }
}

//MARK:- Tableview Functions
extension Reminders : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayReminder?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ReminderCell.self), for: indexPath) as? ReminderCell
        cell?.data=arrayReminder?[indexPath.row]
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        position = indexPath.row
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: String(describing: AddReminder.self), identifier: String(describing: AddReminder.self)) as? AddReminder
        vc?.reminderId = arrayReminder?[indexPath.row].id
        
        if (arrayReminder?[indexPath.row].days?.lowercased() == "everyday"){
            vc?.isMonSelected=true
            vc?.isTueSelected=true
            vc?.isWedSelected=true
            vc?.isThuSelected=true
            vc?.isFriSelected=true
            vc?.isSatSelected=true
            vc?.isSunSelected=true
        }else{
            
            if (arrayReminder?[indexPath.row].days?.contains("1"))!{
                vc?.isMonSelected=true
            }
            
            if (arrayReminder?[indexPath.row].days?.contains("2"))!{
                vc?.isTueSelected=true
            }
            
            if (arrayReminder?[indexPath.row].days?.contains("3"))!{
                vc?.isWedSelected=true
            }
            
            if (arrayReminder?[indexPath.row].days?.contains("4"))!{
                vc?.isThuSelected=true
            }
            
            if (arrayReminder?[indexPath.row].days?.contains("5"))!{
                vc?.isFriSelected=true
            }
            
            if (arrayReminder?[indexPath.row].days?.contains("6"))!{
                vc?.isSatSelected=true
            }
            
            if (arrayReminder?[indexPath.row].days?.contains("7"))!{
                vc?.isSunSelected=true
            }
        }
        vc?.name = arrayReminder?[indexPath.row].name
        vc?.selectedTime = arrayReminder?[indexPath.row].time
        self.switchViewController(viewController: vc)
    }
}
