//
//  SignUpPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 13/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol SignUpDelegate : CommonDelegate {
    func showEmailError(message:String)
    func showPwdError(message:String)
    func showFNameError(message:String)
    func showLNameError(message:String)
    func showMobileError(message:String)
    func showDOBError(message:String)
    func showGenderError(message:String)
}

class SignUpPresenter {
    weak var delegate : SignUpDelegate?
    var service : SignUpService?
    init(service:SignUpService) {
        self.service=service
    }
    func attachView(delegate:SignUpDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func checkEmail(email:String)  {
        if service?.checkEmail(email: email) != nil{
            delegate?.showEmailError(message: (service?.checkEmail(email: email))!)
        }
    }
    func checkPassword(password:String)  {
        if service?.checkPassword(password: password) != nil{
            delegate?.showPwdError(message: (service?.checkPassword(password: password))!)
        }
    }
    func checkFName(fName:String)  {
        if service?.checkFirstName(fName: fName) != nil{
            delegate?.showFNameError(message: (service?.checkFirstName(fName: fName))!)
        }
    }
    func checkLName(lName:String)  {
        if service?.checkLastName(value: lName) != nil{
            delegate?.showLNameError(message: (service?.checkLastName(value: lName))!)
        }
    }
    func checkMobile(mobile:String)  {
        if service?.checkMobile(value: mobile) != nil{
            delegate?.showMobileError(message: (service?.checkMobile(value: mobile))!)
        }
    }
    func checkDOB(dob:String)  {
        if service?.checkDOB(value: dob) != nil{
            delegate?.showDOBError(message: (service?.checkDOB(value: dob))!)
        }
    }
    func checkGender(gender:String)  {
        if service?.checkGender(value: gender) != nil{
            delegate?.showGenderError(message: (service?.checkGender(value: gender))!)
        }
    }
    func callSignUpWS(url:String,fName:String,lName:String,email:String,password:String,mobile:String,dob:String,gender:String,activationCode:String?){
        if service?.checkFirstName(fName: fName) != nil{
            delegate?.showFNameError(message: (service?.checkFirstName(fName: fName))!)
            return
        }
        
        if service?.checkLastName(value: lName) != nil{
            delegate?.showLNameError(message: (service?.checkLastName(value: lName))!)
            return
        }
        
        if service?.checkEmail(email: email) != nil{
            delegate?.showEmailError(message: (service?.checkEmail(email: email))!)
            return
        }
        
        if service?.checkPassword(password: password) != nil{
            delegate?.showPwdError(message: (service?.checkPassword(password: password))!)
            return
        }
        
        if service?.checkMobile(value: mobile) != nil{
            delegate?.showMobileError(message: (service?.checkMobile(value: mobile))!)
            return
        }
        
        if service?.checkDOB(value: dob) != nil{
            delegate?.showDOBError(message: (service?.checkDOB(value: dob))!)
            return
        }
        
        if service?.checkGender(value: gender) != nil{
            delegate?.showGenderError(message: (service?.checkGender(value: gender))!)
            return
        }
        
        let params : [String:Any] = ["first_name":fName,
                                     "last_name":lName,
                                     "email":email,
                                     "password":password,
                                     "dob":"".getFormatedDate(fromFormat: "dd-MM-yyyy", toFormat: "dd/MM/yyyy", dateString: dob),
                                     "screen_name":"Sign Up",
                                     "webservice":"Register User",
                                     "gender_lu_id":gender.lowercased() == "male" ? 1 : 2,
                                     "activation_code":activationCode ?? "",
                                     "mobile":mobile]
        print(params)
        delegate?.showIndicator(showIt: true, showTint: true)
        service?.callSignUpWS(url: url, params: params, result: {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as? String ?? "", errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: true)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        })
    }
}

class SignUpService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func checkEmail(email:String) -> String?  {
        if email.isEmpty{
            return ErrorType.EMPTY_EMAIL.rawValue
        }
        
        if !email.isValidEmail(){
            return ErrorType.INVALID_EMAIL.rawValue
        }
        return nil
    }
    func checkPassword(password:String) -> String?  {
        if password.isEmpty{
            return ErrorType.EMPTY_PASSWORD.rawValue
        }
        
        if password.characters.count < 6{
            return ErrorType.PASSWORD_LENGHT.rawValue
        }
        return nil
    }
    func checkFirstName(fName:String) -> String?  {
        if fName.isEmpty{
            return ErrorType.EMPTY_FNAME.rawValue
        }
        return nil
    }
    func checkLastName(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_LNAME.rawValue
        }
        return nil
    }
    func checkMobile(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_MOBILE.rawValue
        }
        return nil
    }
    func checkDOB(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_DOB.rawValue
        }
        return nil
    }
    func checkGender(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_GENDER.rawValue
        }
        return nil
    }
    func callSignUpWS(url:String, params: [String:Any], result: @escaping onResponseReceived)  {
        NetworkRequest.sharedInstance.callWebService(url: url, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                var array = [User]()
                var model : User?
                var medHistoryModel : MedicalHistory?
                var statsModel = StatsModel()
                var arrMedHistory = [MedicalHistory]()
                var arrStats = [StatsModel]()
                var arrReminders = [RemindersModel]()
                for i in  ((success as? NSDictionary)?.object(forKey: "response") as? NSArray)!{
                    let response = (i as? NSDictionary)
                    Preffrences().setUserId(id: (response?.object(forKey: "user_id") as? String)!)
                    Preffrences().setAuthToken(token: (response?.object(forKey: "api_token") as? String)!)
                    Preffrences().setPassword(value: (params["password"] as? String)!)
                    model = User()
                    model?.userId = response?.object(forKey: "user_id") as? Int
                    model?.access_till = response?.object(forKey: "access_till") as? String
                    model?.activated_on = response?.object(forKey: "activated_on") as? String
                    model?.api_token = response?.object(forKey: "api_token") as? String
                    model?.created_at = response?.object(forKey: "created_at") as? String
                    model?.diet_preference_id = response?.object(forKey: "diet_preference_id") as? Int
                    model?.dob = response?.object(forKey: "dob") as? String
                    model?.email = response?.object(forKey: "email") as? String
                    model?.expire_on = response?.object(forKey: "expire_on") as? String
                    model?.fName = response?.object(forKey: "first_name") as? String
                    model?.lName = response?.object(forKey: "last_name") as? String
                    model?.mobile = response?.object(forKey: "mobile") as? String
                    model?.height = response?.object(forKey: "height") as? String
                    model?.gender_lu_id = response?.object(forKey: "gender_lu_id") as? Int
                    
                    Preffrences().setProfilePic(value: (model?.profile_photo ?? "")!)
                    Preffrences().setName(value: (model?.fName)! + " " + (model?.lName)!)
                    for j in (response?.object(forKey: "medical_history_list") as! NSArray){
                        medHistoryModel = MedicalHistory()
                        medHistoryModel?.has_user = (j as? NSDictionary)?.object(forKey: "has_user") as? Int
                        medHistoryModel?.id = (j as? NSDictionary)?.object(forKey: "id") as? Int
                        medHistoryModel?.name = (j as? NSDictionary)?.object(forKey: "name") as? String
                        arrMedHistory.append(medHistoryModel!)
                    }
                    
                    statsModel.calories = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "calories") as? String
                    statsModel.sleep_log = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "sleep_log") as? String
                    statsModel.water_intake = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "water_intake") as? String
                    statsModel.weight = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "weight") as? String
                    arrStats.append(statsModel)
                    
//                    let stats = ["weight":String(describing: Double(statsModel.weight ?? "0.00")!.round(to: 1)),
//                                 "calories":String(describing: Double(statsModel.calories ?? "0.00")!.round(to: 1)),
//                                 "waterIntake":String(describing: Double(statsModel.water_intake ?? "0.00")!.round(to: 1)),
//                                 "sleepLog":String(describing: Double(statsModel.sleep_log ?? "0.00")!.round(to: 1))]
//                    
//                    Preffrences().setUserStats(value: stats )
                    
                    let stats = ["weight":String(describing: Double(statsModel.weight ?? "0.00")!.round(to: 1)),
                                 "calories":String(describing: Double(statsModel.calories ?? "0.00")!.round(to: 1)),
                                 "waterIntake":String(describing: Double(statsModel.water_intake ?? "0.00")!.round(to: 1)),
                                 "sleepLog":String(describing: Double(statsModel.sleep_log ?? "0.00")!.round(to: 1))]
                    
                    //                    let stats = ["weight":String(describing: Double(statsModel.weight ?? "-")),
                    //                                 "calories":String(describing: Double(statsModel.calories ?? "-")),
                    //                                 "waterIntake":String(describing: Double(statsModel.water_intake ?? "-")),
                    //                                 "sleepLog":String(describing: Double(statsModel.sleep_log ?? "-"))]
                    
                    Preffrences().setUserStats(value: stats )
                    
                    // for reminders
                    var remModel : RemindersModel?
                    for r in (response?.object(forKey: "user_reminder") as! NSArray){
                        remModel = RemindersModel(days: ((r as? NSDictionary)?.object(forKey: "days") as? String)!, id: ((r as? NSDictionary)?.object(forKey: "id") as? String)!, name: ((r as? NSDictionary)?.object(forKey: "name") as? String)!, time: ((r as? NSDictionary)?.object(forKey: "time") as? String)!, userId: ((r as? NSDictionary)?.object(forKey: "user_id") as? String)!)
                        arrReminders.append(remModel!)
                    }
                    Preffrences().setReminderArray(array: arrReminders)
                    
                    if model?.arrMedHistory == nil{
                        model?.arrMedHistory = [MedicalHistory]()
                        model?.arrMedHistory = arrMedHistory
                    }
                    
                    if model?.arrMedHistory == nil{
                        model?.arrMedHistory = [MedicalHistory]()
                        model?.arrMedHistory = arrMedHistory
                    }
                    
                    if model?.arrStats == nil{
                        model?.arrStats = [StatsModel]()
                        model?.arrStats = arrStats
                    }
                    array.append(model!)
                    //Preffrences().setUserArray(array: array)
                }
                result(true as AnyObject, nil)
            }
        }
    }
}
