//
//  CaloriesConsumedModel.swift
//  Obesigo
//
//  Created by Anuj  on 28/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
struct CaloriesConsumedModel {
    var calories_sum: String?
    var calConsumed : [CalConsumedModel]?
    var allFood: [FoodItemModel]?
}

struct CalConsumedModel {
    var id: String?
    var food_item_id: String?
    var item_name: String?
    var og_quantity: String?
    var og_calories_consume: String?
    var unit_lu_id: String?
    var units: String?
    var user_quantity: String?
    var user_calories_consume: String?
}

struct FoodItemModel {
    var id: String?
    var item_name: String?
    var quantity: String?
    var calories_consume: String?
    var unit_lu_id: String?
    var diet_preference_id : String?
    var units: String?
    var isSelected = false
    var index:Int?
}
