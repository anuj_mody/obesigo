//
//  CaloriesConsumed.swift
//  Obesigo
//
//  Created by Anuj  on 28/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class CaloriesConsumed: BaseVC {
    //MARK:- outlets
    @IBOutlet var tvCal: UITableView!
    @IBOutlet var lblCalConsumed: UILabel!
    @IBOutlet var coverView: UIView!
    @IBOutlet var btDate: UIButton!
    @IBOutlet var btSave: UIButton!
    
    //MARK:- constraints
    @IBOutlet var tvCalHeightConst: NSLayoutConstraint!
    
    //MARK:- variables
    var isPickerOpen=false
    var datePicker: UIDatePicker?
    var toolbar: UIToolbar?
    var dateFormatter = DateFormatter()
    var arrayCalories : [CaloriesConsumedModel]?
    var arrayCalConsumed = [CalConsumedModel](){
        didSet{
            tvCal.delegate=self
            tvCal.dataSource=self
            tvCal.reloadData()
            //            if arrayCalConsumed.count > 0{
            //                btSave.isEnabled=true
            //                btSave.backgroundColor = Colors.themeColor
            //            }
            lblCalConsumed.text = "\((arrayCalories?[0].calories_sum ?? "" == "" ? "0" : arrayCalories?[0].calories_sum)!) Cal."
            tvCal.tableHeaderView = nil
            tvCalHeightConst.constant=0
            originalFoodItem = arrayCalories?[0].allFood
            tvCalHeightConst.constant = tvCal.contentSize.height
            coverView.isHidden=true
        }
    }
    var arrayCounts = [String]()
    var position:Int?
    var originalFoodItem : [FoodItemModel]?
    var pickerView : UIPickerView?
    var presenter = CaloriesConsumedPresenter(service: CaloriesConsumedService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=true
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Setup
    func setUp() {
        presenter.attachView(delegate: self)
        self.addStatusBarBG()
        tvCal.register(UINib(nibName: String(describing: CalBurntConsumedCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CalBurntConsumedCell.self))
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        btDate.setTitle(dateFormatter.string(from: Date()), for: .normal)
        tvCal.tableFooterView = UIView()
        presenter.callGetCaloriesconsumedWS(date: btDate.title(for: .normal)!)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constants.FOOD_ARRAY), object: nil, queue: nil){
            [weak self](noti) in
            var calArray = [CalConsumedModel]()
            var model :  CalConsumedModel?
            
            for i in (noti.userInfo as NSDictionary?)?.object(forKey: "data") as! [FoodItemModel]{
                model=CalConsumedModel()
                model?.og_calories_consume = i.calories_consume
                model?.user_calories_consume = i.calories_consume
                model?.og_quantity = i.quantity
                model?.user_quantity = i.quantity
                model?.units = i.units
                model?.food_item_id = i.id
                model?.item_name = i.item_name
                model?.unit_lu_id = i.unit_lu_id
                calArray.append(model!)
            }
            if calArray.count != 0{
                self?.tvCal.tableHeaderView=nil
            }else{
                
                DispatchQueue.main.async {
                    self?.tvCalHeightConst.constant = 100
                    self?.tvCal.tableHeaderView = self?.addFooterView(msg: "You don't have any entries made yet! \nClick on \"Add Items\" now!")
                }
                
            }
            self?.arrayCalories?[0].calConsumed = calArray
            self?.arrayCalories?[0].allFood = self?.originalFoodItem
            self?.arrayCalConsumed = calArray
            self?.tvCal.reloadData()
            self?.calculateCalories()
            
        }
    }
    func getDate()  {
        hidePickers()
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        btDate.setTitle(dateFormatter.string(from: (datePicker?.date)!), for: .normal)
        presenter.callGetCaloriesconsumedWS(date: btDate.title(for: .normal)!)
        
    }
    func hidePickers()  {
        isPickerOpen=false
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY+235, width: self.view.frame.width, height: 35)
            self.pickerView?.frame = CGRect(x: 0, y: self.view.frame.maxY+150, width: self.view.frame.width, height: 150)
        }){
            
            finished in
            self.toolbar?.removeFromSuperview()
            self.datePicker?.removeFromSuperview()
        }
        
        
    }
    func openDatePicker(date:String) {
        //hidePickers()
        isPickerOpen=true
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200))
        datePicker?.backgroundColor = UIColor.white
        datePicker?.datePickerMode = .date
        datePicker?.minimumDate = Calendar.current.date(byAdding: .month, value: -2, to: Date())
        datePicker?.maximumDate = Date()
        datePicker?.date = dateFormatter.date(from: date)!
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.maxY+240, width: self.view.frame.width, height: 200))
        toolbar?.sizeToFit()
        toolbar?.isTranslucent=false
        toolbar?.barTintColor = Colors.themeColor
        toolbar?.tintColor = UIColor.white
        let doneButton = UIBarButtonItem(title: "done", style: .done, target: self, action:#selector(WeightTracker.getDate))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action:#selector(WeightTracker.hidePickers))
        toolbar?.setItems([cancelButton, spacer, doneButton], animated: true)
        self.view.addSubview(self.datePicker!)
        self.view.addSubview(self.toolbar!)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY-200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY-235, width: self.view.frame.width, height: 35)
        }, completion: nil)
        
    }
    func showPicker(selectedPos:Int = 0) {
        isPickerOpen=true
        pickerView = UIPickerView()
        pickerView?.frame = CGRect(x: 0, y: view.frame.maxY+150, width: self.view.frame.width, height: 150)
        pickerView?.backgroundColor = UIColor.white
        pickerView?.tintColor = Colors.themeColor
        pickerView?.showsSelectionIndicator=true
        for i in 1..<50{
            arrayCounts.append(String(i))
        }
        pickerView?.delegate=self
        pickerView?.dataSource=self
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.maxY+150, width: self.view.frame.width, height: 150))
        toolbar?.sizeToFit()
        toolbar?.isTranslucent=false
        toolbar?.barTintColor = Colors.themeColor
        toolbar?.tintColor = UIColor.white
        let doneButton = UIBarButtonItem(title: "done", style: .done, target: self, action:#selector(CaloriesBurnt.getSelectedCount))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action:#selector(WeightTracker.hidePickers))
        toolbar?.setItems([cancelButton, spacer, doneButton], animated: true)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.pickerView?.frame = CGRect(x: 0, y: self.view.frame.maxY-170, width: self.view.frame.width, height: 170)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY-205, width: self.view.frame.width, height: 35)
        }, completion: nil)
        pickerView?.selectRow(selectedPos, inComponent: 0, animated: true)
        self.view.addSubview(self.pickerView!)
        self.view.addSubview(self.toolbar!)
    }
    func getSelectedCount()  {
        //arrayFilter[(pickerView?.selectedRow(inComponent: 0))!]
        hidePickers()
        let perMin = Double(arrayCalConsumed[position!].og_calories_consume!)! /  Double(arrayCalConsumed[position!].og_quantity!)!
        arrayCalConsumed[position!].og_quantity = arrayCounts[(pickerView?.selectedRow(inComponent: 0))!]
        let count = Double(arrayCounts[(pickerView?.selectedRow(inComponent: 0))!])
        arrayCalConsumed[position!].og_calories_consume = String((count! * perMin).round(to: 1))
        tvCal.reloadRows(at: [IndexPath(row: position!, section: 0)], with: .automatic)
        calculateCalories()
    }
    func calculateCalories() {
        var calBurnt = 0.0;
        for i in arrayCalConsumed{
            calBurnt = calBurnt + Double(i.og_calories_consume!)!
        }
        lblCalConsumed.text = String(Int(calBurnt)) + " Cal."
    }
    func addFooterView(msg:String) -> UIView {
        let toastLabel = UILabel(frame:CGRect(x: 0,y:0,width: self.view.frame.width,height: 100))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.numberOfLines = 0
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.font = UIFont.systemFont(ofSize: 16)
        toastLabel.textAlignment = NSTextAlignment.center
        self.view.addSubview(toastLabel)
        toastLabel.text = msg
        toastLabel.clipsToBounds  =  true
        return toastLabel
    }
    
    //MARK:- Button Actions
    @IBAction func btDate(_ sender: Any) {
        if !isPickerOpen{
            openDatePicker(date: btDate.title(for: .normal)!)
        }
    }
    @IBAction func btAddCalories(_ sender: Any) {
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: String(describing: AddCalorie.self), identifier: String(describing: AddCalorie.self)) as? AddCalorie
        if arrayCalories?[0].calConsumed == nil || arrayCalories?[0].calConsumed?.count == 0 {
            vc?.arrayFoodItems = arrayCalories?[0].allFood
            vc?.arrayFoodItemsCopy = arrayCalories?[0].allFood
        }else{
            for i in (arrayCalories?[0].calConsumed)!{
                for var (pos,j) in ((arrayCalories?[0].allFood)?.enumerated())!{
                    if i.food_item_id == j.id{
                        arrayCalories?[0].allFood?[pos].isSelected=true
                    }
                }
            }
            vc?.arrayFoodItems=arrayCalories?[0].allFood
            vc?.arrayFoodItemsCopy = arrayCalories?[0].allFood
        }
        vc?.fromFood = true
        self.switchViewController(viewController: vc)
    }
    @IBAction func btSave(_ sender: Any) {
        presenter.callAddCalConsumedWS(array: arrayCalConsumed, date: "".getFormatedDate(fromFormat: "dd MMMM, yyyy", toFormat: "yyyy-MM-dd", dateString: btDate.title(for: .normal)!))
    }
    @IBAction func btBack(_ sender: Any) {
        goBack()
    }
}

extension CaloriesConsumed : CaloriesConsumedDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        arrayCalories = (successValue as? [CaloriesConsumedModel])
        //originalFoodItem = (successValue as? [CaloriesConsumedModel])
        arrayCalConsumed = (arrayCalories?[0].calConsumed!)!
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        if errorValue == ErrorType.NO_RESULT{
            arrayCalories = (messgae as? [CaloriesConsumedModel])
            arrayCalConsumed = [CalConsumedModel]()
            tvCalHeightConst.constant = 100
            tvCal.tableHeaderView = addFooterView(msg: "You don't have any entries made yet! \nClick on \"Add Items\" now!")
            //            self.btSave.isEnabled=false
            //            self.btSave.backgroundColor = UIColor.lightGray
        }else{
            
            ShowDefaultImages.sharedInstance.showDefaultImage(view: self.view, type: errorValue,header: "",msg: messgae as? String ?? ""){
                [weak self] finished in
                
            }
        }
    }
    func onCalAddedSuccess(msg: Any?) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.CAL_BURNT), object: self, userInfo: ["data":msg as? [TrackerModel] as Any])
        goBack()
    }
    func onCalAddedError(msg: String?, error: ErrorType?) {
        self.showToast(msg: msg ?? "",height: 100)
    }
}

//MARK:- Tableview Functions
extension CaloriesConsumed : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCalConsumed.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CalBurntConsumedCell.self), for: indexPath) as? CalBurntConsumedCell
        cell?.hideOthers=false
        cell?.dataCalConsumed = arrayCalConsumed[indexPath.row]
        cell?.COUNT_CLICK = {
            [weak self] in
            self?.position = indexPath.row
            if !(self?.isPickerOpen)!{
                let value = (Double((self?.arrayCalConsumed[indexPath.row].og_quantity!)!)! - 1)
                self?.showPicker(selectedPos: (Int(value)))
            }
        }
        cell?.DELETE_CLICK = {
            [weak self] in
            self?.arrayCalConsumed.remove(at: indexPath.row)
            self?.arrayCalories?[0].calConsumed = self?.arrayCalConsumed
            self?.arrayCalories?[0].allFood = self?.originalFoodItem
            self?.tvCal.reloadData()
            self?.calculateCalories()
            
            if self?.arrayCalConsumed.count == 0 {
                //                self?.btSave.isEnabled=false
                //                self?.btSave.backgroundColor = UIColor.lightGray
                self?.tvCalHeightConst.constant = 100
                self?.tvCal.tableHeaderView = self?.addFooterView(msg: "You don't have any entries made yet! \nClick on \"Add Items\" now!")
            }
        }
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 57
    }
}

extension CaloriesConsumed : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayCounts.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayCounts[row]
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let color = (row == pickerView.selectedRow(inComponent: component)) ? UIColor.black : Colors.themeColor
        let attribute  = NSAttributedString(string: arrayCounts[row], attributes: [NSForegroundColorAttributeName : color])
        return attribute
    }
}

