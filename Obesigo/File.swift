//
//  File.swift
//  Obesigo
//
//  Created by Anuj  on 13/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol CommonDelegate : class {
    func onSuccess(successValue:Any)
    func onError(messgae:Any?,errorValue:ErrorType)
    func showIndicator(showIt:Bool,showTint:Bool)
}
