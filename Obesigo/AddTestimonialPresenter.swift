//
//  AddTestimonialPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 25/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol AddTestimonialDelegate : CommonDelegate {
    
}

class AddTestimonialPresenter{
    weak var delegate : AddTestimonialDelegate?
    var service : AddTestimonialService
    init(service:AddTestimonialService) {
        self.service=service
    }
    func attachView(delegate:AddTestimonialDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callAddTestimonialWS(params:[String:Any]){
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callAddTestimonialWS(params: params) {
            [weak self](success, error) in
            if error != nil{
              self?.delegate?.onError(messgae: success, errorValue: error!)
            }else{
              self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }

}

class AddTestimonialService{
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callAddTestimonialWS(params:[String:Any],result: @escaping onResponseReceived) {
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.TESTIMONIALS, type: .POST, params: params) {
            (success, error) in
            print(success as Any)
            print(error as Any)
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, nil)
            }
        }
    }
}
