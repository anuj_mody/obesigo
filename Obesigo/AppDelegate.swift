//
//  AppDelegate.swift
//  Obesigo
//
//  Created by Anuj  on 10/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit
import OneSignal
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])  // for crashlytics
        UIApplication.shared.statusBarStyle = .lightContent
        self.window?.tintColor = Colors.themeColor
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 3)], for: UIControlState.selected)
        if Preffrences().getUserId() != "" {
            
            if !Preffrences().getOTPEntered(){
                let storyboard = UIStoryboard(name: "SplashSB", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
                initialViewController.showQuote = false
               self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }else
                if !Preffrences().getBMIDone(){
                
                let storyboard = UIStoryboard(name: "BMICalculatorSB", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "BMICalculatorVC") as! BMICalculatorVC
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
                
            }else{
                let storyboard = UIStoryboard(name: "HomeMain", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
            
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "da8fa706-b5d5-48b5-bd2c-299a92134329",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        Constants.HOME_ARRAY = nil
        Constants.SHOW_BUYNOW_ALERT = true
    }
    
    
    //MARK:- Functions
    
}

