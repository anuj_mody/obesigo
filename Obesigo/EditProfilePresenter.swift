//
//  EditProfilePresenter.swift
//  Obesigo
//
//  Created by Anuj  on 11/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
protocol EditProfileDelegate : CommonDelegate {
    func showFNameError(message:String)
    func showLNameError(message:String)
}

class EditProfilePresenter {
    weak var delegate : EditProfileDelegate?
    var service : EditProfileService
    init(service:EditProfileService) {
        self.service=service
    }
    func attachView(delegate:EditProfileDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func checkFName(value:String)  {
        if service.checkFName(value: value) != nil{
            delegate?.showFNameError(message: (service.checkFName(value: value))!)
        }
    }
    func checkLName(value:String)  {
        if service.checkFName(value: value) != nil{
            delegate?.showLNameError(message: (service.checkLName(value: value))!)
        }
    }
    func callEditProfileWS(fName:String,lName:String,dob:String,deitaryPref:String,medHistory : [MedicalHistory],gender:String) {
        if service.checkFName(value: fName) != nil{
            delegate?.showFNameError(message: (service.checkFName(value: fName))!)
            return
        }
        if service.checkFName(value: lName) != nil{
            delegate?.showLNameError(message: (service.checkFName(value: lName))!)
            return
        }
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callEditProfileWS(fName: fName, lName: lName, dob: dob, deitaryPref: deitaryPref, medHistory: medHistory, gender: gender) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as? String ?? "", errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
        
    }
    func callEditProfileWithImageWS(images:[String:UIImage],fName:String,lName:String,dob:String,deitaryPref:String,medHistory : [MedicalHistory],gender:String)  {
        if service.checkFName(value: fName) != nil{
            delegate?.showFNameError(message: (service.checkFName(value: fName))!)
            return
        }
        if service.checkFName(value: lName) != nil{
            delegate?.showLNameError(message: (service.checkFName(value: lName))!)
            return
        }
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callEditProfileWithImageWS(images: images, fName: fName, lName: lName, dob: dob, deitaryPref: deitaryPref, medHistory: medHistory, gender: gender) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as? String ?? "", errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
        
    }
}

class EditProfileService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func checkFName(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_FNAME.rawValue
        }
        return nil
    }
    func checkLName(value:String) -> String?  {
        if value.isEmpty{
            return ErrorType.EMPTY_LNAME.rawValue
        }
        return nil
    }
    func callEditProfileWS(fName:String,lName:String,dob:String,deitaryPref:String,medHistory : [MedicalHistory],gender:String,result: @escaping onResponseReceived)  {
        var medHistoryService = ""
        for i in medHistory{
            if  i.has_user == 1{
                medHistoryService += "\(i.id!),"
            }
        }
        let params = ["screen_name":"Edit Profile",
                      "webservice":"Edit Profile",
                      "user_id":Preffrences().getUserId(),
                      "first_name":fName,
                      "last_name":lName,
                      "dob":"".getFormatedDate(fromFormat: "dd-MM-yyyy", toFormat: "yyyy-MM-dd", dateString: dob),
                      "diet_preference_id":deitaryPref == "Veg" ? "1" : "2",
                      "gender_lu_id":gender.lowercased() == "male" ? "1" : "2",
                      "medical_history":String(medHistoryService.characters.dropLast())]
        print(params)
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.EDIT_PROFILE, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                var array = [User]()
                var model : User?
                var medHistoryModel : MedicalHistory?
                var statsModel = StatsModel()
                var arrMedHistory = [MedicalHistory]()
                var arrStats = [StatsModel]()
                for i in  ((success as? NSDictionary)?.object(forKey: "response") as? NSArray)!{
                    let response = (i as? NSDictionary)
                    model = User()
                    model?.userId = response?.object(forKey: "user_id") as? Int
                    model?.access_till = response?.object(forKey: "access_till") as? String
                    model?.activated_on = response?.object(forKey: "activated_on") as? String
                    model?.api_token = response?.object(forKey: "api_token") as? String
                    model?.created_at = response?.object(forKey: "created_at") as? String
                    model?.diet_preference_id = Int((response?.object(forKey: "diet_preference_id") as? String)!)
                    model?.dob = response?.object(forKey: "dob") as? String
                    model?.email = response?.object(forKey: "email") as? String
                    model?.expire_on = response?.object(forKey: "expire_on") as? String
                    model?.fName = response?.object(forKey: "first_name") as? String
                    model?.lName = response?.object(forKey: "last_name") as? String
                    model?.mobile = response?.object(forKey: "mobile") as? String
                    model?.height = response?.object(forKey: "height") as? String
                    model?.profile_photo = response?.object(forKey: "profile_photo") as? String
                    model?.heightUnitId = response?.object(forKey: "height_unit") as? String
                    Preffrences().setProfilePic(value: (model?.profile_photo)!)
                    Preffrences().setName(value: (model?.fName)! + " " + (model?.lName)!)
                    model?.gender_lu_id = Int((response?.object(forKey: "gender_lu_id") as? String)!)
                    
                    //for med history
                    for j in (response?.object(forKey: "medical_history_list") as! NSArray){
                        medHistoryModel = MedicalHistory()
                        medHistoryModel?.has_user = Int(((j as? NSDictionary)?.object(forKey: "has_user") as? String)!)
                        medHistoryModel?.id = Int(((j as? NSDictionary)?.object(forKey: "id") as? String)!)
                        medHistoryModel?.name = (j as? NSDictionary)?.object(forKey: "name") as? String
                        arrMedHistory.append(medHistoryModel!)
                    }
                    
                    //for stats
                    statsModel.calories = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "calories") as? String
                    statsModel.sleep_log = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "sleep_log") as? String
                    statsModel.water_intake = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "water_intake") as? String
                    statsModel.weight = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "weight") as? String
                    arrStats.append(statsModel)
                    
                    if model?.arrStats == nil{
                        model?.arrStats = [StatsModel]()
                        model?.arrStats = arrStats
                    }
                    
                    if model?.arrMedHistory == nil{
                        model?.arrMedHistory = [MedicalHistory]()
                        model?.arrMedHistory = arrMedHistory
                    }
                    
                    
                    array.append(model!)
                }
                result(array as AnyObject, nil)
            }
        }
    }
    func callEditProfileWithImageWS(images:[String:UIImage],fName:String,lName:String,dob:String,deitaryPref:String,medHistory : [MedicalHistory],gender:String,result: @escaping onResponseReceived)  {
        var medHistoryService = ""
        for i in medHistory{
            if  i.has_user == 1{
                medHistoryService += "\(i.id!),"
            }
        }
        let params = ["screen_name":"Edit Profile",
                      "webservice":"Edit Profile",
                      "user_id":Preffrences().getUserId(),
                      "first_name":fName,
                      "last_name":lName,
                      "dob":"".getFormatedDate(fromFormat: "dd-MM-yyyy", toFormat: "yyyy-MM-dd", dateString: dob),
                      "diet_preference_id":deitaryPref == "Veg" ? "1" : "2",
                      "gender_lu_id":gender.lowercased() == "male" ? "1" : "2",
                      "medical_history":String(medHistoryService.characters.dropLast())]
        
        NetworkRequest.sharedInstance.uploadDataWithImage(url: NetworkUrls.EDIT_PROFILE, images: images, parameters: params){
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                guard (((success as? NSDictionary)?.object(forKey: "response") as? NSArray) != nil) else{
                    return result(nil, error)
                }
                var array = [User]()
                var model : User?
                var medHistoryModel : MedicalHistory?
                var statsModel = StatsModel()
                var arrMedHistory = [MedicalHistory]()
                var arrStats = [StatsModel]()
                for i in  ((success as? NSDictionary)?.object(forKey: "response") as? NSArray)!{
                    let response = (i as? NSDictionary)
                    model = User()
                    model?.userId = response?.object(forKey: "user_id") as? Int
                    model?.access_till = response?.object(forKey: "access_till") as? String
                    model?.activated_on = response?.object(forKey: "activated_on") as? String
                    model?.api_token = response?.object(forKey: "api_token") as? String
                    model?.created_at = response?.object(forKey: "created_at") as? String
                    model?.diet_preference_id = Int((response?.object(forKey: "diet_preference_id") as? String)!)
                    model?.dob = response?.object(forKey: "dob") as? String
                    model?.email = response?.object(forKey: "email") as? String
                    model?.expire_on = response?.object(forKey: "expire_on") as? String
                    model?.fName = response?.object(forKey: "first_name") as? String
                    model?.lName = response?.object(forKey: "last_name") as? String
                    model?.mobile = response?.object(forKey: "mobile") as? String
                    model?.height = response?.object(forKey: "height") as? String
                    model?.profile_photo = response?.object(forKey: "profile_photo") as? String
                    Preffrences().setProfilePic(value: (model?.profile_photo)!)
                    model?.gender_lu_id = Int((response?.object(forKey: "gender_lu_id") as? String)!)
                    Preffrences().setName(value: (model?.fName)! + " " + (model?.lName)!)
                    //for med history
                    for j in (response?.object(forKey: "medical_history_list") as! NSArray){
                        medHistoryModel = MedicalHistory()
                        medHistoryModel?.has_user = Int(((j as? NSDictionary)?.object(forKey: "has_user") as? String)!)
                        medHistoryModel?.id = Int(((j as? NSDictionary)?.object(forKey: "id") as? String)!)
                        medHistoryModel?.name = (j as? NSDictionary)?.object(forKey: "name") as? String
                        arrMedHistory.append(medHistoryModel!)
                    }
                    
                    //for stats
                    statsModel.calories = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "calories") as? String
                    statsModel.sleep_log = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "sleep_log") as? String
                    statsModel.water_intake = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "water_intake") as? String
                    statsModel.weight = (response?.object(forKey: "stats") as? NSDictionary)?.object(forKey: "weight") as? String
                    arrStats.append(statsModel)
                    
                    if model?.arrMedHistory == nil{
                        model?.arrMedHistory = [MedicalHistory]()
                        model?.arrMedHistory = arrMedHistory
                    }
                    
                    if model?.arrStats == nil{
                        model?.arrStats = [StatsModel]()
                        model?.arrStats = arrStats
                    }
                    array.append(model!)
                }
                result(array as AnyObject, nil)
            }
        }
    }
}
