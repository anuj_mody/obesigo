//
//  BMIDietaryPrefPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 05/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol BMIDietaryPrefDelegae  : CommonDelegate    {
    
}

class BMIDietaryPrefPresenter {
    weak var delegate : BMIDietaryPrefDelegae?
    var service : BMIDietaryPrefService
    init(service:BMIDietaryPrefService) {
        self.service=service
    }
    func attachView(delegate:BMIDietaryPrefDelegae){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callAddTestimonialWS(params:[String:Any]){
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callAddBasicStatsWS(params: params) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success, errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: true)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
}

class BMIDietaryPrefService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callAddBasicStatsWS(params:[String:Any],result: @escaping onResponseReceived)  {
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.BMI_DIETARYPREF, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                var stats = Preffrences().getUserStats()
                for i in ((success as? NSDictionary)?.object(forKey: "response") as? NSArray)!{
                    stats?["weight"] = (i as? NSDictionary)?.object(forKey: "weight") as? String
                    Preffrences().setUserStats(value: stats!)
                    result(true as AnyObject, nil)
                }
            }
        }
    }
}
