//
//  WeightGraphCell.swift
//  Obesigo
//
//  Created by Anuj  on 26/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import UIKit
import Charts

class WeightGraphCell: UITableViewCell {
    
    @IBOutlet var chartView: LineChartView!
    @IBOutlet var lblUnit: UILabel!
    @IBOutlet var lblHeader: UILabel!
    
    override func awakeFromNib() {
        lblUnit.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
    }
    
    
    var headers:[String]!{
        didSet{
            lblUnit.text = headers[0]
            lblHeader.text = headers[1]
        }
    }
    
    var data:[Logs]?{
        didSet{
            var months=[String]()
            var counts = [String]()
            for i in (data?.reversed())!{
                let date = (i.log_date?.components(separatedBy: ",")[0])
                months.append("".getFormatedDate(fromFormat: "dd MMMM", toFormat: "dd MMM", dateString: date!))
                counts.append(i.log_val!)
            }
            chartView.data =  setCharts(dataPoints: months, values: counts)
            chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: months);
            chartView.xAxis.labelPosition = .bottom
            chartView.xAxis.granularity = 1;
            chartView.animate(xAxisDuration: 1.0)
            chartView.rightAxis.labelTextColor = UIColor.clear
            chartView.tintColor = Colors.themeColor
            chartView.legend.enabled = false
            chartView.chartDescription?.text = ""
            chartView.xAxis.labelHeight = 20
            chartView.xAxis.labelFont = UIFont.systemFont(ofSize: 10)
            //chartView.legend.font = UIFont(name: "Verdana", size: 16.0)!
        }
    }
    
    //MARK:- Functions
    func setCharts(dataPoints: [String],values:[String]) -> LineChartData {
        let chartData : LineChartData?
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: Double(values[i])!)
            dataEntries.append(dataEntry)
        }
        let lineChatDataSet = LineChartDataSet(values: dataEntries, label: nil)
        lineChatDataSet.circleColors = [Colors.themeColor];
        lineChatDataSet.colors = [Colors.themeColor];
        lineChatDataSet.circleRadius = 3.0;
        //lineChatDataSet.valueFont=UIFont(name: "Verdana", size: 12.0)!
        
        chartData = LineChartData(dataSets: [lineChatDataSet])
        return chartData!
    }
}
