//
//  BMICalculatorVC.swift
//  Obesigo
//
//  Created by Anuj  on 14/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit

class BMICalculatorVC : BaseVC{
    
    
    //MARK:- Outlets
    @IBOutlet var tfFt: UITextField!
    @IBOutlet var viewFt: UIView!
    @IBOutlet var tfCm: UITextField!
    @IBOutlet var viewCm: UIView!
    @IBOutlet var tfMeter: UITextField!
    @IBOutlet var viewMeters: UIView!
    @IBOutlet var lblFt: UILabel!
    @IBOutlet var lblHeightUnit: UILabel!
    @IBOutlet var ivPointer: UIImageView!
    @IBOutlet var lblBMI: UILabel!
    @IBOutlet var lblHeightError: UILabel!
    @IBOutlet var lblWeightError: UILabel!
    @IBOutlet var tfWeight: UITextField!
    @IBOutlet var lblIdealWeight: UILabel!
    @IBOutlet var lblTargetWeight: UILabel!
    
    //MARK:- variables
    var meter : Double?
    var selectedUnit = 1
    var previousSelected = 1
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        //            self.rotateNeedle(value: 30, duration: 0.5)
        //        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    // MARK:- Functions
    func changeHeightUnit()  {
        lblHeightError.isHidden=true
        lblWeightError.isHidden=true
        switch selectedUnit {
        case 1:
            self.addInputAccessoryForTextFields(textFields: [self.tfMeter,self.tfWeight])
            self.tfFt.isHidden=true
            self.viewFt.isHidden=true
            self.tfCm.isHidden=true
            self.viewCm.isHidden=true
            self.lblFt.isHidden=true
            self.tfMeter.isHidden=false
            self.viewMeters.isHidden=false
            tfMeter.placeholder = "metre"
            break
            
        case 2:
            self.addInputAccessoryForTextFields(textFields: [self.tfMeter,self.tfWeight])
            self.tfFt.isHidden=true
            self.viewFt.isHidden=true
            self.tfCm.isHidden=true
            self.viewCm.isHidden=true
            self.lblFt.isHidden=true
            self.tfMeter.isHidden=false
            self.viewMeters.isHidden=false
            tfMeter.placeholder = "Cms"
            break
            
        case 3:
            self.addInputAccessoryForTextFields(textFields: [self.tfFt,self.tfCm,self.tfWeight])
            self.tfFt.isHidden=false
            self.viewFt.isHidden=false
            self.tfCm.isHidden=false
            self.viewCm.isHidden=false
            //self.lblFt.isHidden=false
            self.tfMeter.isHidden=true
            self.viewMeters.isHidden=true
            tfFt.placeholder = "ft"
            tfCm.placeholder = "inch"
            break
        default:
            break
        }
        //        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
        //
        //
        //            if showFeet{
        //                self.addInputAccessoryForTextFields(textFields: [self.tfFt,self.tfCm,self.tfWeight])
        //                self.tfFt.isHidden=false
        //                self.viewFt.isHidden=false
        //                self.tfCm.isHidden=false
        //                self.viewCm.isHidden=false
        //                self.lblFt.isHidden=false
        //                self.tfMeter.isHidden=true
        //                self.viewMeters.isHidden=true
        //            }else{
        //                self.addInputAccessoryForTextFields(textFields: [self.tfMeter,self.tfWeight])
        //                self.tfFt.isHidden=true
        //                self.viewFt.isHidden=true
        //                self.tfCm.isHidden=true
        //                self.viewCm.isHidden=true
        //                self.lblFt.isHidden=true
        //                self.tfMeter.isHidden=false
        //                self.viewMeters.isHidden=false
        //            }
        //
        //        }, completion: nil)
        
        
        
    }
    func rotateNeedle(value: Double, duration: Double)
    {
        
        let fullRotation = CABasicAnimation(keyPath: "transform.rotation")
        fullRotation.fromValue = Double(0)
        fullRotation.toValue = Double((value * M_PI)/180)
        fullRotation.fillMode = kCAFillModeForwards
        fullRotation.duration = duration
        fullRotation.isRemovedOnCompletion = false
        
        // add animation to your view
        //self.ivPointer.layer.anchorPoint = set
        //setAnchorPoint(anchorPoint: CGPoint(x: 1, y: 1), view: ivPointer)
        self.ivPointer.layer.add(fullRotation, forKey: "rotateViewAnimation")
        
        //        let fullRotation = CABasicAnimation(keyPath: "transform.rotation")
        //
        //        fullRotation.fromValue = Double(0)
        //        fullRotation.toValue = Double((value * M_PI)/180)
        //        fullRotation.fillMode = kCAFillModeForwards
        //        fullRotation.duration = duration
        //        fullRotation.isRemovedOnCompletion = false
        //
        //        // add animation to your view
        //        ivPointer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.6)
        //        ivPointer.layer.add(fullRotation, forKey: "rotateViewAnimation")
        
        
        
        /*ivPointer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
         _ = 30 * M_PI / 180;
         ivPointer.transform =       CGAffineTransform(translationX: <#T##CGFloat#>, y: <#T##CGFloat#>)*/
        
        /* let fullRotation = CABasicAnimation(keyPath: "transform.rotation")
         fullRotation.fromValue = Double(0)
         fullRotation.toValue = Double((value * M_PI)/180)
         fullRotation.fillMode = kCAFillModeForwards
         fullRotation.duration = duration
         fullRotation.isRemovedOnCompletion = false
         
         // add animation to your view
         //self.pointerImg.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
         self.ivPointer.layer.add(fullRotation, forKey: "rotateViewAnimation")*/
        
        //        ivPointer.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
        //        let angle = Double((value * Double.pi)/180)
        ////        var transform = CGAffineTransform.identity.rotated(by: CGFloat(angle))
        //        //ivPointer.layer.transform = CATransform3DMakeRotation(CGFloat(angle), 0, 0, 1)
        //
        //       // let radians = CGFloat(angle) / 180.0 * CGFloat(Double.pi)
        //        let rotation = ivPointer.transform.rotated(by: CGFloat(angle));
        //        ivPointer.transform = rotation
        
    }
    func setUp()  {
        self.hideKeyboardWhenTappedAround()
        tfMeter.delegate=self
        tfCm.delegate=self
        tfFt.delegate=self
        tfWeight.delegate=self
        self.addInputAccessoryForTextFields(textFields: [tfMeter, tfWeight])
        lblBMI.layer.borderWidth = 1
        lblBMI.layer.borderColor = UIColor.white.cgColor
        lblBMI.layer.cornerRadius = 5
        //rotateNeedle(value: 0, duration: 0.7)
        
    }
    func calculateMeters(fromFeet: Bool, isWeightAdded: Bool = false){
        
        if selectedUnit == 3 {
            meter = (Double(tfFt.text ?? "0")! * 0.3048).round(to: 2) + (Double(tfCm.text ?? "0")! * 0.0254).round(to: 2)
        }else if selectedUnit == 2{
            meter = (Double((self.tfMeter.text!))! / 100).round(to: 2)
        }else{
            meter = Double(tfMeter.text!)?.round(to: 2)
        }
        if isWeightAdded{
            calculateBMI()
        }
        
    }
    func calculateBMI()  {
        let BMIvalue =  Double(tfWeight.text!)! / ((meter)! * (meter)!)
        lblBMI.text = String(BMIvalue.round(to: 2))
        let idealWeight = 24.9 * (meter! * meter!)
        
        if 18.5...24.9 ~= idealWeight{
            lblTargetWeight.isHidden=true
            lblIdealWeight.text = "You are at your target weight"
        }else{
            lblTargetWeight.isHidden=false
            if idealWeight > Double(tfWeight.text!)!{
                lblIdealWeight.text = "Your are \((idealWeight - Double(tfWeight.text!)!).round(to: 2)) under weight"
            }else{
                lblIdealWeight.text = "Your are \(abs(idealWeight - Double(tfWeight.text!)!).round(to: 2)) over weight"
            }
        }
        
        switch BMIvalue {
        //Underweight
        case 0...9.5:
            rotateNeedle(value: -79, duration: 0.7)
            lblBMI.layer.borderColor = Colors.bmiBlueColor.cgColor
        case 9.6...18.5:
            rotateNeedle(value: -56, duration: 0.7)
            lblBMI.layer.borderColor = Colors.bmiBlueColor.cgColor
        //Normal
        case 18.5...20.5:
            rotateNeedle(value: -39, duration: 0.7)
            lblTargetWeight.isHidden=true
            lblIdealWeight.text = "You are at your target weight"
            lblBMI.layer.borderColor = Colors.bmiGreenColor.cgColor
        case 20.5...24.9:
            rotateNeedle(value: -11, duration: 0.7)
            lblTargetWeight.isHidden=true
            lblIdealWeight.text = "You are at your target weight"
            lblBMI.layer.borderColor = Colors.bmiGreenColor.cgColor
        //Overweight
        case 25...27.5:
            rotateNeedle(value: 11, duration: 0.7)
            lblBMI.layer.borderColor = Colors.bmiYellowColor.cgColor
        case 27.6...29.9:
            rotateNeedle(value: 39, duration: 0.7)
            lblBMI.layer.borderColor = Colors.bmiYellowColor.cgColor
        //Obese
        case 30...50:
            rotateNeedle(value: 56, duration: 0.7)
            lblBMI.layer.borderColor = Colors.bmiRedColor.cgColor
        case 50...BMIvalue:
            rotateNeedle(value: 79, duration: 0.7)
            lblBMI.layer.borderColor = Colors.bmiRedColor.cgColor
            
        default:
            break
        }
    }
    func setAnchorPoint(anchorPoint: CGPoint, view: UIView) {
        var newPoint = CGPoint(x:view.bounds.size.width * anchorPoint.x,y: view.bounds.size.height * anchorPoint.y)
        var oldPoint = CGPoint(x:view.bounds.size.width * view.layer.anchorPoint.x, y:view.bounds.size.height * view.layer.anchorPoint.y)
        newPoint = newPoint.applying(view.transform)
        oldPoint = oldPoint.applying(view.transform)
        var position : CGPoint = view.layer.position
        
        position.x -= oldPoint.x
        position.x += newPoint.x;
        
        position.y -= oldPoint.y;
        position.y += newPoint.y;
        
        view.layer.position = position;
        view.layer.anchorPoint = anchorPoint;
    }
    
    
    // MARK:- Button Actions
    @IBAction func btHeightUnit(_ sender: Any) {
        self.view.endEditing(true)
        self.showActionSheet(data: ["Metre","Cms","Feet"]) {
            [weak self] (value) -> (Void) in
            if value == "Metre"{
                self?.selectedUnit = 1
                self?.lblHeightUnit.text = "Mtr"
                self?.changeHeightUnit()
            }else if value == "Feet"{
                self?.selectedUnit = 3
                self?.lblHeightUnit.text = "Ft"
                self?.changeHeightUnit()
            }else{
                self?.selectedUnit = 2
                self?.lblHeightUnit.text = "Cms"
                self?.changeHeightUnit()
            }
            
            self?.tfMeter.text = ""
            self?.tfFt.text = ""
            self?.tfCm.text = ""
            //            if self?.previousSelected == 1{
            //                if self?.selectedUnit == 2{
            //                    if !(self?.tfMeter.text?.isEmpty)!{
            //                        self?.tfMeter.text = String(Int(((Double((self?.tfMeter.text!)!)?.round(to: 2))! * 100)))
            //                        self?.previousSelected=2
            //                    }
            //                }else{
            //                    if !(self?.tfMeter.text?.isEmpty)!{
            //                        let feet = (Double((self?.tfMeter.text!)!)! / 0.3048)
            //                        let strFeet = String(feet.round(to: 2)).components(separatedBy: ".")[0]
            //                        let strInch = String(feet.round(to: 2)).components(separatedBy: ".")[1]
            //                        self?.tfFt.text = strFeet
            //                        self?.tfCm.text = strInch
            //                        self?.previousSelected=3
            //                    }
            //                }
            //                self?.previousSelected=(self?.selectedUnit)!
            //            }else if self?.previousSelected == 2{
            //                if self?.selectedUnit == 1{
            //                    if !(self?.tfMeter.text?.isEmpty)!{
            //                        self?.tfMeter.text = String((Double((self?.tfMeter.text!)!)! / 100).round(to: 2))
            //                        self?.previousSelected=1
            //                    }
            //                }else{
            //
            //                    if !(self?.tfMeter.text?.isEmpty)!{
            //                        let feet = (Double((self?.tfMeter.text!)!)! / 30.48)
            //                        print(feet.round(to: 2))
            //                        let strFeet = String(feet.round(to: 2)).components(separatedBy: ".")[0]
            //                        let strInch = String(feet.round(to: 2)).components(separatedBy: ".")[1]
            //                        self?.tfFt.text = strFeet
            //                        self?.tfCm.text = strInch
            //                        self?.previousSelected=3
            //                    }
            //                }
            //                self?.previousSelected=(self?.selectedUnit)!
            //            }else if self?.previousSelected == 3 {
            //
            //                if !(self?.tfFt.text?.isEmpty)! || !(self?.tfCm.text?.isEmpty)!{
            //                    let strFeet = "\(self?.tfFt.text ?? "0").\(self?.tfCm.text ?? "0")"
            //                    print(strFeet)
            //                    let feet : Double?
            //                    if  self?.selectedUnit == 1{
            //                        feet = Double(strFeet)! * 0.3048
            //                        self?.tfMeter.text = String(describing: feet!.round(to: 2))
            //                        self?.previousSelected=1
            //                    }else{
            //                        feet = Double(strFeet)! * 30.48.round(to: 2)
            //                        self?.tfMeter.text = String(describing: feet!.round(to: 2))
            //                        self?.previousSelected=2
            //                    }
            //                }
            //
            //
            //            }
        }
    }
    @IBAction func btNext(_ sender: Any) {
        
        if selectedUnit == 1 || selectedUnit == 2{
            if (tfMeter.text?.isEmpty)!{
                lblHeightError.isHidden=false
                return
            }
        }
        if selectedUnit == 3{
            if (tfFt.text?.isEmpty)! || (tfCm.text?.isEmpty)!{
                lblHeightError.isHidden=false
                return
            }
        }
        if (tfWeight.text?.isEmpty)!{
            lblWeightError.isHidden=false
            return
        }
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: "DietaryPrefSB", identifier: "DietaryPrefVC") as? DietaryPrefVC
        vc?.weight = tfWeight.text
        vc?.meters = String(describing: meter!)
        vc?.selectedUnit = selectedUnit
        self.switchViewController(viewController: vc)
    }
}

extension BMICalculatorVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lblBMI.text = ""
        lblIdealWeight.text = ""
        lblTargetWeight.isHidden = true
        rotateNeedle(value: 0, duration: 0.5)
        lblBMI.layer.borderColor = UIColor.white.cgColor
        if textField == tfMeter || textField == tfFt || textField == tfCm{
            lblHeightError.isHidden=true
        }else if textField == tfWeight{
            if tfMeter.isHidden{
                if !(tfFt.text?.isEmpty)! && !(tfCm.text?.isEmpty)!{
                    calculateMeters(fromFeet: true)
                }
            }else{
                if !(tfMeter.text?.isEmpty)!{
                    calculateMeters(fromFeet: false)
                }
            }
            lblWeightError.isHidden=true
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == tfMeter || textField == tfFt || textField == tfCm{
            if (textField.text?.isEmpty)!{
                lblHeightError.isHidden=false
            }
            
            if tfMeter.isHidden{
                if !(tfFt.text?.isEmpty)! && !(tfCm.text?.isEmpty)!{
                    calculateMeters(fromFeet: true,isWeightAdded: (tfWeight.text?.isEmpty)! ? false : true)
                }
            }else{
                if !(tfMeter.text?.isEmpty)!{
                    calculateMeters(fromFeet: false,isWeightAdded: (tfWeight.text?.isEmpty)! ? false : true)
                }
            }
            
        }else if textField == tfWeight{
            
            if (textField.text?.isEmpty)!{
                lblWeightError.isHidden=false
            }else{
                if tfMeter.isHidden{
                    if !(tfFt.text?.isEmpty)! && !(tfCm.text?.isEmpty)!{
                        calculateMeters(fromFeet: true,isWeightAdded: true)
                    }
                }else{
                    if !(tfMeter.text?.isEmpty)!{
                        calculateMeters(fromFeet: false,isWeightAdded: true)
                    }
                }
            }
        }
        
    }
}

