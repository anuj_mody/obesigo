//
//  HomeModel.swift
//  Obesigo
//
//  Created by Anuj  on 04/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

struct HomeModel {
    var isPurchase : String?
    var data : [DataModel]?
    var day : Int?
    var currentDay : Int?
}


struct DataModel {
    var isImage=false
    var isPurchaseImage=false
    var imageUrl:String?
    var isMealHeader = false
    var isExerciseHeader = false
    var isMealData=false
    var isExerciseData=false
    var isBuyNowCell=false
    var isPurchaseCell=false
    var mealName:String?
    var mealData : [MealsModel]?
    var exerciseName:String?
    var exerciseTime:String?
    var exerciseUnit:String?
}

struct DaysModel {
    var day_number : Int?
    var img_url:[String]?
    var mealName:String?
    var mealData : [[String:[MealsModel]]]?
}

struct MealsModel {
    var quantity: String?
    var item_name : String?
    var units:String?
    var calories:String?
}




