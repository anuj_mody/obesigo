//
//  User.swift
//  Obesigo
//
//  Created by Anuj  on 13/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

struct User {
    var fName:String?
    var lName:String?
    var userId:Int?
    var api_token:String?
    var access_till:String?
    var activated_on:String?
    var activation_code:String?
    var age:String?
    var created_at:String?
    var diet_preference_id:Int?
    var dob:String?
    var email:String?
    var expire_on:String?
    var gender_lu_id:Int?
    var height:String?
    var mobile:String?
    var heightUnitId:String?
    var is_mobile_verified:String?
    var profile_photo:String?
    var arrMedHistory : [MedicalHistory]?
    var arrStats : [StatsModel]?
}

struct MedicalHistory {
    var has_user : Int?
    var id:Int?
    var name:String?
}

struct StatsModel {
    var calories:String?
    var sleep_log:String?
    var water_intake:String?
    var weight:String?
}

struct UserReminderModel {
    var days : String?
    var id:Int?
    var name:String?
    var time:String?
}

class RemindersModel: NSObject, NSCoding {
    var days : String?
    var id : String?
    var name:String?
    var time:String?
    var userId:String?
    
    init(days:String,id:String,name:String,time:String,userId:String) {
        self.days = days
        self.id = id
        self.name = name
        self.time = time
        self.userId = userId
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.days = aDecoder.decodeObject(forKey: "days") as? String
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.time = aDecoder.decodeObject(forKey: "time") as? String
        self.userId = aDecoder.decodeObject(forKey: "userId") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(days, forKey: "days")
        aCoder.encode(time, forKey: "time")
        aCoder.encode(userId, forKey: "userId")
    }
}
