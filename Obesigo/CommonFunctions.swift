//
//  CommonFunctions.swift
//  Obesigo
//
//  Created by Anuj  on 08/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

struct CommonFunctions {
    typealias ON_CLICK = ((_ isPosButton:Bool)->Void)
    func showAlert(viewController:UIViewController,title: String = "",message: String,showReverseButtons: Bool = false,posButton: String = "Yes",negButton: String = "No",onClick : @escaping ON_CLICK) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let posButton = UIAlertAction(title: posButton, style: .default){
            finished in
            onClick(true)
        }
        let negButton = UIAlertAction(title: negButton, style: .default){
            finished in
            onClick(false)
        }
        if showReverseButtons{
            alert.addAction(negButton)
            alert.addAction(posButton)
        }else{
            alert.addAction(posButton)
            alert.addAction(negButton)
        }
        viewController.present(alert, animated: true, completion: nil)
    }
}
