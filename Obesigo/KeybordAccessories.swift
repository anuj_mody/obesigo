//
//  KeybordAccessories.swift
//  MonishaJaising
//
//  Created by Archana Vetkar on 17/05/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit
class KeybordAccessories:NSObject {
    static var scrollView = UIScrollView()
    static var activeField : UITextField?;
    static var activeTextView : UITextView?;
    static var actualContentInset = UIEdgeInsets()
    static var view = UIView();
    static var selectedFrame : CGRect?;
    
    
    static func addKeyboardNotification(scrollView:UIScrollView, view:UIView, activeField:UITextField? = nil, activeTextView:UITextView? = nil,actualContentInset:UIEdgeInsets)
    {
        self.scrollView=scrollView;
        self.view = view;
        if activeField == nil{
            self.activeTextView=activeTextView;
        }else{
            self.activeField=activeField;
        }
        self.actualContentInset = actualContentInset
        NotificationCenter.default.addObserver(self, selector: #selector(KeybordAccessories.animateKeyboardUp(notification:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(KeybordAccessories.hideKeyboard), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
    }
    static func activeTextField (textField:UITextField?,textView:UITextView?,frame:CGRect?=nil){
        if textField == nil{
            self.activeTextView=textView;
        }else{
            self.activeField=textField;
        }
        self.selectedFrame = frame
    }
    static func animateKeyboardUp(notification:NSNotification){
        let userInfo = notification.userInfo!
        let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue;
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = view.frame
        aRect.size.height -= keyboardSize!.height
        if let _ = activeField
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                if selectedFrame == nil{
                    scrollView.scrollRectToVisible(activeField!.frame, animated: true)
                }else{
                    scrollView.scrollRectToVisible(selectedFrame!, animated: true)
                }
            }
        }else{
            if let _ = activeTextView
            {
                if selectedFrame == nil{
                    scrollView.scrollRectToVisible(activeTextView!.frame, animated: true)
                }else{
                    scrollView.scrollRectToVisible(selectedFrame!, animated: true)
                }
            }
        }
    }
    static func hideKeyboard(){
        scrollView.contentOffset.y = 0
        scrollView.contentInset = actualContentInset
        scrollView.scrollIndicatorInsets = actualContentInset
    }
    static func removeKeyboardObserver(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    static func addInputAccessoryForTextFields(textFields: [UITextField], dismissable: Bool = true, previousNextable: Bool = false,showDone : Bool = true) {
        
        for (index, textField) in textFields.enumerated() {
            
            let toolbar: UIToolbar = UIToolbar()
            toolbar.sizeToFit()
            toolbar.tintColor=Colors.themeColor
            var items = [UIBarButtonItem]()
            if previousNextable {
                let previousButton = UIBarButtonItem(title: "Previous", style: .plain, target: nil, action: nil)
                if textField == textFields.first {
                    previousButton.isEnabled = false
                } else {
                    previousButton.target = textFields[index - 1]
                    previousButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                _ = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                
                
                let nextButton = UIBarButtonItem(title: "Next", style: .plain, target: nil, action: nil)
                if textField == textFields.last {
                    nextButton.isEnabled = false
                } else {
                    nextButton.target = textFields[index + 1]
                    nextButton.action = #selector(UITextField.becomeFirstResponder)
                }
                
                items.append(contentsOf: [previousButton,nextButton])
            }
            
            let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            if(showDone){
                
                let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: nil, action: nil)
                doneButton.target = textFields[index]
                doneButton.action = #selector(UITextField.resignFirstResponder)
                items.append(contentsOf: [spacer, doneButton])
            }else{
                
                items.append(contentsOf: [spacer])
            }
            toolbar.barTintColor = UIColor.white
            toolbar.setItems(items, animated: false)
            textField.inputAccessoryView = toolbar
        }
    }
    static func addInputAccessoryForTextView(textFields: [UITextView], dismissable: Bool = true, previousNextable: Bool = false) {
        
        for (index, textField) in textFields.enumerated() {
            
            let toolbar: UIToolbar = UIToolbar()
            toolbar.sizeToFit()
            
            var items = [UIBarButtonItem]()
            if previousNextable {
                let previousButton = UIBarButtonItem(image: UIImage(named: "ic_tickmark"), style: .plain, target: nil, action: nil)
                previousButton.tintColor = UIColor.white
                previousButton.width = 20
                if textField == textFields.first {
                    previousButton.isEnabled = false
                } else {
                    previousButton.target = textFields[index - 1]
                    previousButton.action = #selector(UITextView.becomeFirstResponder)
                }
                
                let nextButton = UIBarButtonItem(image: UIImage(named: "ic_tickmark"), style: .plain, target: nil, action: nil)
                nextButton.width = 30
                nextButton.tintColor = UIColor.white
                if textField == textFields.last {
                    nextButton.isEnabled = false
                } else {
                    nextButton.target = textFields[index + 1]
                    nextButton.action = #selector(UITextView.becomeFirstResponder)
                }
                items.append(contentsOf: [previousButton, nextButton])
            }
            
            let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: scrollView, action: #selector(UITextView.resignFirstResponder))
            //doneButton.tintColor = UIColor.MJColor.GoldenColor
            items.append(contentsOf: [spacer, doneButton])
            
            toolbar.setItems(items, animated: false)
            textField.inputAccessoryView = toolbar
        }
    }
}
