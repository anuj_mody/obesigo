//
//  DietaryPrefVC.swift
//  Obesigo
//
//  Created by Anuj  on 14/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit

class DietaryPrefVC: BaseVC {
    
    // MARK:- outlets
    @IBOutlet var ivVeg: UIImageView!
    @IBOutlet var ivNonVeg: UIImageView!
    @IBOutlet var btNext: UIButton!
    
    
    // MARK:- variables
    var isVegSelected = false
    var isNonVegSelected=false
    var weight : String?
    var meters : String?
    var dietaryPrefId:String?
    var selectedUnit:Int?
    var presenter = BMIDietaryPrefPresenter(service: BMIDietaryPrefService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Functions
    func setUp() {
        presenter.attachView(delegate: self)
        ivVeg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DietaryPrefVC.onVegClicked)))
        ivVeg.isUserInteractionEnabled=true
        ivNonVeg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DietaryPrefVC.onNonVegClicked)))
        ivNonVeg.isUserInteractionEnabled=true
        
    }
    func onVegClicked() {
        btNext.setTitleColor(UIColor.white, for: .normal)
        btNext.isEnabled=true
        isNonVegSelected=false
        dietaryPrefId = "1"
        scaleImage(selectedImage: ivVeg, scaleIn: false, isVeg: true)
    }
    func onNonVegClicked()  {
        btNext.setTitleColor(UIColor.white, for: .normal)
        btNext.isEnabled=true
        isVegSelected=false
        dietaryPrefId = "2"
        scaleImage(selectedImage: ivNonVeg, scaleIn: false, isVeg: false)
    }
    func scaleImage(selectedImage:UIView?,scaleIn:Bool,isVeg:Bool){
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.1, options: .beginFromCurrentState, animations: {
            if scaleIn{
                selectedImage?.transform = .identity
                if isVeg{
                    self.ivVeg.image=#imageLiteral(resourceName: "ic_veg_unselected")
                }else{
                    self.ivNonVeg.image=#imageLiteral(resourceName: "ic_nonveg_unselected")
                }
            }else{
                selectedImage?.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                if isVeg{
                    self.ivVeg.image=#imageLiteral(resourceName: "ic_veg_selected")
                    self.ivNonVeg.image=#imageLiteral(resourceName: "ic_nonveg_unselected")
                }else{
                    self.ivNonVeg.image=#imageLiteral(resourceName: "ic_nonveg_selected")
                    self.ivVeg.image=#imageLiteral(resourceName: "ic_veg_unselected")
                }
            }
            if isVeg{
                self.ivNonVeg.transform = .identity
            }else{
                self.ivVeg.transform = .identity
            }
        }) { (true) in
            
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btNext(_ sender: Any) {
        
        let params = ["screen_name":"DietaryPref",
                      "webservice":"Submit user basic details",
                      "user_id":Preffrences().getUserId(),
                      "weight":weight!,
                      "height":meters!,
                      "height_unit":String(selectedUnit ?? 1),
            "diet_preference_id":dietaryPrefId!] as [String : Any]
        presenter.callAddTestimonialWS(params: params)
    }
    @IBAction func btPrevious(_ sender: Any) {
        self.goBack()
    }
    
}
extension DietaryPrefVC : BMIDietaryPrefDelegae{
    func showIndicator(showIt: Bool, showTint: Bool) {
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "HomeMain", identifier: "SWRevealViewController") as! SWRevealViewController)
        Preffrences().setBMIDone(value: true)
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        
    }
}
