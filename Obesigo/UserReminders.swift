//
//  UserReminders.swift
//  Obesigo
//
//  Created by Anuj  on 15/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UserNotifications

class UserReminders {
    static let sharedInstance = UserReminders()
    //this is to add the reminders
    func addReminder(title:String,subtitle:String="",body:String="",id:String,weekDay:Int,hours:Int,mins:Int) {
        var dateComponents = DateComponents()
        dateComponents.weekday = weekDay
        dateComponents.hour = hours
        dateComponents.minute = mins
        if #available(iOS 10.0, *) {
            let notification = UNMutableNotificationContent()
            notification.title = "Reminder"
            notification.subtitle = title
            notification.body = " "
            let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            let request = UNNotificationRequest(identifier: "\(id)_\(weekDay)", content: notification, trigger: notificationTrigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: {
                (error) in
                if let error = error {
                    // Something went wrong
                    print(error.localizedDescription)
                }
            })
           
        } else {
            // Fallback on earlier versions
            let notification = UILocalNotification()
            notification.fireDate = Calendar.current.date(from: dateComponents)
            if #available(iOS 8.2, *) {
                notification.alertTitle = "Reminder"
            } else {
                // Fallback on earlier versions
            }
            notification.alertBody = title
            notification.alertAction = "Reminder"
            notification.repeatInterval = .day
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.userInfo = ["id":"\(id)_\(weekDay)"]
            UIApplication.shared.scheduleLocalNotification(notification)
        }
        
    }
    
     //this is to remove a particular reminder
    func removeNotifications(reminderId:String) {
        if #available(iOS 10.0, *) {
            var notificationIds = [String]()
            for i in 1 ..< 8{
                notificationIds.append("\(reminderId)_\(i)")
                print(notificationIds[i-1])
            }
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: notificationIds)
        } else {
            for i in UIApplication.shared.scheduledLocalNotifications!{
                for k in 1 ..< 8{
                    print(i.userInfo?["id"] as! String)
                    print("\(reminderId)_\(k)")
                    if i.userInfo?["id"] as! String == "\(reminderId)_\(k)"{
                        UIApplication.shared.cancelLocalNotification(i)
                    }
                }
            }
        }
    }
    
     //this is to remove all the reminders
    func removeAllNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        } else {
            // Fallback on earlier versions
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    
    
    func addRemindersForFirstTime() {
        if (Preffrences().getReminderArray()?.count)! > 0 {
            for i in Preffrences().getReminderArray()!{
                let hours = "".getFormatedDate(fromFormat: "HH:mm:ss", toFormat: "HH", dateString: i.time!)
                let min = "".getFormatedDate(fromFormat: "HH:mm:ss", toFormat: "mm", dateString: i.time!)
                for j in (i.days?.components(separatedBy: ","))!{
                    if j == "1"{
                        addReminder(title: i.name!, id: i.id!, weekDay: 2, hours: Int(hours)!, mins: Int(min)!)
                    }
                    if j == "2"{
                        addReminder(title: i.name!, id: i.id!, weekDay: 3, hours: Int(hours)!, mins: Int(min)!)
                    }
                    if j == "3"{
                        addReminder(title: i.name!, id: i.id!, weekDay: 4, hours: Int(hours)!, mins: Int(min)!)
                    }
                    if j == "4"{
                        addReminder(title: i.name!, id: i.id!, weekDay: 5, hours: Int(hours)!, mins: Int(min)!)
                    }
                    if j == "5"{
                        addReminder(title: i.name!, id: i.id!, weekDay: 6, hours: Int(hours)!, mins: Int(min)!)
                    }
                    if j == "6"{
                        addReminder(title: i.name!, id: i.id!, weekDay: 7, hours: Int(hours)!, mins: Int(min)!)
                    }
                    if j == "7"{
                        addReminder(title: i.name!, id: i.id!, weekDay: 1, hours: Int(hours)!, mins: Int(min)!)
                    }
                }
            }
        }
    }
    
}
