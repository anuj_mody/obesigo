//
//  AddReminderPresener.swift
//  Obesigo
//
//  Created by Anuj  on 05/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol AddReminderDelegate : CommonDelegate {
    func onDeleteReminderSuccess(msg:String)
    func onDeleteReminderError(msg:String?,error:ErrorType?)
    func onValidationError(message:String)
}

class AddReminderPresenter {
    weak var delegate : AddReminderDelegate?
    var service : AddReminderService
    init(service:AddReminderService) {
        self.service=service
    }
    func attachView(delegate:AddReminderDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callAddEditReminderWS(reminderId:String,name:String,time:String,days:[Bool])  {
        if name.isEmpty{
            delegate?.onValidationError(message: ErrorType.REMINDER_NAME_ERROR.rawValue)
            return
        }
        var counter = 0
        for i in days{
            if i{
                counter += 1
            }
        }
        if counter == 0 {
            delegate?.onValidationError(message: ErrorType.REMINDER_DAYS_ERROR.rawValue)
            return
        }
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callAddEditReminderWS(reminderId: reminderId, name: name, time: time, days: days){
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success, errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
    func callDeleteReminderWS(id:String)  {
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callDeleteReminderWS(id: id) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onDeleteReminderError(msg: success as? String ?? "", error: error)
            }else{
                self?.delegate?.onDeleteReminderSuccess(msg: success as? String ?? "")
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
}

class AddReminderService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callAddEditReminderWS(reminderId:String,name:String,time:String,days:[Bool],result: @escaping onResponseReceived)  {
        var dayString = ""
        for (var pos,i) in days.enumerated(){
            pos += 1
            switch pos {
            case 1:
                if i{
                    dayString += "1,"
                }
                break
            case 2:
                if i{
                    dayString += "2,"
                }
                break
            case 3:
                if i{
                    dayString += "3,"
                }
                break
            case 4:
                if i{
                    dayString += "4,"
                }
                break
            case 5:
                if i{
                    dayString += "5,"
                }
                break
            case 6:
                if i{
                    dayString += "6,"
                }
                break
            case 7:
                if i{
                    dayString += "7,"
                }
                break
            default:
                break
            }
        }
        let params = ["screen_name":"Add Edit Reminder",
                      "webservice":"Add/Edit User Reminder",
                      "user_id":Preffrences().getUserId(),
                      "name":name,
                      "time":"".getFormatedDate(fromFormat: "hh:mm a", toFormat: "HH:mm", dateString: time),
                      "days":String(dayString.characters.dropLast()),
                      "id":reminderId]
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.ADD_EDIT_REMINDER, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                let r = (((success as? NSDictionary)?.object(forKey: "response") as? NSArray)?[0] as? NSDictionary)
                let remModel = RemindersModel(days: ((r)?.object(forKey: "days") as? String)!, id: ((r)?.object(forKey: "id") as? String)!, name: ((r)?.object(forKey: "name") as? String)!, time: ((r)?.object(forKey: "time") as? String)!, userId: "")
                result(remModel as AnyObject, nil)
            }
        }
        
    }
    func callDeleteReminderWS(id:String,result: @escaping onResponseReceived)  {
        let params = ["screen_name":"Add Edit Reminder",
                      "webservice":"Delete User Reminder",
                      "user_id":Preffrences().getUserId()]
        
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.ADD_EDIT_REMINDER + "/\(id)", type: .DELETE, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, nil)
            }
        }
    }
}
