//
//  HomePresenter.swift
//  Obesigo
//
//  Created by Anuj  on 04/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

protocol HomeDelegate : CommonDelegate {
    func onActivationSuccess(success:Any?)
    func onActivationError(success:Any?,error:ErrorType?)
}

class HomePresenter {
    weak var delegate : HomeDelegate?
    var service : HomeService
    init(service:HomeService) {
        self.service=service
    }
    func attachView(delegate:HomeDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callGetHomeScreenWS() {
        delegate?.showIndicator(showIt: true, showTint: false)
        service.callGetHomeScreenWS {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onError(messgae: success as? String ?? "", errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: false)
        }
    }
    func callSubmitActivationCode(code:String)  {
        delegate?.showIndicator(showIt: true, showTint: true)
        service.callSubmitActivationCode(code: code) {
            [weak self](success, error) in
            if error != nil{
                self?.delegate?.onActivationError(success: success as? String ?? "", error: error!)
            }else{
                self?.delegate?.onActivationSuccess(success: true as Any)
            }
            self?.delegate?.showIndicator(showIt: false, showTint: true)
        }
    }
}

class HomeService {
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callGetHomeScreenWS(result: @escaping onResponseReceived)  {
        let url = NetworkUrls.HOME + "?screen_name=Home&webservice=Home Screen&user_id=\(Preffrences().getUserId())"
        NetworkRequest.sharedInstance.callWebService(url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, type: .GET, params: nil) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "status") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                
                var model = HomeModel()
                var arrayHome = [HomeModel]()
                
                var dataModel = DataModel()
                var dataArray = [DataModel]()
                
                var mealModel = MealsModel()
                var mealArray = [MealsModel]()
                
                if ((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "is_purchase") as? String == "0"{
                    // show puchase cell
                    dataModel = DataModel()
                    dataModel.isPurchaseImage = true
                    dataArray.append(dataModel)
                    
                    dataModel = DataModel()
                    dataModel.isBuyNowCell = true
                    dataArray.append(dataModel)
                    
                    dataModel = DataModel()
                    dataModel.isPurchaseCell = true
                    dataArray.append(dataModel)
                    
                    model = HomeModel()
                    model.data = dataArray
                    arrayHome.append(model)
                    
                    result(arrayHome as AnyObject, nil)
                    
                }else{
                    // show meal cell
                    let data = ((((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "meal_plan") as? NSArray)?[0] as? NSDictionary)
                    for i in 1 ..< 16{
                        for j in data?.object(forKey: "day\(i)") as! NSArray{
                            model.day = i
                            model.currentDay = ((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "current_day_no") as? Int
                            //for images
                            for images in ((j as? NSDictionary)?.object(forKey: "img_url") as? NSArray)!{
                                dataModel = DataModel()
                                dataModel.isImage = true
                                dataModel.imageUrl = images as? String
                                dataArray.append(dataModel)
                            }
                            
                            //for meal header
                            dataModel = DataModel()
                            dataModel.isMealHeader = true
                            dataArray.append(dataModel)
                            
                            //for early morning
                            for value in ((j as? NSDictionary)?.object(forKey: "early_morning") as? NSArray)!{
                                mealModel = MealsModel()
                                mealModel.item_name = (value as? NSDictionary)?.object(forKey: "item_name") as? String
                                mealModel.quantity = (value as? NSDictionary)?.object(forKey: "quantity") as? String
                                mealModel.units = (value as? NSDictionary)?.object(forKey: "units") as? String
                                 mealModel.calories = (value as? NSDictionary)?.object(forKey: "calories_consume") as? String
                                mealArray.append(mealModel)
                            }
                            dataModel = DataModel()
                            dataModel.isMealData=true
                            dataModel.mealName = "Early Morning"
                            dataModel.mealData = mealArray
                            dataArray.append(dataModel)
                            mealArray.removeAll()
                            
                            //for breakfast
                            for value in ((j as? NSDictionary)?.object(forKey: "breakfast") as? NSArray)!{
                                
                                mealModel = MealsModel()
                                mealModel.item_name = (value as? NSDictionary)?.object(forKey: "item_name") as? String
                                mealModel.quantity = (value as? NSDictionary)?.object(forKey: "quantity") as? String
                                mealModel.units = (value as? NSDictionary)?.object(forKey: "units") as? String
                                mealModel.calories = (value as? NSDictionary)?.object(forKey: "calories_consume") as? String
                                mealArray.append(mealModel)
                            }
                            dataModel = DataModel()
                            dataModel.isMealData=true
                            dataModel.mealName = "Breakfast"
                            dataModel.mealData = mealArray
                            dataArray.append(dataModel)
                            mealArray.removeAll()
                            
                            //for morning_snacks
                            for value in ((j as? NSDictionary)?.object(forKey: "morning_snacks") as? NSArray)!{
                                
                                mealModel = MealsModel()
                                mealModel.item_name = (value as? NSDictionary)?.object(forKey: "item_name") as? String
                                mealModel.quantity = (value as? NSDictionary)?.object(forKey: "quantity") as? String
                                mealModel.units = (value as? NSDictionary)?.object(forKey: "units") as? String
                                mealModel.calories = (value as? NSDictionary)?.object(forKey: "calories_consume") as? String
                                mealArray.append(mealModel)
                            }
                            dataModel = DataModel()
                            dataModel.isMealData=true
                            dataModel.mealName = "Morning Snacks"
                            dataModel.mealData = mealArray
                            dataArray.append(dataModel)
                            mealArray.removeAll()
                            
                            
                            
                            //for lunch
                            for value in ((j as? NSDictionary)?.object(forKey: "lunch") as? NSArray)!{
                                
                                mealModel = MealsModel()
                                mealModel.item_name = (value as? NSDictionary)?.object(forKey: "item_name") as? String
                                mealModel.quantity = (value as? NSDictionary)?.object(forKey: "quantity") as? String
                                mealModel.units = (value as? NSDictionary)?.object(forKey: "units") as? String
                                mealModel.calories = (value as? NSDictionary)?.object(forKey: "calories_consume") as? String
                                mealArray.append(mealModel)
                            }
                            dataModel = DataModel()
                            dataModel.isMealData=true
                            dataModel.mealName = "Lunch"
                            dataModel.mealData = mealArray
                            dataArray.append(dataModel)
                            mealArray.removeAll()
                            
                            //for evening_snacks
                            for value in ((j as? NSDictionary)?.object(forKey: "evening_snacks") as? NSArray)!{
                                
                                mealModel = MealsModel()
                                mealModel.item_name = (value as? NSDictionary)?.object(forKey: "item_name") as? String
                                mealModel.quantity = (value as? NSDictionary)?.object(forKey: "quantity") as? String
                                mealModel.units = (value as? NSDictionary)?.object(forKey: "units") as? String
                                mealModel.calories = (value as? NSDictionary)?.object(forKey: "calories_consume") as? String
                                mealArray.append(mealModel)
                            }
                            dataModel = DataModel()
                            dataModel.isMealData=true
                            dataModel.mealName = "Evening Snacks"
                            dataModel.mealData = mealArray
                            dataArray.append(dataModel)
                            mealArray.removeAll()
                            
                            //for dinner
                            for value in ((j as? NSDictionary)?.object(forKey: "dinner") as? NSArray)!{
                                
                                mealModel = MealsModel()
                                mealModel.item_name = (value as? NSDictionary)?.object(forKey: "item_name") as? String
                                mealModel.quantity = (value as? NSDictionary)?.object(forKey: "quantity") as? String
                                mealModel.units = (value as? NSDictionary)?.object(forKey: "units") as? String
                                mealModel.calories = (value as? NSDictionary)?.object(forKey: "calories_consume") as? String
                                mealArray.append(mealModel)
                            }
                            
                            dataModel = DataModel()
                            dataModel.isMealData=true
                            dataModel.mealName = "Dinner"
                            dataModel.mealData = mealArray
                            dataArray.append(dataModel)
                            mealArray.removeAll()
                            
                            //for exercise header
                            dataModel = DataModel()
                            dataModel.isExerciseHeader = true
                            dataArray.append(dataModel)
                            
                            for exercise in ((((success as? NSDictionary)?.object(forKey: "response") as? NSDictionary)?.object(forKey: "exercise") as? NSArray))!{
                                dataModel = DataModel()
                                dataModel.isExerciseData = true
                                dataModel.exerciseName = (exercise as? NSDictionary)?.object(forKey: "exercise_name") as? String
                                dataModel.exerciseTime = (exercise as? NSDictionary)?.object(forKey: "duration_count") as? String
                                dataModel.exerciseUnit = (exercise as? NSDictionary)?.object(forKey: "units") as? String

                                dataArray.append(dataModel)
                            }
                        }
                        
                        model.data = dataArray
                        arrayHome.append(model)
                        dataArray.removeAll()
                    }
                    
                }
                result(arrayHome as AnyObject, nil)
            }
        }
    }
    func callSubmitActivationCode(code:String,result: @escaping onResponseReceived) {
        let params = ["screen_name":"Home Screen",
                      "webservice":"Submit Activation Code",
                      "user_id":Preffrences().getUserId(),
                      "activation_code":code]
        NetworkRequest.sharedInstance.callWebService(url: NetworkUrls.SUBMIT_ACTIVATIONCODE, type: .POST, params: params) {
            (success, error) in
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.WRONG_DATA)
            }else{
                result(true as AnyObject, nil)
            }
        }
    }
}
