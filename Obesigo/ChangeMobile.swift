//
//  ChangeMobile.swift
//  Obesigo
//
//  Created by Anuj  on 12/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class ChangeMobile: BaseVC {
    //MARK:- outlets
    @IBOutlet var tfMobile: UITextField!
    @IBOutlet var lblMobileError: UILabel!
    
    //MARK:- variables
    var lblMobilePH : UILabel?
    var presenter = OtpPresenter(service: OtpService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Functions
    func setUp() {
        presenter.attachView(delegate: self)
        self.addStatusBarBG()
        self.addInputAccessoryForTextFields(textFields: [tfMobile])
        lblMobilePH = tfMobile.addPlaceHolder(mainView: self.view, placeHolderText: "Mobile Number (eg : 8888888888)")
        tfMobile.delegate=self
    }
    
    //MARK:- Button Actions
    @IBAction func btSubmit(_ sender: Any) {
        presenter.callOTPWS(value: tfMobile.text!, type: 1)
    }
    @IBAction func btBack(_ sender: Any) {
        goBack()
    }
}

extension ChangeMobile : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lblMobileError.isHidden=true
        lblMobilePH?.animatePlaceHolder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        presenter.checkOTP(value: textField.text!)
        if (textField.text?.isEmpty)!{
            lblMobilePH?.animatePlaceHolder(animateUp: false)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfMobile{
            let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
            if newLength > 10{
                return false
            }
            return true
        }
        return true
    }
}

extension ChangeMobile : OtpDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        self.goBack()
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.MOBILE_EDITED), object: self, userInfo: ["data":successValue as? String ?? ""])
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        self.showAlert(title: "Error!", message: messgae as? String ?? "")
    }
    func showOtpError(message: String) {
        lblMobileError.isHidden=false
        lblMobileError.text = ErrorType.EMPTY_MOBILE.rawValue
    }
}
