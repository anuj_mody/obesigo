//
//  AddCalorie.swift
//  Obesigo
//
//  Created by Anuj  on 26/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

class AddCalorie: BaseVC {
    //MARK:-outlets
    @IBOutlet var tvCalories: UITableView!
    @IBOutlet var lblHeader: UILabel!
    
    //MARK:-variables
    let searchController = UISearchController(searchResultsController: nil)
    //for exercise
    var arrayExercise : [ExerciseModel]?
    var arrayCopy : [ExerciseModel]?
    var filteredArray : [ExerciseModel]?
    
    //for food
    var arrayFoodItems : [FoodItemModel]?
    var arrayFoodItemsCopy : [FoodItemModel]?
    var filteredFoodItemArray : [FoodItemModel]?
    
    var fromFood = false
    var fromDone = false
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        // self.navigationController?.navigationBar.isHidden=true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    //MARK:- Functions
    func setUp() {
        self.addStatusBarBG()
        if fromFood{
            lblHeader.text = "Add Calories Consumed"
        }else{
            lblHeader.text = "Add Calories Burnt"
        }
        searchController.searchBar.barTintColor = Colors.themeColor
        searchController.navigationController?.setNavigationBarHidden(true, animated: true)
        searchController.isActive=false
        searchController.searchResultsUpdater=self
        UISearchBar.appearance().tintColor = UIColor.white
        searchController.searchBar.clipsToBounds=true
        searchController.dimsBackgroundDuringPresentation=false
        definesPresentationContext=true
        if #available(iOS 9.0, *) {
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = Colors.themeColor
        } else {
            // Fallback on earlier versions
        }
        tvCalories.tableHeaderView = searchController.searchBar
        tvCalories.register(UINib(nibName: String(describing: CalBurntConsumedCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CalBurntConsumedCell.self))
        tvCalories.delegate=self
        tvCalories.dataSource=self
        tvCalories.reloadData()
    }
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        if searchText == ""{
            if fromFood{
                arrayFoodItems = arrayFoodItemsCopy
            }else{
                arrayExercise = arrayCopy
            }
            
        }else{
            if fromFood{
                filteredFoodItemArray = [FoodItemModel]()
                filteredFoodItemArray = (arrayFoodItemsCopy!.filter({( candy : FoodItemModel) -> Bool in
                    return (candy.item_name?.lowercased().contains(searchText.lowercased()))!
                }))
                arrayFoodItems?.removeAll()
                arrayFoodItems = filteredFoodItemArray
            }else{
                filteredArray = [ExerciseModel]()
                filteredArray = (arrayCopy!.filter({( candy : ExerciseModel) -> Bool in
                    return (candy.exercise_name?.lowercased().contains(searchText.lowercased()))!
                }))
                arrayExercise?.removeAll()
                arrayExercise = filteredArray
            }
            
        }
        tvCalories.reloadData()
    }
    
    //MARK:- Button Actions
    @IBAction func btDone(_ sender: Any) {
        fromDone=true
        if fromFood{
            arrayFoodItems = arrayFoodItems?.filter{
                $0.isSelected
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.FOOD_ARRAY), object: self, userInfo: ["data":arrayFoodItems as Any])
            self.goBack()
        }else{
            arrayExercise = arrayExercise?.filter{
                $0.isSelected
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.EXERCISE_ARRAY), object: self, userInfo: ["data":arrayExercise as Any])
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.searchController.isActive=false
            })
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.goBack()
            })
        }
    }
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
}
//MARK:- Tableview Functions
extension AddCalorie : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if fromFood{
            return arrayFoodItems?.count ?? 0
        }else{
            return arrayExercise?.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CalBurntConsumedCell.self), for: indexPath) as? CalBurntConsumedCell
        cell?.hideOthers = true
        if fromFood{
            cell?.dataFoodItems = arrayFoodItems?[indexPath.row]
        }else{
            cell?.dataExercise = arrayExercise?[indexPath.row]
        }
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 57
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if fromFood{
            if (arrayFoodItems?[indexPath.row].isSelected)!{
                arrayFoodItems?[indexPath.row].isSelected=false
                arrayFoodItemsCopy?[(arrayFoodItems?[indexPath.row].index!)!].isSelected=false
            }else{
                arrayFoodItems?[indexPath.row].isSelected=true
                arrayFoodItemsCopy?[(arrayFoodItems?[indexPath.row].index!)!].isSelected=true
            }
        }else{
            if (arrayExercise?[indexPath.row].isSelected)!{
                arrayExercise?[indexPath.row].isSelected=false
                arrayCopy?[(arrayExercise?[indexPath.row].index!)!].isSelected=false
            }else{
                arrayExercise?[indexPath.row].isSelected=true
                arrayCopy?[(arrayExercise?[indexPath.row].index!)!].isSelected=true
            }
        }
        tvCalories.reloadRows(at: [indexPath], with: .automatic)
    }
}
extension AddCalorie : UISearchResultsUpdating,UISearchBarDelegate{
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if !fromDone{
            if fromFood{
                arrayFoodItems = arrayFoodItemsCopy
            }else{
                arrayExercise = arrayCopy
            }
            tvCalories.reloadData()
        }
       
    }
}
