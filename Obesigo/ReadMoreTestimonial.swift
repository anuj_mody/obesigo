//
//  ReadMoreTestimonial.swift
//  Obesigo
//
//  Created by Anuj  on 12/09/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

class ReadMoreTestimonial: BaseVC {
    //MARK:- outlets
    @IBOutlet var ivPP: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var btStar1: UIButton!
    @IBOutlet var btStar2: UIButton!
    @IBOutlet var btStar3: UIButton!
    @IBOutlet var btStar4: UIButton!
    @IBOutlet var btStar5: UIButton!
    
    //MARK:- variables
    var name:String?
    var date:String?
    var msg:String?
    var imageUrl:String?
    var starRating: String?
    let starSelected = #imageLiteral(resourceName: "ic_star_small")
    let starEmpty = #imageLiteral(resourceName: "ic_star_empty_small")
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Button Actions
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
    
    
    //MARK:- Functions
    func setUp() {
        self.addStatusBarBG()
        ivPP.layer.cornerRadius = ivPP.frame.width/2
        lblName.text = name ?? ""
        lblDate.text = date ?? ""
        lblMsg.text = msg ?? ""
        showRating(rating: Int(starRating ?? "1")!)
        if let imageUrl = URL(string: (imageUrl ?? "")){
            ivPP.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "im_default_profile_testimonails"), options: nil, progressBlock: nil, completionHandler: nil)
        }else{
            ivPP.image = #imageLiteral(resourceName: "im_default_profile_testimonails")
        }
    }
    func showRating(rating:Int)  {
        
        switch rating {
        case 1:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starEmpty, for: .normal)
            btStar3.setImage(starEmpty, for: .normal)
            btStar4.setImage(starEmpty, for: .normal)
            btStar5.setImage(starEmpty, for: .normal)
            break
            
        case 2:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starSelected, for: .normal)
            btStar3.setImage(starEmpty, for: .normal)
            btStar4.setImage(starEmpty, for: .normal)
            btStar5.setImage(starEmpty, for: .normal)
            break
        case 3:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starSelected, for: .normal)
            btStar3.setImage(starSelected, for: .normal)
            btStar4.setImage(starEmpty, for: .normal)
            btStar5.setImage(starEmpty, for: .normal)
            break
        case 4:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starSelected, for: .normal)
            btStar3.setImage(starSelected, for: .normal)
            btStar4.setImage(starSelected, for: .normal)
            btStar5.setImage(starEmpty, for: .normal)
            break
        case 5:
            btStar1.setImage(starSelected, for: .normal)
            btStar2.setImage(starSelected, for: .normal)
            btStar3.setImage(starSelected, for: .normal)
            btStar4.setImage(starSelected, for: .normal)
            btStar5.setImage(starSelected, for: .normal)
            break
        default:
            break
        }
    }
}
