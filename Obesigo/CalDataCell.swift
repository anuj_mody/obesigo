//
//  CalDataCell.swift
//  Obesigo
//
//  Created by Anuj  on 23/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class CalDataCell: UITableViewCell {
    
    //MARK:-outlets
    @IBOutlet var lblCalConsumed: UILabel!
    @IBOutlet var lblCalBurnt: UILabel!
    @IBOutlet var lblDate: UILabel!
    
    var data : [String]?{
        didSet{
            lblCalConsumed.text = data?[0]
            lblCalBurnt.text = data?[1]
            lblDate.text = "".getFormatedDate(fromFormat: "dd MMMM, yyyy", toFormat: "dd MMM, yyyy", dateString: (data?[2])!)
        }
    }
    
}
