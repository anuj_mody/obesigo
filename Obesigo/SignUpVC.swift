//
//  SignUpVC.swift
//  Obesigo
//
//  Created by Anuj  on 12/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import UIKit

class SignUpVC: BaseVC {

    //outlets
    @IBOutlet var tfFName: UITextField!
    @IBOutlet var tfLName: UITextField!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var tfMobile: UITextField!
    @IBOutlet var tfDOB: UITextField!
    @IBOutlet var tfGender: UITextField!
    @IBOutlet var tfActivationCode: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var lblFNameError: UILabel!
    @IBOutlet var lblLNameError: UILabel!
    @IBOutlet var lblEmailError: UILabel!
    @IBOutlet var lblPasswordError: UILabel!
    @IBOutlet var lblMobileError: UILabel!
    @IBOutlet var lblDOBError: UILabel!
    @IBOutlet var lblGenderError: UILabel!
    
    
    //variables
    var lblFNamePH: UILabel?
    var lblLNamePH: UILabel?
    var lblEmailPH: UILabel?
    var lblPasswordPH: UILabel?
    var lblMobilePH: UILabel?
    var lblDOBPH: UILabel?
    var lblGenderPH: UILabel?
    var lblActivationCodePH: UILabel?
    var activePH: UILabel?
    var lblActiveError:UILabel?
    var datePicker: UIDatePicker?
    var toolbar: UIToolbar?
    var actualInset : UIEdgeInsets?
    var presenter : SignUpPresenter?
    var isDOBAdded = false
    
    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    // MARK: - Functions
    func setUp()  {
        presenter = SignUpPresenter(service: SignUpService())
        presenter?.attachView(delegate: self)
        self.hideKeyboardWhenTappedAround()
        self.addStatusBarBG()
        actualInset = scrollView.contentInset
        self.addInputAccessoryForTextFields(textFields: [tfFName,tfLName,tfEmail,tfPassword,tfMobile,tfActivationCode])
        lblFNamePH=tfFName.addPlaceHolder(mainView: scrollView, placeHolderText: "First Name")
        lblLNamePH=tfLName.addPlaceHolder(mainView: scrollView, placeHolderText: "Last Name")
        lblEmailPH=tfEmail.addPlaceHolder(mainView: scrollView, placeHolderText: "Email")
        lblPasswordPH=tfPassword.addPlaceHolder(mainView: scrollView, placeHolderText: "Password")
        lblMobilePH=tfMobile.addPlaceHolder(mainView: scrollView, placeHolderText: "Mobile (10 digits)")
        lblDOBPH=tfDOB.addPlaceHolder(mainView: scrollView, placeHolderText: "D.O.B")
        lblGenderPH=tfGender.addPlaceHolder(mainView: scrollView, placeHolderText: "Gender")
        lblActivationCodePH=tfActivationCode.addPlaceHolder(mainView: scrollView, placeHolderText: "Activation Code (Optional)")
        tfFName.delegate=self
        tfLName.delegate=self
        tfEmail.delegate=self
        tfPassword.delegate=self
        tfMobile.delegate=self
        tfDOB.delegate=self
        tfGender.delegate=self
        tfActivationCode.delegate=self
    }
    func openGenderDialog() {
        self.view.endEditing(true)
        self.showActionSheet(data: ["Male" , "Female"]) {
            [weak self](value) -> () in
            self?.tfGender.text = value
            self?.lblGenderPH?.animatePlaceHolder()
        }
    }
    func openDatePicker() {
        //hidePickers()
        self.view.endEditing(true)
        isDOBAdded = true
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200))
        datePicker?.backgroundColor = UIColor.white
        datePicker?.datePickerMode = .date
        datePicker?.date = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        datePicker?.maximumDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.maxY+240, width: self.view.frame.width, height: 200))
        toolbar?.sizeToFit()
        toolbar?.isTranslucent=false
        toolbar?.barTintColor = Colors.themeColor
        toolbar?.tintColor = UIColor.white
        let doneButton = UIBarButtonItem(title: "done", style: .done, target: self, action:#selector(SignUpVC.getDate))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action:#selector(SignUpVC.hidePickers))
        toolbar?.setItems([cancelButton, spacer, doneButton], animated: true)
        self.view.addSubview(self.datePicker!)
        self.view.addSubview(self.toolbar!)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY-200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY-235, width: self.view.frame.width, height: 35)
        }, completion: nil)
        
    }
    func getDate()  {
        hidePickers()
        
        tfDOB.text = datePicker?.date.getFormattedDate(formatterString: "dd-MM-yyyy")
        lblDOBPH?.animatePlaceHolder()
    }
    func hidePickers()  {
        isDOBAdded=false
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY+235, width: self.view.frame.width, height: 35)
        }){
            
            finished in
            self.toolbar?.removeFromSuperview()
            self.datePicker?.removeFromSuperview()
        }
        
        
    }
    func showHideError(label:UILabel,msg:String,show:Bool)  {
        if show{
            label.isHidden=false
            label.text = msg
        }else{
            label.isHidden = true
        }
    }
    
    // MARK: - Button Actions
    @IBAction func btSignUp(_ sender: Any) {
        
        presenter?.callSignUpWS(url: NetworkUrls.REGISTER, fName: tfFName.text!, lName: tfLName.text!, email: tfEmail.text!, password: tfPassword.text!, mobile: tfMobile.text!, dob: tfDOB.text!, gender: tfGender.text!, activationCode: tfActivationCode.text!)
    }
    @IBAction func btBack(_ sender: Any) {
        self.goBack()
    }
    
}

extension SignUpVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        KeybordAccessories.addKeyboardNotification(scrollView: scrollView, view: self.view, activeField: textField, activeTextView: nil, actualContentInset: actualInset!)
        if textField == tfFName{
            activePH = lblFNamePH
            lblActiveError = lblFNameError
        }else if textField == tfLName{
            activePH = lblLNamePH
            lblActiveError = lblLNameError
        }else if textField == tfEmail{
            activePH = lblEmailPH
            lblActiveError = lblEmailError
        }else if textField == tfPassword{
            activePH = lblPasswordPH
            lblActiveError = lblPasswordError
        }else if textField == tfMobile{
            activePH = lblMobilePH
            lblActiveError = lblMobileError
        }else if textField == tfActivationCode{
            activePH = lblActivationCodePH
        }else{
            activePH=nil
        }
        activePH?.animatePlaceHolder()
        lblActiveError?.isHidden=true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfFName{
            activePH = lblFNamePH
            presenter?.checkFName(fName: textField.text!)
        }else if textField == tfLName{
            activePH = lblLNamePH
            presenter?.checkLName(lName: textField.text!)
        }else if textField == tfEmail{
            activePH = lblEmailPH
            presenter?.checkEmail(email: textField.text!)
        }else if textField == tfPassword{
            activePH = lblPasswordPH
            presenter?.checkPassword(password: textField.text!)
        }else if textField == tfMobile{
            activePH = lblMobilePH
            presenter?.checkMobile(mobile: textField.text!)
        }else if textField == tfActivationCode{
            activePH = lblActivationCodePH
            
        }
        if (textField.text?.isEmpty)!{
            activePH?.animatePlaceHolder(animateUp: false)
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfGender{
            openGenderDialog()
            return false
        }
        
        if textField == tfDOB{
            if !isDOBAdded{
                openDatePicker()
            }
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfMobile{
            let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
            if newLength > 10{
                return false
            }
            return true
        }
        return true
    }
}

extension SignUpVC : SignUpDelegate{
    func showIndicator(showIt: Bool,showTint:Bool) {
        if showIt{
            IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: "OtpSB", identifier: "OtpVC") as? OtpVC
        self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "OtpSB", identifier: "OtpVC"))
        //self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "HomeMain", identifier: "SWRevealViewController") as! SWRevealViewController)
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        self.showAlert(title: "Error!", message: (messgae as? String) ?? "")
    }
    func showDOBError(message: String) {
        showHideError(label: lblDOBError, msg: message, show: true)
    }
    func showGenderError(message: String) {
        showHideError(label: lblGenderError, msg: message, show: true)
    }
    func showPwdError(message: String) {
        showHideError(label: lblPasswordError, msg: message, show: true)
    }
    func showEmailError(message: String) {
        showHideError(label: lblEmailError, msg: message, show: true)
    }
    func showFNameError(message: String) {
        showHideError(label: lblFNameError, msg: message, show: true)
    }
    func showLNameError(message: String) {
        showHideError(label: lblLNameError, msg: message, show: true)
    }
    func showMobileError(message: String) {
        showHideError(label: lblMobileError, msg: message, show: true)
    }
}
