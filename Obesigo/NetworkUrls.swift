//
//  NetworkUrls.swift
//  Obesigo
//
//  Created by Anuj  on 24/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation

struct NetworkUrls {
    //    static let BASE_URL  = "http://techmorphosis-development.com/obesigo/public/api/"
    static let BASE_URL  = "http://obesigo.com/obesigo/public/api/"
    static let LOGIN = BASE_URL + "app_authenticate"
    static let TESTIMONIALS = BASE_URL + "testimonial"
    static let USER_STAT = BASE_URL + "user_stat"
    static let REGISTER = BASE_URL + "register"
    static let RESET_PASSWORD = BASE_URL + "reset_password"
    static let BMI_DIETARYPREF = BASE_URL + "diet_preference"
    static let ADD_EDIT_REMINDER = BASE_URL + "user_reminder"
    static let PROFILE = BASE_URL + "user"
    static let CHANGE_PASSWORD = BASE_URL + "change_password"
    static let EDIT_PROFILE = BASE_URL + "edit_profile"
    static let OTP = BASE_URL + "mobile_otp"
    static let CALORIES_BURNT = BASE_URL + "calories_burnt"
    static let CALORIES_CONSUMED = BASE_URL + "calories_consume"
    static let HOME = BASE_URL + "home"
    static let SUBMIT_ACTIVATIONCODE = BASE_URL + "submit_activationcode"
    //submit_activationcode
}
