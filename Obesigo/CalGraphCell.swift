//
//  CalGraphCell.swift
//  Obesigo
//
//  Created by Anuj  on 23/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
import Charts

class CalGraphCell: UITableViewCell {
    //MARK:-outlets
    @IBOutlet var chartView: LineChartView!
    
    //MARK:-variables
    @IBOutlet var lblCalories: UILabel!
    
    override func awakeFromNib() {
        lblCalories.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
    }
    var data:[Logs]?{
        didSet{
            var months=[String]()
            var calConsumed = [String]()
            var calBurnt = [String]()
            for i in (data?.reversed())!{
                let date = (i.log_date?.components(separatedBy: ",")[0])
                
                months.append("".getFormatedDate(fromFormat: "dd MMMM", toFormat: "dd MMM", dateString: date!))
                calConsumed.append(i.cal_consumed!)
                calBurnt.append(i.cal_burnt!)
            }
            chartView.data =  setCharts(dataPoints: months,calConsumed: calConsumed,calBurnt:calBurnt)
            chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: months);
            chartView.xAxis.labelPosition = .bottom
            chartView.xAxis.granularity = 1;
            chartView.animate(xAxisDuration: 1.0)
            chartView.rightAxis.labelTextColor = UIColor.clear
            chartView.tintColor = Colors.themeColor
            chartView.legend.enabled = false
            chartView.chartDescription?.text = ""
            chartView.xAxis.labelHeight = 20
            chartView.xAxis.labelFont = UIFont.systemFont(ofSize: 10)
            //chartView.legend.font = UIFont(name: "Verdana", size: 16.0)!
        }
    }
    
    //MARK:- Functions
    func setCharts(dataPoints: [String],calConsumed:[String],calBurnt:[String]) -> LineChartData {
        let chartData : LineChartData?
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: Double(calConsumed[i])!)
            dataEntries.append(dataEntry)
        }
        let lineChatDataSet = LineChartDataSet(values: dataEntries, label: nil)
        lineChatDataSet.circleColors = [Colors.themeColor];
        lineChatDataSet.colors = [Colors.themeColor];
        lineChatDataSet.circleRadius = 3.0;
        //lineChatDataSet.valueFont=UIFont(name: "Verdana", size: 12.0)!
        
        var dataEntries1: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: Double(calBurnt[i])!)
            dataEntries1.append(dataEntry)
        }
        let lineChatDataSet1 = LineChartDataSet(values: dataEntries1, label: nil)
        lineChatDataSet1.circleColors = [Colors.chartBlue];
        lineChatDataSet1.colors = [Colors.chartBlue];
        lineChatDataSet1.circleRadius = 3.0;

        chartData = LineChartData(dataSets: [lineChatDataSet,lineChatDataSet1])
        return chartData!
    }
    
}
