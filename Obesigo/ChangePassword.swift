//
//  ChangePassword.swift
//  Obesigo
//
//  Created by Anuj  on 09/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class ChangePassword: BaseVC {
    
    //MARK:- outlets
    @IBOutlet var tfCurrentPwd: UITextField!
    @IBOutlet var tfNewPwd: UITextField!
    @IBOutlet var tfConfirmPwd: UITextField!
    @IBOutlet var lblCurrentPwdError: UILabel!
    @IBOutlet var lblNewPwdError: UILabel!
    @IBOutlet var lblConfirmPwdError: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    
    //MARK:- variables
    var lblCurrentPwdPH : UILabel?
    var lblNewPwdPH : UILabel?
    var lblConfirmPwdPH : UILabel?
    var actualInset : UIEdgeInsets?
    var lblActive:UILabel?
    var presenter = ChangePasswordPresenter(service: ChangePasswordService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Functions
    func setUp()  {
        presenter.attachView(delegate: self)
        lblCurrentPwdPH = tfCurrentPwd.addPlaceHolder(mainView: self.scrollView, placeHolderText: "Current Password")
        lblNewPwdPH = tfNewPwd.addPlaceHolder(mainView: self.scrollView, placeHolderText: "New Password")
        lblConfirmPwdPH = tfConfirmPwd.addPlaceHolder(mainView: self.scrollView, placeHolderText: "Confirm Password")
        self.hideKeyboardWhenTappedAround()
        self.addInputAccessoryForTextFields(textFields: [tfCurrentPwd,tfNewPwd,tfConfirmPwd])
        actualInset = scrollView.contentInset
        tfCurrentPwd.delegate=self
        tfNewPwd.delegate=self
        tfConfirmPwd.delegate=self
    }
    
    //MARK:- Button Actions
    @IBAction func btSave(_ sender: Any) {
        presenter.callChangePasswordWS(currentPwd: tfCurrentPwd.text!, newPwd: tfNewPwd.text!, confirmPwd: tfConfirmPwd.text!)
    }
    
}

extension ChangePassword : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        KeybordAccessories.addKeyboardNotification(scrollView: scrollView, view: self.view, activeField: textField, activeTextView: nil, actualContentInset: actualInset!)
        if textField == tfCurrentPwd{
            lblActive = lblCurrentPwdPH
            lblCurrentPwdError.isHidden=true
        }else if textField == tfNewPwd{
            lblActive = lblNewPwdPH
            lblNewPwdError.isHidden=true
        }else{
            lblActive = lblConfirmPwdPH
            lblConfirmPwdError.isHidden=true
        }
        lblActive?.animatePlaceHolder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfCurrentPwd{
            lblActive = lblCurrentPwdPH
            presenter.checkCurrentPwd(value: textField.text!)
        }else if textField == tfNewPwd{
            lblActive = lblNewPwdPH
            presenter.checkNewPwd(value: textField.text!)
        }else{
            lblActive = lblConfirmPwdPH
            presenter.checkConfirmPwd(newPwd: tfNewPwd.text!, confirmPwd: textField.text!)
        }
        
        if (textField.text?.isEmpty)!{
            lblActive?.animatePlaceHolder(animateUp: false)
        }
        
    }
}

extension ChangePassword : ChangePasswordDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        self.showAlert(title: "", message: successValue as? String ?? "")
        Preffrences().setPassword(value: tfNewPwd.text!)
        tfNewPwd.text = ""
        tfConfirmPwd.text = ""
        tfConfirmPwd.text = ""
        lblCurrentPwdPH?.animatePlaceHolder(animateUp: false)
        lblNewPwdPH?.animatePlaceHolder(animateUp: false)
        lblConfirmPwdPH?.animatePlaceHolder(animateUp: false)
        
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        self.showAlert(title: "", message: messgae as? String ?? "")
    }
    func showNewPwdError(message: String) {
        lblNewPwdError.isHidden=false
        lblNewPwdError.text = message
    }
    func showConfirmPwdError(message: String) {
        lblConfirmPwdError.isHidden=false
        lblConfirmPwdError.text = message
    }
    func showCurrentPwdError(message: String) {
        lblCurrentPwdError.isHidden=false
        lblCurrentPwdError.text = message
    }
}
