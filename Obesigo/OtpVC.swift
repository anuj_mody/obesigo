//
//  OtpVC.swift
//  Obesigo
//
//  Created by Anuj  on 12/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class OtpVC: BaseVC {
    //MARK:- outlets
    @IBOutlet var tfOtp: UITextField!
    @IBOutlet var lblOtpError: UILabel!
    @IBOutlet var btBack: UIButton!
    @IBOutlet var btChangeNumber: UIButton!
    
    //MARK:- variables
    var lblOtpPH : UILabel?
    var otp:String?
    var showBack = true
    var fromResendOTP = false
    var presenter = OtpPresenter(service: OtpService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.MOBILE_EDITED), object: nil)
    }
    
    //MARK:- Functions
    func setUp() {
        presenter.attachView(delegate: self)
        if !showBack{
            btBack.isHidden=true
        }else{
            btBack.isHidden=false
        }
        self.hideKeyboardWhenTappedAround()
        self.addStatusBarBG()
        btChangeNumber.layer.borderWidth = 1
        btChangeNumber.layer.borderColor = Colors.themeColor.cgColor
        lblOtpPH = tfOtp.addPlaceHolder(mainView: self.view, placeHolderText: "OTP")
        tfOtp.delegate=self
        tfOtp.text = Constants.OTP == "otp" ? "" : Constants.OTP
        lblOtpPH?.animatePlaceHolder()
        self.addInputAccessoryForTextFields(textFields: [tfOtp])
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constants.MOBILE_EDITED), object: nil, queue: nil){
            [weak self] (noti) in
            self?.showToast(msg: (noti.userInfo as NSDictionary?)?.object(forKey: "data") as! String)
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btSubmit(_ sender: Any) {
        presenter.callOTPWS(value: tfOtp.text!, type: 0)
    }
    @IBAction func btResendOtp(_ sender: Any) {
        fromResendOTP = true
        presenter.callOTPWS(value: "1", type: 2)
    }
    @IBAction func btChageNumber(_ sender: Any) {
        self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "ChangeNumber", identifier: String(describing: ChangeMobile.self)))
    }
    @IBAction func btBack(_ sender: Any) {
        if self.presentingViewController?.presentingViewController == nil{
self.goBack()
        }else{
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
        //self.goBack()
    }
    
}

extension OtpVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lblOtpError.isHidden=true
        lblOtpPH?.animatePlaceHolder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        presenter.checkOTP(value: textField.text!)
        if (textField.text?.isEmpty)!{
            lblOtpPH?.animatePlaceHolder(animateUp: false)
        }
    }
}

extension OtpVC : OtpDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        if fromResendOTP{
            fromResendOTP = false
            self.showAlert(title: "", message: successValue as? String ?? "")
        }else{
            Preffrences().setOTPEntered(value: true)
            self.switchViewController(viewController: self.getViewControllerFromStoryBoard(storyBoardName: "BMICalculatorSB", identifier: "BMICalculatorVC"))
        }
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        self.showAlert(title: "", message: messgae as? String ?? "")
    }
    func showOtpError(message: String) {
        lblOtpError.isHidden=false
        lblOtpError.text = message
    }
}

