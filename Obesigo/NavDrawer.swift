//
//  NavDrawer.swift
//  Obesigo
//
//  Created by Anuj  on 28/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//
import UIKit
import Kingfisher

class NavDrawer : BaseVC{
    //MARK:- outlets
    @IBOutlet var tvNav: UITableView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var ivProfilePic: UIImageView!
    
    //MARK:- variables
    lazy var arrNav : [NavModel] =  {
        var array = [NavModel]()
        var model = NavModel()
        model.header = "Home"
        model.icon = "ic_home_nav"
//        model.isSelected=true
        array.append(model)
        
        model = NavModel()
        model.header = "Reminder"
        model.icon = "ic_reminder_nav"
        array.append(model)
        
        model = NavModel()
        model.header = "Testimonials"
        model.icon = "ic_testimonials_nav"
        array.append(model)
        
        model = NavModel()
        model.header = "Share App"
        model.icon = "ic_share_app_nav"
        array.append(model)
        
        model = NavModel()
        model.header = "Change Password"
        model.icon = "ic_change_password_nav"
        array.append(model)
        
        model = NavModel()
        model.header = "Send Enquiry"
        model.icon = "ic_send_inquiry_nav"
        array.append(model)
        
        model = NavModel()
        model.header = "Logout"
        model.icon = "ic_logout_nav"
        array.append(model)
        
        return array
    }()
    let NAV_CELL = "NavCell"
    var previousPosition : Int?
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewDidAppear(_ animated: Bool) {
        if let imageUrl = URL(string: Preffrences().getProfilePic()){
            ivProfilePic.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "im_default_profile_nav"), options: nil, progressBlock: nil, completionHandler: nil)
        }else{
            ivProfilePic.image = #imageLiteral(resourceName: "im_default_profile_nav")
        }
        
        lblName.text = Preffrences().getName()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.addStatusBarBG()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Functions
    func setUp() {
        tvNav.register(UINib(nibName: NAV_CELL, bundle: nil), forCellReuseIdentifier: NAV_CELL)
        lblName.text = Preffrences().getName()
        ivProfilePic.layer.cornerRadius = ivProfilePic.frame.width/2
        ivProfilePic.layer.masksToBounds=true
        tvNav.estimatedRowHeight = 100
        tvNav.rowHeight = UITableViewAutomaticDimension
        tvNav.delegate=self
        tvNav.dataSource=self
        tvNav.reloadData()
    }
    
    //MARK:- Button Actions
    @IBAction func btProfile(_ sender: Any) {
        let vc = (self.revealViewController().frontViewController as! UINavigationController).viewControllers[0] as? HomeVC
        vc?.openEditProfile()
        self.revealViewController().revealToggle(animated: true)
    }
}

//MARK:- Tableview Functions
extension NavDrawer : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNav.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NAV_CELL, for: indexPath) as? NavCell
        cell?.lblHeader.text=arrNav[indexPath.row].header
        cell?.ivIcon.image = UIImage(named: arrNav[indexPath.row].icon!)?.withRenderingMode(.alwaysTemplate)
        cell?.data = arrNav[indexPath.row]
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if previousPosition != nil {
            arrNav[previousPosition!].isSelected = false
            self.tvNav.reloadRows(at: [IndexPath(row: previousPosition!, section: 0)], with: .automatic)
        }
        arrNav[indexPath.row].isSelected = true
        self.tvNav.reloadRows(at: [indexPath], with: .automatic)
        previousPosition = indexPath.row
        let vc = (self.revealViewController().frontViewController as! UINavigationController).viewControllers[0] as? HomeVC
        vc?.changeViewController(which: indexPath.row)
        self.revealViewController().revealToggle(animated: true)

    }
}
