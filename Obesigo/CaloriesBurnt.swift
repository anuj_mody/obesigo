//
//  CaloriesBurnt.swift
//  Obesigo
//
//  Created by Anuj  on 24/08/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
class CaloriesBurnt : BaseVC{
    //MARK:- outlets
    @IBOutlet var tvCal: UITableView!
    @IBOutlet var lblCalBurnt: UILabel!
    @IBOutlet var coverView: UIView!
    @IBOutlet var btDate: UIButton!
    @IBOutlet var btSave: UIButton!
    
    //MARK:- constraints
    @IBOutlet var tvCalHeightConst: NSLayoutConstraint!
    
    //MARK:- variables
    var isPickerOpen=false
    var datePicker: UIDatePicker?
    var toolbar: UIToolbar?
    var dateFormatter = DateFormatter()
    var arrayCalories : [CaloriesBurntModel]?
    var arrayCalBurnt = [CalBurntModel](){
        didSet{
            tvCal.delegate=self
            tvCal.dataSource=self
            tvCal.reloadData()
            //            if arrayCalBurnt.count > 0{
            //                btSave.isEnabled=true
            //                btSave.backgroundColor = Colors.themeColor
            //            }
            
            lblCalBurnt.text = "\((arrayCalories?[0].calories_sum ?? "" == "" ? "0" : arrayCalories?[0].calories_sum)!) Cal."
            tvCalHeightConst.constant = tvCal.contentSize.height
            coverView.isHidden=true
        }
    }
    var arrayCounts = [String]()
    var position:Int?
    var originalAllExercise : [ExerciseModel]?
    var pickerView : UIPickerView?
    var presenter = CaloriesBurntPresenter(service: CaloriesBurntService())
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        //        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //        self.navigationController?.navigationBar.isHidden=true
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Setup
    func setUp() {
        presenter.attachView(delegate: self)
        self.addStatusBarBG()
        tvCal.register(UINib(nibName: String(describing: CalBurntConsumedCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CalBurntConsumedCell.self))
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        btDate.setTitle(dateFormatter.string(from: Date()), for: .normal)
        tvCal.tableFooterView = UIView()
        //btDate.setAttributedTitle(NSAttributedString(string: dateFormatter.string(from: Date())), for: .normal)
        //showPicker()
        presenter.callGetCaloriesBurntWS(date: btDate.title(for: .normal)!)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constants.EXERCISE_ARRAY), object: nil, queue: nil){
            [weak self](noti) in
            var calArray = [CalBurntModel]()
            var model :  CalBurntModel?
            
            //var prevCal = Int((self?.lblCalBurnt.text!.replacingOccurrences(of: " Cal.", with: ""))!) ?? 0
            for i in (noti.userInfo as NSDictionary?)?.object(forKey: "data") as! [ExerciseModel]{
                model=CalBurntModel()
                model?.og_calories_burnt = i.calories_burnt
                model?.user_calories_burnt = i.calories_burnt
                model?.og_duration_count = i.duration_count
                model?.user_duration_count = i.duration_count
                model?.units = i.units
                model?.exercise_id = i.id
                model?.unit_lu_id = i.unit_lu_id
                model?.exercise_name = i.exercise_name
                calArray.append(model!)
            }
            if calArray.count != 0{
                self?.tvCal.tableHeaderView=nil
            }else{
                DispatchQueue.main.async {
                    self?.tvCalHeightConst.constant = 100
                    self?.tvCal.tableHeaderView = self?.addFooterView(msg: "You don't have any entries made yet! \nClick on \"Add Items\" now!")
                }
            }
            self?.arrayCalories?[0].calBurt = calArray
            self?.arrayCalBurnt = calArray
            self?.arrayCalories?[0].allExercise = self?.originalAllExercise
            self?.calculateCalories()
            // self?.lblCalBurnt.text = String(describing: prevCal)
            //self?.tvCal.reloadData()
            
        }
    }
    func getDate()  {
        hidePickers()
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM, yyyy"
        btDate.setTitle(dateFormatter.string(from: (datePicker?.date)!), for: .normal)
        tvCal.tableHeaderView = nil
        presenter.callGetCaloriesBurntWS(date: btDate.title(for: .normal)!)
        
    }
    func hidePickers()  {
        isPickerOpen=false
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY+235, width: self.view.frame.width, height: 35)
            self.pickerView?.frame = CGRect(x: 0, y: self.view.frame.maxY+150, width: self.view.frame.width, height: 150)
        }){
            
            finished in
            self.toolbar?.removeFromSuperview()
            self.datePicker?.removeFromSuperview()
        }
        
        
    }
    func openDatePicker(date:String) {
        //hidePickers()
        isPickerOpen=true
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: self.view.frame.maxY+200, width: self.view.frame.width, height: 200))
        datePicker?.backgroundColor = UIColor.white
        datePicker?.datePickerMode = .date
        datePicker?.minimumDate = Calendar.current.date(byAdding: .month, value: -2, to: Date())
        datePicker?.maximumDate = Date()
        datePicker?.date = dateFormatter.date(from: date)!
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.maxY+240, width: self.view.frame.width, height: 200))
        toolbar?.sizeToFit()
        toolbar?.isTranslucent=false
        toolbar?.barTintColor = Colors.themeColor
        toolbar?.tintColor = UIColor.white
        let doneButton = UIBarButtonItem(title: "done", style: .done, target: self, action:#selector(WeightTracker.getDate))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action:#selector(WeightTracker.hidePickers))
        toolbar?.setItems([cancelButton, spacer, doneButton], animated: true)
        self.view.addSubview(self.datePicker!)
        self.view.addSubview(self.toolbar!)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.datePicker?.frame = CGRect(x: 0, y: self.view.frame.maxY-200, width: self.view.frame.width, height: 200)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY-235, width: self.view.frame.width, height: 35)
        }, completion: nil)
        
    }
    func showPicker(selectedPos:Int = 0) {
        pickerView = UIPickerView()
        pickerView?.frame = CGRect(x: 0, y: view.frame.maxY+150, width: self.view.frame.width, height: 150)
        pickerView?.backgroundColor = UIColor.white
        pickerView?.tintColor = Colors.themeColor
        pickerView?.showsSelectionIndicator=true
        for i in 1..<50{
            arrayCounts.append(String(i))
        }
        pickerView?.delegate=self
        pickerView?.dataSource=self
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.maxY+150, width: self.view.frame.width, height: 150))
        toolbar?.sizeToFit()
        toolbar?.isTranslucent=false
        toolbar?.barTintColor = Colors.themeColor
        toolbar?.tintColor = UIColor.white
        let doneButton = UIBarButtonItem(title: "done", style: .done, target: self, action:#selector(CaloriesBurnt.getSelectedCount))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action:#selector(WeightTracker.hidePickers))
        toolbar?.setItems([cancelButton, spacer, doneButton], animated: true)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.pickerView?.frame = CGRect(x: 0, y: self.view.frame.maxY-170, width: self.view.frame.width, height: 170)
            self.toolbar?.frame = CGRect(x: 0, y: self.view.frame.maxY-205, width: self.view.frame.width, height: 35)
        }, completion: nil)
        pickerView?.selectRow(selectedPos, inComponent: 0, animated: true)
        self.view.addSubview(self.pickerView!)
        self.view.addSubview(self.toolbar!)
    }
    func getSelectedCount()  {
        //arrayFilter[(pickerView?.selectedRow(inComponent: 0))!]
        hidePickers()
        let perMin = Double(arrayCalBurnt[position!].og_calories_burnt!)! /  Double(arrayCalBurnt[position!].og_duration_count!)!
        arrayCalBurnt[position!].og_duration_count = arrayCounts[(pickerView?.selectedRow(inComponent: 0))!]
        let count = Double(arrayCounts[(pickerView?.selectedRow(inComponent: 0))!])
        arrayCalBurnt[position!].og_calories_burnt = String((count! * perMin).round(to: 1))
        tvCal.reloadRows(at: [IndexPath(row: position!, section: 0)], with: .automatic)
        calculateCalories()
        
    }
    func calculateCalories() {
        var calBurnt = 0.0;
        for i in arrayCalBurnt{
            calBurnt = calBurnt + Double(i.og_calories_burnt!)!
        }
        lblCalBurnt.text = String(Int(calBurnt)) + " Cal."
    }
    func addFooterView(msg:String) -> UIView {
        let toastLabel = UILabel(frame:CGRect(x: 0,y:0,width: self.view.frame.width,height: 100))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.numberOfLines = 0
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.font = UIFont.systemFont(ofSize: 16)
        toastLabel.textAlignment = NSTextAlignment.center
        self.view.addSubview(toastLabel)
        toastLabel.text = msg
        toastLabel.clipsToBounds  =  true
        return toastLabel
    }
    
    //MARK:- Button Actions
    @IBAction func btDate(_ sender: Any) {
        if !isPickerOpen{
            openDatePicker(date: btDate.title(for: .normal)!)
        }
    }
    @IBAction func btAddCalories(_ sender: Any) {
        let vc = self.getViewControllerFromStoryBoard(storyBoardName: String(describing: AddCalorie.self), identifier: String(describing: AddCalorie.self)) as? AddCalorie
        if arrayCalories?[0].calBurt == nil || arrayCalories?[0].calBurt?.count == 0{
            vc?.arrayExercise = arrayCalories?[0].allExercise
            vc?.arrayCopy = arrayCalories?[0].allExercise
        }else{
            
            for i in (arrayCalories?[0].calBurt)!{
                for var (pos,j) in ((arrayCalories?[0].allExercise)?.enumerated())!{
                    if i.exercise_id == j.id{
                        arrayCalories?[0].allExercise?[pos].isSelected=true
                    }
                }
            }
            vc?.arrayExercise=arrayCalories?[0].allExercise
            vc?.arrayCopy = arrayCalories?[0].allExercise
        }
        self.switchViewController(viewController: vc)
    }
    @IBAction func btSave(_ sender: Any) {
        presenter.callAddCalBurntWS(array: arrayCalBurnt, date: "".getFormatedDate(fromFormat: "dd MMMM, yyyy", toFormat: "yyyy-MM-dd", dateString: btDate.title(for: .normal)!))
    }
    @IBAction func btBack(_ sender: Any) {
        goBack()
    }
}

extension CaloriesBurnt : CaloriesBurntDelegate{
    func showIndicator(showIt: Bool, showTint: Bool) {
        if showIt{
            if showTint{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: true, isObtrusive: true)
            }else{
                IndicatorView.sharedInstance.showActivityIdicator(mainView: self.view, showTint: false, isObtrusive: false)
            }
        }else{
            IndicatorView.sharedInstance.hideActivityIndicator()
        }
    }
    func onSuccess(successValue: Any) {
        arrayCalories = [CaloriesBurntModel]()
        arrayCalories = (successValue as? [CaloriesBurntModel])
        originalAllExercise = arrayCalories?[0].allExercise
        arrayCalBurnt = (arrayCalories?[0].calBurt!)!
    }
    func onError(messgae :Any?,errorValue: ErrorType) {
        if errorValue == ErrorType.NO_RESULT{
            arrayCalories = (messgae as? [CaloriesBurntModel])
            originalAllExercise = arrayCalories?[0].allExercise
            arrayCalBurnt = [CalBurntModel]()
            tvCalHeightConst.constant = 100
            tvCal.tableHeaderView = addFooterView(msg: "You don't have any entries made yet! \nClick on \"Add Items\" now!")
            //            self.btSave.isEnabled=false
            //            self.btSave.backgroundColor = UIColor.lightGray
            
        }else{
            
            ShowDefaultImages.sharedInstance.showDefaultImage(view: self.view, type: errorValue,header: "",msg: messgae as? String ?? ""){
                [weak self] finished in
                
            }
        }
    }
    func onCalAddedSuccess(msg: Any?) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.CAL_BURNT), object: self, userInfo: ["data":msg as? [TrackerModel] as Any])
        goBack()
    }
    func onCalAddedError(msg: String?, error: ErrorType?) {
        self.showToast(msg: msg ?? "",height: 100)
    }
}

//MARK:- Tableview Functions
extension CaloriesBurnt : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCalBurnt.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CalBurntConsumedCell.self), for: indexPath) as? CalBurntConsumedCell
        cell?.hideOthers=false
        cell?.data = arrayCalBurnt[indexPath.row]
        cell?.COUNT_CLICK = {
            [weak self] in
            self?.position = indexPath.row
            if !(self?.isPickerOpen)!{
                self?.showPicker(selectedPos: (Int((self?.arrayCalBurnt[indexPath.row].og_duration_count!)!)! - 1))
            }
            
        }
        cell?.DELETE_CLICK = {
            [weak self] in
            
            
            self?.arrayCalBurnt.remove(at: indexPath.row)
            self?.arrayCalories?[0].calBurt = self?.arrayCalBurnt
            self?.arrayCalories?[0].allExercise = self?.originalAllExercise
            self?.tvCal.reloadData()
            self?.calculateCalories()
            
            if self?.arrayCalBurnt.count == 0 {
                //                self?.btSave.isEnabled=false
                //                self?.btSave.backgroundColor = UIColor.lightGray
                self?.tvCalHeightConst.constant = 100
                self?.tvCal.tableHeaderView = self?.addFooterView(msg: "You don't have any entries made yet! \nClick on \"Add Items\" now!")
                self?.tvCal.reloadData()
            }
            
        }
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 57
    }
}

extension CaloriesBurnt : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayCounts.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayCounts[row]
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let color = (row == pickerView.selectedRow(inComponent: component)) ? UIColor.black : Colors.themeColor
        let attribute  = NSAttributedString(string: arrayCounts[row], attributes: [NSForegroundColorAttributeName : color])
        return attribute
    }
}
