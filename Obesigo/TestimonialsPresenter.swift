//
//  TestimonialsPresenter.swift
//  Obesigo
//
//  Created by Anuj  on 24/07/17.
//  Copyright © 2017 Tech Morphosis. All rights reserved.
//

import Foundation
protocol TestimonialsDelegate : CommonDelegate {
}
class TestimonialsPresenter{
    weak var delegate : TestimonialsDelegate?
    var service : TestimonialsService
    init(service:TestimonialsService) {
        self.service=service
    }
    func attachView(delegate:TestimonialsDelegate){
        self.delegate=delegate
    }
    func detachView() {
        delegate=nil
    }
    func callGetTestimonialsWS() {
        delegate?.showIndicator(showIt: true, showTint: false)
        service.callTestimonialWS {
            [weak self](success, error) in
            
            if error != nil{
                self?.delegate?.onError(messgae: success, errorValue: error!)
            }else{
                self?.delegate?.onSuccess(successValue: success as Any)
            }
            
            self?.delegate?.showIndicator(showIt: false, showTint: false)
            
        }
    }
}
class TestimonialsService{
    typealias onResponseReceived = (_ success: AnyObject?, _ error : ErrorType?) -> Void
    func callTestimonialWS(result: @escaping onResponseReceived) {
        let url = NetworkUrls.TESTIMONIALS+"?screen_name=Testimonials&webservice=Get Testimonial List&user_id=\(Preffrences().getUserId())"
        NetworkRequest.sharedInstance.callWebService(url: url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!, type: .GET, params: nil) {
            (success, error) in
            print(success as Any)
            print(error as Any)
            guard success != nil else{
                return result(nil, error)
            }
            if (success as? NSDictionary)?.object(forKey: "status") as? String == "Error"{
                if (success as? NSDictionary)?.object(forKey: "status_code") as? Int == 404{
                    result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.NO_RESULT)
                }else{
                    result((success as? NSDictionary)?.object(forKey: "message") as? String as AnyObject, ErrorType.Something_Went_Wrong)
                }
            }else{
                var array = [TestimonialsModel]()
                var model : TestimonialsModel?
                for i in ((success as? NSDictionary)?.object(forKey: "response") as? NSArray)!{
                    model = TestimonialsModel()
                    model?.first_name = (i as? NSDictionary)?.object(forKey: "first_name") as? String
                    model?.last_name = (i as? NSDictionary)?.object(forKey: "last_name") as? String
                    model?.created_date = (i as? NSDictionary)?.object(forKey: "created_date") as? String
                    model?.profile_photo = (i as? NSDictionary)?.object(forKey: "profile_photo") as? String
                    model?.testimonial_msg = (i as? NSDictionary)?.object(forKey: "testimonial_msg") as? String
                    model?.rating = (i as? NSDictionary)?.object(forKey: "rating") as? String
                    array.append(model!)
                }
                result(array as AnyObject, nil)
            }
        }
    }
}
